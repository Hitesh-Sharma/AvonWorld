/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package antistatic.spinnerwheel.adapters;

import java.util.ArrayList;

import android.content.Context;

/**
 * The simple Array spinnerwheel adapter
 * 
 * @param <T>
 *            the element type
 */
public class StringArrayWheelAdapter<T> extends AbstractWheelTextAdapter {

	// items
	private ArrayList<String> listItems;

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the current context
	 * @param items
	 *            the items
	 */
	public StringArrayWheelAdapter(Context context, ArrayList<String> listItems) {
		super(context);

		// setEmptyItemResource(TEXT_VIEW_ITEM_RESOURCE);
		this.listItems = listItems;
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the current context
	 * @param items
	 *            the items
	 */
	public StringArrayWheelAdapter(Context context, int resourceId,
			ArrayList<String> listItems) {
		super(context, resourceId);

		// setEmptyItemResource(TEXT_VIEW_ITEM_RESOURCE);
		this.listItems = listItems;
	}

	@Override
	public CharSequence getItemText(int index) {
		if (index >= 0 && index < listItems.size()) {
			return listItems.get(index);
		} else
			return "";

	}

	@Override
	public int getItemsCount() {
		return listItems.size();
	}
}
