package com.avon.india.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;

import java.util.HashMap;

public class RegisterOnServer implements ILoadListner {

    Context mContext;
    SharedPreferences mSharedPreferences;

    public RegisterOnServer(Context context) {
        this.mContext = context;
        this.mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
    }

    public void registerPush(String gcm_key, String device_id) {

        boolean isRegistered = mSharedPreferences.getBoolean(
                CommonUtilities.PREFRENCE_REGISTER, false);
        boolean isUserRegistered = AppPrefrence.getInstance(mContext)
                .getUserRegistationStatus();
        AppLogger.e("", "registered " + isRegistered);
        if (isRegistered && isUserRegistered)
            return;
        if (CheckNetworkState.isNetworkAvailable(mContext)) {
            try {
                HashMap<String, String> register = RequestFormater
                        .getRegisterOnServer(gcm_key, device_id, AppPrefrence
                                .getInstance(mContext).getMyCountryId());
                MyLoader loader = new MyLoader(Avon.getInstance()
                        .getActivityInstance(), false);
                loader.setOnLoadListner(this);
                loader.setRequestType(RequestType.NOTIFICATION);
                loader.startLoading(WebUrls.URL_GCM_REGISTER, register);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void afterLoadComplete(ResponseParser response) {
        if (response == null)
            return;

        // try {
        // JSONObject jsonResponse = new JSONObject(response.getResponse()
        // .toString());
        // String date_created = jsonResponse
        // .getString(JSONKeys.JSON_DATE_CREATED);
        // if (date_created != null
        // && !date_created.trim().equalsIgnoreCase("")) {
        // mSharedPreferences.edit()
        // .putBoolean(CommonUtilities.PREFRENCE_REGISTER, true)
        // .commit();
        // } else {
        // mSharedPreferences.edit()
        // .putBoolean(CommonUtilities.PREFRENCE_REGISTER, false)
        // .commit();
        // }
        // } catch (Exception e) {
        // e.printStackTrace();
        // }

        if (response.getResponseCode() == CommonUtilities.KEY_SUCCESS_RESPONSE || response.getResponseCode() == CommonUtilities.KEY_ALREADY_REGISTER) {
            mSharedPreferences.edit()
                    .putBoolean(CommonUtilities.PREFRENCE_REGISTER, true)
                    .commit();
            AppPrefrence.getInstance(mContext).setUserRegistationStatus(true);
        } else {
            mSharedPreferences.edit()
                    .putBoolean(CommonUtilities.PREFRENCE_REGISTER, false)
                    .commit();
        }
    }
}
