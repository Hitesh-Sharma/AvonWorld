//package com.avon.india.gcm;
//
//import java.util.List;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicHttpResponse;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.CoreConnectionPNames;
//import org.apache.http.params.HttpParams;
//import org.apache.http.util.EntityUtils;
//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//
//public class HttpPostRequest {
//
//	Context mContext;
//
//	public HttpPostRequest(Context context) {
//		this.mContext = context;
//		// TODO Auto-generated constructor stub
//	}
//
//	public String post(String Url) {
//
//		if (isNetworkAvailable()) {
//			HttpParams httpParameters = new BasicHttpParams();
//
//			int timeoutConnection = 7000;
//			httpParameters.setIntParameter(
//					CoreConnectionPNames.CONNECTION_TIMEOUT, timeoutConnection);
//
//			int timeoutSocket = 7000;
//
//			httpParameters.setIntParameter(CoreConnectionPNames.SO_TIMEOUT,
//					timeoutSocket);
//
//			HttpClient httpclient = new DefaultHttpClient(httpParameters);
//			HttpPost httpPost = null;
//			try {
//
//				httpPost = new HttpPost(Url);
//				BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//						.execute(httpPost);
//				HttpEntity entity = httpResponse.getEntity();
//				return EntityUtils.toString(entity);
//
//			} catch (Exception e) {
//				// TODO: handle exception
//
//				e.printStackTrace();
//				return e.getMessage();
//			}
//		} else
//			return "Sorry no Internet connection available";
//
//	}
//
//	public String post(String Url, List<NameValuePair> values) {
//
//		if (isNetworkAvailable()) {
//			HttpParams httpParameters = new BasicHttpParams();
//
//			int timeoutConnection = 7000;
//			httpParameters.setIntParameter(
//					CoreConnectionPNames.CONNECTION_TIMEOUT, timeoutConnection);
//
//			int timeoutSocket = 7000;
//
//			httpParameters.setIntParameter(CoreConnectionPNames.SO_TIMEOUT,
//					timeoutSocket);
//
//			HttpClient httpclient = new DefaultHttpClient(httpParameters);
//			HttpPost httpPost = null;
//			try {
//
//				httpPost = new HttpPost(Url);
//				if (values != null) {
//					httpPost.setEntity(new UrlEncodedFormEntity(values));
//				}
//				BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//						.execute(httpPost);
//				// Log.i("Login"," HTTP request url " +
//				// EntityUtils.toString(httpPost.getEntity()));
//				HttpEntity entity = httpResponse.getEntity();
//				return EntityUtils.toString(entity);
//
//			} catch (Exception e) {
//				// TODO: handle exception
//
//				e.printStackTrace();
//				return e.getMessage();
//			}
//		} else
//			return "Sorry no Internet connection available";
//
//	}
//
//	public boolean isNetworkAvailable() {
//		boolean outcome = false;
//
//		if (mContext != null) {
//			ConnectivityManager cm = (ConnectivityManager) mContext
//					.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//			NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
//
//			for (NetworkInfo tempNetworkInfo : networkInfos) {
//				if (tempNetworkInfo.isConnected()) {
//					outcome = true;
//					break;
//				}
//			}
//		}
//
//		return outcome;
//	}
//
//}
