//package com.avon.india.gcm;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.NameValuePair;
//import org.apache.http.message.BasicNameValuePair;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.os.AsyncTask;
//import android.preference.PreferenceManager;
//
//public class UnregisterOnServer extends AsyncTask<String, String, String> {
//
//	Context mContext;
//	String response = "";
//	String device_id;
//	String gcm_id;
//	String device_id_value;
//	String gcm_id_value;
//	String mUrl;
//	SharedPreferences mSharedPreferences;
//
//	public UnregisterOnServer(Context context, String device_id,
//			String device_id_value, String gcm_id, String gcm_id_value,
//			String url, String response) {
//		this.mContext = context;
//		this.device_id = device_id;
//		this.gcm_id = gcm_id;
//		this.device_id_value = device_id_value;
//		this.gcm_id_value = gcm_id_value;
//		this.mUrl = url;
//		this.response = response;
//		mSharedPreferences = PreferenceManager
//				.getDefaultSharedPreferences(mContext);
//	}
//
//	@Override
//	protected String doInBackground(String... params) {
//
//		String result = null;
//
//		try {
//			List<NameValuePair> values = new ArrayList<NameValuePair>();
//			values.add(new BasicNameValuePair(device_id, device_id_value));
//			values.add(new BasicNameValuePair(gcm_id, gcm_id_value));
//
//			result = new HttpPostRequest(mContext).post(mUrl, values);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return result;
//	}
//
//	@Override
//	protected void onPostExecute(String result) {
//
//		boolean isRegister = false;
//
//		if (result != null && result.equalsIgnoreCase(response)) {
//			isRegister = true;
//		} else {
//			isRegister = false;
//		}
//		mSharedPreferences.edit()
//				.putBoolean(CommonUtilities.PREFRENCE_REGISTER, isRegister)
//				.commit();
//	}
//}
