package com.avon.india.gcm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public final class CommonUtilities {

	public static final String SENDER_ID = "416521821620";

	public static final String TAG = "Gcm Service";
	public static final String DISPLAY_MESSAGE_ACTION = "src.avonmalaysia.DISPLAY_MESSAGE";
	public static final String EXTRA_MESSAGE = "message";

	public static String KEY_DEVICE_ID = "device_id";
	public static String KEY_GCM_ID = "device_token";

	public static String KEY_REGISTRATION_KEY = "registration_key";
	public static String KEY_TYPE_PUSH = "push_registration";

	public static int KEY_SUCCESS_RESPONSE = 200;
	public static int KEY_ALREADY_REGISTER=202;
	public static String KEY_SUCCESS_RESULT = "keyCode";
	public static String PREFRENCE_REGISTER = "isGcmRegisterToServer";
	public static String GCM_ID = "gcm_id";

	public static int Id = 0;

	public static String PREFRENCE_IS_REGISTRING = "isRegistring";

	static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		Log.e("Message here:", "The mesage coming " + message);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
