package com.avon.india.async;

/**
 * Created by esec-0052 on 18/5/16.
 */

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class RequestBuilderOKHttp {
    final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    OkHttpClient client = new OkHttpClient();

    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String post(String url, HashMap<String, String> params) throws Exception {
        FormBody.Builder builder = new FormBody.Builder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
        }
        RequestBody body = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code" + response.toString());
            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    String post(String url, JSONObject json) throws Exception {
        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code" + response.toString());
            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    String post(String url, HashMap<String, String> params, HashMap<String, File> fileParams) throws Exception {


        MultipartBody.Builder buildernew = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);

        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                buildernew.addFormDataPart(entry.getKey(), entry.getValue());
            }
        }

        if (fileParams != null && fileParams.size() > 0) {
            for (Map.Entry<String, File> entry : fileParams.entrySet()) {
                if (entry.getValue() != null && entry.getValue().exists()) {
                    buildernew.addFormDataPart(entry.getKey(), entry.getValue().getName(), RequestBody.create(MEDIA_TYPE_PNG, entry.getValue()));
                }
            }
        }

        RequestBody body = buildernew.build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try

        {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code" + response.toString());
            return response.body().string();
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }

        return null;
    }
}
