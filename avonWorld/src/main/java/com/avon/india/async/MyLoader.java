package com.avon.india.async;

import java.util.HashMap;

import com.avon.india.Avon;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.supports.AppLogger;

import android.content.Context;
import android.os.AsyncTask;

public class MyLoader {

    private ILoadListner listner;
    private String hitedUrl;
    private HashMap<String, String> params;
    Context mcContext;

    /**
     * @param mcContext
     */
    public MyLoader(Context mcContext) {
        this.mcContext = mcContext;
        Avon.getInstance().getActivityInstance().showLoading();
    }

    public MyLoader(Context mcContext, boolean isVisible) {
        this.mcContext = mcContext;
        if (isVisible) {
            Avon.getInstance().getActivityInstance().showLoading();
        }
    }

    /**
     * @param onLoadListner
     */
    public void setOnLoadListner(ILoadListner onLoadListner) {
        this.listner = onLoadListner;
    }

    /**
     * @param url
     * @param params
     */
    public void startLoading(String url, HashMap<String, String> params) {
        this.hitedUrl = url;
        this.params = params;
        try {
            AppLogger.e("url is ", "" + hitedUrl);
            AppLogger.e("params", "" + params);
        } catch (Exception e) {

        }
        new Loader().execute();
    }

    public void startLoading(String url) {
        this.hitedUrl = url;
        new Loader().execute();
    }

//    class Loader extends AsyncTask<Void, Void, ResponseParser> {
//        RequestBuilder builder;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            if (builder != null) {
//                builder.closeConnection();
//                builder = null;
//            }
//            builder = new RequestBuilder();
//        }
//
//        @Override
//        protected ResponseParser doInBackground(Void... voids) {
//            ResponseParser webResponse;
//            String response = null;
//            AppLogger.e("", "url is " + hitedUrl);
//            if (params != null) {
//                response = builder.post(hitedUrl, params);
//            } else {
//                response = builder.get(hitedUrl);
//            }
//            AppLogger.e("", "response " + response);
//            webResponse = new ResponseParser(requestType, response);
//            return webResponse;
//        }
//
//        @Override
//        protected void onPostExecute(ResponseParser result) {
//            super.onPostExecute(result);
//            if (listner != null) {
//                Avon.getInstance().getActivityInstance().hideLoading();
//                listner.afterLoadComplete(result);
//            }
//            if (builder != null)
//                builder.closeConnection();
//        }
//    }

    class Loader extends AsyncTask<Void, Void, ResponseParser> {
        RequestBuilderOKHttp builder;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (builder != null)
                builder = null;
            builder = new RequestBuilderOKHttp();
        }

        @Override
        protected ResponseParser doInBackground(Void... voids) {
            ResponseParser webResponse;
            String response = null;
            try {
                if (params != null) {
                    response = builder.post(hitedUrl, params);
                } else {
                    response = builder.get(hitedUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (response != null)
                AppLogger.e("response", " " + response);
            webResponse = new ResponseParser(requestType, response);
            return webResponse;
        }

        @Override
        protected void onPostExecute(ResponseParser result) {
            super.onPostExecute(result);
            if (listner != null) {
                Avon.getInstance().getActivityInstance().hideLoading();
                listner.afterLoadComplete(result);
            }
            if (builder != null)
                builder = null;
        }
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    private RequestType requestType;

    public enum RequestType {
        COUNTRY_LIST, PRODUCTS_ALL, PRODUCTS_BY_CATEGORY, OFFERS_DETAILS, FEEDBACK, NOTIFICATION, MEDIA, BOUTIQUE_LOCATOR, STATES_LIST,
        CALCULATE_PERCENT, CALCULATE_PERCENTAGE, PRODUCTS_FOR_BUEATY_STORE
    }
}
