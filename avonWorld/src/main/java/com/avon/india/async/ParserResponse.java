package com.avon.india.async;

import android.util.SparseArray;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.db.DbOperations;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.Category;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.Country;
import com.avon.india.itemproperty.LocationItem;
import com.avon.india.itemproperty.Offers;
import com.avon.india.itemproperty.Products;
import com.avon.india.itemproperty.States;
import com.avon.india.itemproperty.VideoItem;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.JSONKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParserResponse implements JSONKeys {

    public static Object getCountryList(String response) {
        List<Country> country = new ArrayList<Country>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray json_countries_array = jsonObject
                        .getJSONArray(JSONKeys.JSON_KEY_RESPONSE);
                if (json_countries_array != null
                        && json_countries_array.length() > 0) {
                    for (int i = 0; i < json_countries_array.length(); i++) {
                        JSONObject jobFeed = json_countries_array
                                .getJSONObject(i);
                        Country cntry = new Country(jobFeed);
                        country.add(cntry);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return country;
    }

    public static Object getProducts(String response) {
        List<Category> categories = new ArrayList<Category>();
        List<CategoryItem> categoryItems = null;
        Products product = new Products();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject json_response = jsonObject
                        .getJSONObject(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_response != null) {
                    JSONArray jsonCategories = json_response
                            .getJSONArray(JSONKeys.JSON_KEY_CATEGORIES);
                    if (jsonCategories != null && jsonCategories.length() > 0) {
                        for (int i = 0; i < jsonCategories.length(); i++) {
                            Category category = new Category(
                                    jsonCategories.getJSONObject(i));
                            if (category != null)
                                categories.add(category);
                        }
                    }
//                    Avon.getInstance().setCategoriesList(categories);
                    JSONObject jsonProducts = json_response
                            .getJSONObject(JSONKeys.JSON_KEY_PRODUCTS);
                    SparseArray<List<CategoryItem>> items = new SparseArray<List<CategoryItem>>();
                    categoryItems = new ArrayList<CategoryItem>();
                    for (Category category : categories) {
                        int category_id = category.getCategoryId();
                        JSONArray json_products_array = jsonProducts
                                .getJSONArray(category_id + "");
                        if (json_products_array != null
                                && json_products_array.length() > 0) {
                            for (int i = 0; i < json_products_array.length(); i++) {
                                CategoryItem feed = new CategoryItem(
                                        json_products_array.getJSONObject(i));
                                if (feed != null)
                                    categoryItems.add(feed);
                            }
                        }
                        if (categoryItems != null) {
                            items.put(category_id, categoryItems);
                        }
                    }
//                    Avon.getInstance().setProductsItemsList(items);
                }
                if (categories.size() > 0)
                    product.setCategoriesList(categories);
                if (categoryItems.size() > 0)
                    product.setCategoryItemsList(categoryItems);
                AppLogger.e("categoryItems", "" + categoryItems.size());
//                DbOperations.deleteAllCategories(Avon.getInstance().getActivityInstance());
//                DbOperations.deleteAllProducts(Avon.getInstance().getActivityInstance());
                DbOperations.insertCategory(Avon.getInstance().getActivityInstance(), product);

                fetchDataFromDBandInsetToList();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return product;
    }

    public static Object getProductForIndia(String response) {
        List<Category> categories = new ArrayList<Category>();
        List<CategoryItem> categoryItems = null;
        Products product = new Products();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject json_response = jsonObject
                        .getJSONObject(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_response != null) {
                    JSONArray jsonCategories = json_response
                            .getJSONArray(JSONKeys.JSON_KEY_CATEGORIES);
                    if (jsonCategories != null && jsonCategories.length() > 0) {
                        for (int i = 0; i < jsonCategories.length(); i++) {
                            Category category = new Category(
                                    jsonCategories.getJSONObject(i));
                            if (category != null)
                                categories.add(category);
                        }
                    }
                    JSONObject jsonProducts = json_response
                            .getJSONObject(JSONKeys.JSON_KEY_PRODUCTS);
                    categoryItems = new ArrayList<CategoryItem>();
                    for (Category category : categories) {
                        int category_id = category.getCategoryId();
                        JSONArray json_products_array = jsonProducts
                                .getJSONArray(category_id + "");
                        if (json_products_array != null
                                && json_products_array.length() > 0) {
                            for (int i = 0; i < json_products_array.length(); i++) {
                                CategoryItem feed = new CategoryItem(
                                        json_products_array.getJSONObject(i));
                                if (feed != null)
                                    categoryItems.add(feed);
                            }
                        }
                    }
                }
                if (categories.size() > 0)
                    product.setCategoriesList(categories);
                if (categoryItems.size() > 0)
                    product.setCategoryItemsList(categoryItems);
                DbOperations.deleteAllCategories(Avon.getInstance().getActivityInstance());
                DbOperations.deleteAllProducts(Avon.getInstance().getActivityInstance());
                DbOperations.insertCategory(Avon.getInstance().getActivityInstance(), product);

                fetchDataFromDBandInsetToList();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return product;
    }

    public static void fetchDataFromDBandInsetToList() {
        List<Category> categories = DbOperations.getCategoriesList(Avon.getInstance().getActivityInstance());
        if (categories == null | categories.size() == 0)
            return;
        Avon.getInstance().setCategoriesList(categories);
        SparseArray<List<CategoryItem>> items = new SparseArray<List<CategoryItem>>();
        for (Category category : categories) {
            int category_id = category.getCategoryId();
            List<CategoryItem> categoryItems = DbOperations.getProductsListList(Avon.getInstance().getActivityInstance(), category_id);
            AppLogger.e("category_id ", "category_id " + category_id);
            if (categoryItems != null
                    && categoryItems.size() > 0) {
                AppLogger.e("categoryItems ", "categoryItems " + categoryItems.size());
                items.put(category_id, categoryItems);
            }
        }
        Avon.getInstance().setProductsItemsList(items);
    }

    public static Object getCategoryProducts(String response) {
        List<CategoryItem> categoryItems = new ArrayList<CategoryItem>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject json_response = jsonObject
                        .getJSONObject(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_response != null) {
                    JSONArray jsonProducts = json_response
                            .getJSONArray(JSONKeys.JSON_KEY_PRODUCTS);
                    if (jsonProducts != null && jsonProducts.length() > 0) {
                        for (int i = 0; i < jsonProducts.length(); i++) {
                            CategoryItem feed = new CategoryItem(
                                    jsonProducts.getJSONObject(i));
                            if (feed != null)
                                categoryItems.add(feed);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (categoryItems.size() > 0)
            DbOperations.insertProducts(Avon.getInstance().getActivityInstance(), categoryItems);
//        fetchDataFromDBandInsetToList();
        return categoryItems;
    }

    public static Object getAllProducts(String response) {
        List<CategoryItem> categoryItems = new ArrayList<CategoryItem>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray json_response = jsonObject
                        .getJSONArray(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_response != null && json_response.length() > 0) {
                    for (int i = 0; i < json_response.length(); i++) {
                        CategoryItem feed = new CategoryItem(
                                json_response.getJSONObject(i));
                        if (feed != null)
                            categoryItems.add(feed);
                    }
                }
//                try {
//                    if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
//                        DbOperations.deleteAllProducts(Avon.getInstance().getActivityInstance());
//                        DbOperations.insertProducts(Avon.getInstance().getActivityInstance(), categoryItems);
//                    }
//                } catch (Exception e) {
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return categoryItems;
    }

    public static Object getoffersList(String response) {
        List<Offers> offers = new ArrayList<Offers>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray json_offers_array = jsonObject
                        .getJSONArray(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_offers_array != null) {
                    for (int i = 0; i < json_offers_array.length(); i++) {
                        JSONObject jobFeed = json_offers_array.getJSONObject(i);
                        Offers offers_list = new Offers(jobFeed);
                        offers.add(offers_list);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return offers;
    }

    public static Object getnotification(JSONObject json) {
        CategoryItem category_item = null;
        if (json != null) {
            try {
                JSONObject jord = new JSONObject(
                        json.getString(JSON_KEY_NOTIFICATION_KEY));
                category_item = new CategoryItem(jord);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return category_item;
    }

    public static Object getBoutiqueLocation(String response) {
        List<LocationItem> boutique_location = new ArrayList<LocationItem>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray json_products_array = jsonObject
                        .getJSONArray(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_products_array != null
                        && json_products_array.length() > 0) {
                    for (int i = 0; i < json_products_array.length(); i++) {
                        JSONObject jobFeed = json_products_array
                                .getJSONObject(i);
                        LocationItem feed = new LocationItem(jobFeed);
                        boutique_location.add(feed);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return boutique_location;
    }

    public static Object getVideoListings(String response) {
        List<VideoItem> video_List = new ArrayList<VideoItem>();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray json_products_array = jsonObject
                        .getJSONArray(JSONKeys.JSON_OBJECT_RESPONSE);
                if (json_products_array != null
                        && json_products_array.length() > 0) {
                    for (int i = 0; i < json_products_array.length(); i++) {
                        JSONObject jobFeed = json_products_array
                                .getJSONObject(i);
                        VideoItem feed = new VideoItem(jobFeed);
                        video_List.add(feed);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return video_List;
    }

    public static Object getStatesListings(String response) {
        List<States> states_list = new ArrayList<States>();
        try {
            JSONObject json = new JSONObject(response);
            JSONArray jsonArray = json.getJSONArray(JSONKeys.JSON_KEY_RESPONSE);
            for (int i = 0; i < jsonArray.length(); i++) {
                States state = new States(jsonArray.getJSONObject(i));
                if (state != null)
                    states_list.add(state);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return states_list;

    }

    public static void setCalculatingPercentage(String response) {
        try {
            JSONObject json = new JSONObject(response);
            JSONArray jsonArray = json.getJSONArray(JSONKeys.JSON_KEY_RESPONSE);
            if (jsonArray != null && jsonArray.length() > 0) {
                JSONObject jsonResponse = jsonArray.getJSONObject(0);
                AppPrefrence.getInstance(
                        Avon.getInstance().getActivityInstance())
                        .setCalculationMaxPercent(
                                (float) jsonResponse
                                        .getDouble(JSON_KEY_MAX_PERCENT));
                AppPrefrence.getInstance(
                        Avon.getInstance().getActivityInstance())
                        .setCalculationMaxPercent(
                                (float) jsonResponse
                                        .getDouble(JSON_KEY_MIN_PERCENT));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}