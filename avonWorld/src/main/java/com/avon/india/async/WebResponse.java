package com.avon.india.async;

import com.avon.india.async.MyLoader.RequestType;

/**
 * @author ESEC-0054 Preeti
 * 
 *         Jul 24, 2015
 */
public class WebResponse {

	private RequestType requestType;
	private Object response;

	/**
	 * @return the requestedUrl
	 */
	public RequestType getRequestedType() {
		return requestType;
	}

	/**
	 * @param requestedUrl
	 *            the requestedUrl to set
	 */
	public void setRequestedType(RequestType requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the response
	 */
	public Object getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(Object response) {
		this.response = response;
	}
}
