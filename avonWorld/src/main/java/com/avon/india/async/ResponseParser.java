package com.avon.india.async;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.db.DbOperations;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.JSONKeys;

import org.json.JSONObject;

public class ResponseParser extends WebResponse implements IResponseCode {

    private int responseCode = -1;
    private String responseMessage;

    public ResponseParser(RequestType requestType, String response) {
        setRequestedType(requestType);
        if (response == null || response.trim().length() == 0)
            return;
        String code = "", message = "";
        AppLogger.e("", requestType.toString() + " Response : " + response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            code = jsonObject.getString(JSONKeys.JSON_KEY_CODE);
            message = jsonObject.getString(JSONKeys.JSON_KEY_CODE_MESSAGE);
            try {
                String timeStamp = jsonObject.getString(JSONKeys.JSON_KEY_TIMESTAMP);
                if (timeStamp != null && timeStamp.trim().length() > 0)
                    DbOperations.insertTimeStamp(Avon.getInstance().getActivityInstance(), timeStamp);
            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (code != null && !code.trim().equalsIgnoreCase("")) {
            try {
                responseCode = Integer.parseInt(code);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (message != null && !message.trim().equalsIgnoreCase("")) {
            responseMessage = message;
        }
        if (code == null || code.trim().equalsIgnoreCase("")) {
            setResponse(response);
        }

        switch (responseCode) {
            case IResponseCode.RESPONSE_SUCCESS:
                setDataFromJson(response, requestType);
                break;

            default:
                break;
        }

    }

    public void setDataFromJson(String response, RequestType type) {
        AppLogger.e("RequestType " + type.name().toString(), " response " + response);
        switch (type) {
            case COUNTRY_LIST:
                setResponse(ParserResponse.getCountryList(response));
                AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).saveCountryIds(response);
                break;
            case PRODUCTS_ALL:
                try {
//                    if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA))
//                        setResponse(ParserResponse.getProductForIndia(response));
//                    else
                    setResponse(ParserResponse.getProducts(response));
                } catch (Exception e) {
                    setResponse(ParserResponse.getProducts(response));
                }

                break;
            case PRODUCTS_BY_CATEGORY:
                setResponse(ParserResponse.getCategoryProducts(response));
                break;
            case PRODUCTS_FOR_BUEATY_STORE:
                setResponse(ParserResponse.getAllProducts(response));
                break;
            case OFFERS_DETAILS:
                setResponse(ParserResponse.getoffersList(response));
                break;
            case BOUTIQUE_LOCATOR:
                setResponse(ParserResponse.getBoutiqueLocation(response));
                break;
            case MEDIA:
                setResponse(ParserResponse.getVideoListings(response));
                break;
            case STATES_LIST:
                setResponse(ParserResponse.getStatesListings(response));
                break;
            case NOTIFICATION:

                break;
            case CALCULATE_PERCENT:
                ParserResponse.setCalculatingPercentage(response);
                break;
            default:
                break;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

}
