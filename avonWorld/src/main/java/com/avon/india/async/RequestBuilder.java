//package src.avon.world.saveon.async;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicHttpResponse;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.CoreConnectionPNames;
//import org.apache.http.params.HttpParams;
//import org.apache.http.util.EntityUtils;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//import src.avon.world.saveon.supports.AppLogger;
//
//public class RequestBuilder {
//    private HttpClient httpclient;
//    private HttpPost httpPost;
//
//    private HttpGet httpGet;
//
//    public RequestBuilder() {
//        HttpParams httpParameters = new BasicHttpParams();
//        int timeoutConnection = 7000;
//        httpParameters.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
//                timeoutConnection);
//        int timeoutSocket = 14000;
//        httpParameters.setIntParameter(CoreConnectionPNames.SO_TIMEOUT,
//                timeoutSocket);
//        httpclient = new DefaultHttpClient(httpParameters);
//    }
//
//    /**
//     * @param url String URL
//     * @author Don't Worry
//     */
//    public String get(String url) {
//        httpGet = null;
//        try {
//            httpGet = new HttpGet(url);
//            BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//                    .execute(httpGet);
//            HttpEntity entity = httpResponse.getEntity();
//            return EntityUtils.toString(entity);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    /**
//     * @param url   String URL
//     * @param param Params
//     * @author Don't Worry
//     */
//    public String get(String url, String param) {
//        url = url + "/" + param;
//        httpGet = null;
//        try {
//            httpGet = new HttpGet(url);
//            BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//                    .execute(httpGet);
//            HttpEntity entity = httpResponse.getEntity();
//            return EntityUtils.toString(entity);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    /**
//     * @param url    String URL
//     * @param values List<NameValuePair>
//     * @author Don't Worry
//     */
//
//    public String post(String Url, List<NameValuePair> values) {
//        httpPost = null;
//        try {
//            httpPost = new HttpPost(Url);
//            if (values != null) {
//                httpPost.setEntity(new UrlEncodedFormEntity(values));
//            }
//            BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//                    .execute(httpPost);
//            HttpEntity entity = httpResponse.getEntity();
//            return EntityUtils.toString(entity);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    /**
//     * @param url     String URL
//     * @param hashMap HashMap<String, String>
//     * @author Don't Worry
//     */
//    public String post(String Url, HashMap<String, String> hashMap) {
//        List<NameValuePair> values = null;
//        if (hashMap != null) {
//            List<String> keys = new ArrayList<String>(hashMap.keySet());
//            List<String> value = new ArrayList<String>(hashMap.values());
//            values = new ArrayList<NameValuePair>();
//            for (int i = 0; i < keys.size(); i++) {
//                values.add(new BasicNameValuePair(keys.get(i), value.get(i)));
//            }
//            AppLogger.e("namevaluepair", "" + values);
//        }
//        try {
//            httpPost = new HttpPost(Url);
//            if (hashMap != null && values != null) {
//                httpPost.setEntity(new UrlEncodedFormEntity(values));
//            }
//            BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
//                    .execute(httpPost);
//            HttpEntity entity = httpResponse.getEntity();
//            return EntityUtils.toString(entity);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    public String post(String Url, JSONObject json) {
//        try {
//            httpPost = new HttpPost(Url);
//            if (json != null) {
//                AppLogger.e("namevaluepair", "" + json.toString());
//                StringEntity se = new StringEntity(json.toString());
//                se.setContentEncoding("UTF-8");
//                // se.setContentType("application/json");
//                httpPost.setHeader("Accept", "application/json");
//                httpPost.setHeader("Content-type", "application/json");
//                httpPost.setEntity(se);
//            }
//            HttpResponse httpResponse = httpclient.execute(httpPost);
//            String response = EntityUtils.toString(httpResponse.getEntity());
//            return response;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    public void closeConnection() {
//        try {
//            if (httpclient != null)
//                httpclient.getConnectionManager().shutdown();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
