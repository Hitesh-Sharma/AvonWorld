package com.avon.india.async;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.AvonMainActivity;
import com.avon.india.R;
import com.avon.india.db.DbOperations;
import com.avon.india.fragments.FragmentNotificationNew;
import com.avon.india.itemproperty.Notifications;

public class AsyncNotification extends AsyncTask<String, Void, Void> {

	private Notifications notification;
	private Context context;

	public AsyncNotification(Context context) {
		this.context = context;
	}

	@Override
	protected Void doInBackground(String... params) {
		if (params == null || params.length == 0)
			return null;
		notification = new Notifications(params[0]);
		DbOperations.setNotificationList(context, notification);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (Avon.getInstance().getActivityInstance() != null) {
			if (Avon.getInstance().getFlowOrganization() != null) {
				String tag = Avon.getInstance().getFlowOrganization()
						.getCurrentFragmentTag();
				Fragment fr = Avon.getInstance().getActivityInstance()
						.getSupportFragmentManager().findFragmentByTag(tag);
				if (fr != null && fr instanceof FragmentNotificationNew) {
					FragmentNotificationNew ls = (FragmentNotificationNew) fr;
					try {
						ls.loadNotification();
					} catch (Exception e) {
					}
				}
			}
		}
		sendnotification(notification.getTitle(),notification.getDescription());
	}

	/**
	 * @author Wild Coder
	 * @param push
	 *            Push
	 * */

	private void sendnotification(String title,String notifactionText) {
		Uri notification = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Intent notificationIntent;
		notificationIntent = new Intent(context, AvonMainActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
				.addFlags(Intent.FLAG_FROM_BACKGROUND)
				.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.app_icon)
				.setContentTitle(title).setAutoCancel(true)
				.setSound(notification).setContentText(notifactionText);
		Bundle bundle = new Bundle();
		// bundle.putParcelable(Extras.PUSH_OBJECT, pushManger);
		notificationIntent.putExtras(bundle);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(contentIntent);
		// Add as notification
		NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// int id = (int) System.currentTimeMillis();
		int id = AppPrefrence.getInstance(context).getNotificationIds().size();
		AppPrefrence.getInstance(context).saveNotificationIds(id + 1);
		manager.notify((id + 1), builder.build());
	}
	// public void showNotification() {
	// String ns = Context.NOTIFICATION_SERVICE;
	// int icon = R.drawable.app_icon;
	//
	// RemoteViews contentView = new RemoteViews(context.getPackageName(),
	// R.layout.push_notification_layout);
	// contentView.setImageViewResource(R.id.img_tittle_custom_noti,
	// R.drawable.ic_launcher);
	// contentView.setTextViewText(R.id.txt_tittle_custom_noti,
	// notification.getTitle());
	//
	// contentView.setTextViewText(R.id.txt_description_custom_noti,
	// notification.getMessage());
	//
	// CharSequence tickerText = notification.getDescription();
	//
	// long when = System.currentTimeMillis();
	//
	// NotificationManager mNotificationManager = (NotificationManager) context
	// .getSystemService(ns);
	// Intent notificationIntent = new Intent(context, AvonMainActivity.class);
	// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
	// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
	// | Intent.FLAG_ACTIVITY_CLEAR_TOP);
	// notificationIntent.setAction(AvonAction.ACTION_PUSH);
	// notificationIntent.putExtra(AvonAction.ACTION_PUSH,
	// AvonAction.ACTION_PUSH);
	// PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
	// notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	//
	// Notification notification = new Notification(icon, tickerText, when);
	// notification.contentView = contentView;
	// notification.flags = Notification.FLAG_AUTO_CANCEL;
	// notification.defaults |= Notification.DEFAULT_SOUND;
	// notification.defaults |= Notification.DEFAULT_VIBRATE;
	// notification.contentIntent = contentIntent;
	// mNotificationManager.notify(1, notification);
	// }

}
