package com.avon.india;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.supports.SupportUtils;

public class AvonSplash extends Activity {
	private Handler mHandler;
	Geocoder geocodeLocation;
	TextView txtView_version;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.activity_splash);
		txtView_version = (TextView) findViewById(R.id.txtView_version);
		mHandler = new Handler();
		mHandler.postDelayed(mUpdate, 500);

		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			if (SupportUtils.isValidString(version))
				txtView_version.setText("Version : " + version);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadHome() {
		Intent intent = new Intent(AvonSplash.this, AvonMainActivity.class);
		startActivity(intent);
		destroyHandler();
		finish();
	}

	/**
	 * @category Destroy Handler
	 * 
	 */
	private void destroyHandler() {
		if (mHandler != null && mUpdate != null) {
			mHandler.removeCallbacks(mUpdate);
		}
	}

	// runnable
	Runnable mUpdate = new Runnable() {
		@Override
		public void run() {
			loadHome();

		}
	};

}
