package com.avon.india.interfaces;

import android.support.v4.app.Fragment;

public interface IOnFragmentUpdated {
	public int onUpdated(int index, Fragment fr);
}
