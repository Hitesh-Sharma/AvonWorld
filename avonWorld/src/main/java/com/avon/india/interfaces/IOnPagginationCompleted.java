package com.avon.india.interfaces;

public interface IOnPagginationCompleted {

	void onPaggingCompleted(boolean isMoreItemsThere, int position,
			int list_size);

}
