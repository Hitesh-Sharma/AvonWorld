package com.avon.india.interfaces;

public interface IOnBackPressedListener {

	public boolean onBackPressed();

}
