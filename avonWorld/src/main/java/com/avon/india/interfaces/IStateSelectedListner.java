package com.avon.india.interfaces;

public interface IStateSelectedListner {
	public void getSelectedStatePosition(int position, int item_id, String name);

	public void isListMatched(boolean listMatching);
}
