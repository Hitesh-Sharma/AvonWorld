package com.avon.india.interfaces;

import android.view.View;

public interface IOnpreparedListener {
	public void onViewPrepared(View view);

}
