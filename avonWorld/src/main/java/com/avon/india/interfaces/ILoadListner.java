package com.avon.india.interfaces;

import com.avon.india.async.ResponseParser;

/**
 * @author ESEC-0054 Preeti
 * 
 *         Jul 24, 2015
 */
public interface ILoadListner {
	void afterLoadComplete(ResponseParser response);
}
