package com.avon.india.interfaces;

public interface IOnscrollUp {

	public void onScrolledUp(int lastIndex,
			IOnPagginationCompleted iOnPaggingCompleted, boolean showLoader);

}
