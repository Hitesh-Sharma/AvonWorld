package com.avon.india.interfaces;

public interface IOnPageChangeListener {

	public void onPageChangedTo(int changed_position);

}
