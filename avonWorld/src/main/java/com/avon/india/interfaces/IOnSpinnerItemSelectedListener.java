package com.avon.india.interfaces;

public interface IOnSpinnerItemSelectedListener {
	public void onItemSelected(int text);
}
