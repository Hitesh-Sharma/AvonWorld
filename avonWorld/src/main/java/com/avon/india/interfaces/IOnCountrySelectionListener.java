package com.avon.india.interfaces;

public interface IOnCountrySelectionListener {
	public void onCountrySelected(int country_id, String country_name,
			int position);
}
