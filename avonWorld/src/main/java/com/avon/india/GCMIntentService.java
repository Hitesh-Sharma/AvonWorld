package com.avon.india;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.avon.india.R;
import com.avon.india.async.AsyncNotification;
import com.avon.india.gcm.CommonUtilities;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.DeviceIdReceiver;
import com.avon.india.supports.SupportUtils;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";
	SharedPreferences sharedprefernces;

	public GCMIntentService() {
		super(CommonUtilities.SENDER_ID);
		HandlerThread thread = new HandlerThread(
				"UmanoGCMIntentService:WorkerThread",
				android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
	}

	@Override
	protected void onRegistered(final Context mContext, final String regId) {
		sharedprefernces = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		AppLogger.e("GCM ID", "Id:" + regId);
		sharedprefernces.edit().putString(CommonUtilities.GCM_ID, regId)
				.commit();
		String device_id = DeviceIdReceiver.getInstance().getHashedDeivceId(
				getApplicationContext());
		Avon.getInstance().getActivityInstance()
				.registerGCMID(regId, device_id);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		AppLogger.e(TAG, "Device onMessage");
		String data2 = intent.getExtras().toString();
		String data = intent.getStringExtra(JSONKeys.JSON_KEY_DATA);
		sharedprefernces = PreferenceManager
				.getDefaultSharedPreferences(context);
		AppLogger.e("data received by receiver data2", " " + data2);
		AppLogger.e("data received by receiver data", " " + data);

		if (SupportUtils.isValidString(data)) {
			AsyncNotification asyncNotification = new AsyncNotification(context);
			asyncNotification.execute(data);
		} else {
			sendnotification(context,
					"Hello Here is the AVON Malaysia push data \n" + data);
		}
		// PushParserManager pushManger = new PushParserManager(data);
		//
		// if (checkForAppOpen()) {
		// AppLogger.e("App already open", "yes");
		// FetchiziApp.getInstanse().getScreenManagerInstanse(pushManger)
		// .notifieAction();
		// } else {
		// int initial_count = sharedprefernces.getInt(
		// ISharedPreferenceKeys.APP_BADGE_COUNT, 0);
		//
		// if (initial_count == 0 || initial_count < 0) {
		// sharedprefernces.edit()
		// .putInt(ISharedPreferenceKeys.APP_BADGE_COUNT, 1)
		// .commit();
		// } else if (initial_count > 0) {
		// initial_count += 1;
		// sharedprefernces
		// .edit()
		// .putInt(ISharedPreferenceKeys.APP_BADGE_COUNT,
		// initial_count).commit();
		// }
		// int count = sharedprefernces.getInt(
		// ISharedPreferenceKeys.APP_BADGE_COUNT, 0);
		// AppLogger.e("Notification Count", "" + count);
		// BadgeUtils.setBadge(context, count);

		// }
	}

	// private boolean checkForAppOpen() {
	// boolean showed = false;
	// if (Avon.getInstance().getActivityInstance() != null
	// && !Avon.getInstance().isAppOnBackground()) {
	// showed = true;
	// }
	// return showed;
	// }

	protected void sendnotification(Context context, String notifactionText) {
		Uri notification = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Intent notificationIntent;
		notificationIntent = new Intent(context, AvonMainActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
				.addFlags(Intent.FLAG_FROM_BACKGROUND)
				.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.app_icon)
				.setContentTitle("AVON").setAutoCancel(true)
				.setSound(notification).setContentText(notifactionText);
		Bundle bundle = new Bundle();
		// bundle.putParcelable(Extras.PUSH_OBJECT, pushManger);
		notificationIntent.putExtras(bundle);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(contentIntent);
		// Add as notification
		NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		int id = (int) System.currentTimeMillis();
		manager.notify(id, builder.build());
	}

	@Override
	public void onError(Context context, String errorId) {
		AppLogger.e(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		AppLogger.e(TAG, "Received recoverable error: " + errorId);
		final SharedPreferences mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);

		boolean isRegistring = mSharedPreferences.getBoolean(
				CommonUtilities.PREFRENCE_IS_REGISTRING, false);
		mSharedPreferences
				.edit()
				.putBoolean(CommonUtilities.PREFRENCE_IS_REGISTRING,
						!isRegistring).commit();

		return super.onRecoverableError(context, errorId);
	}

}
