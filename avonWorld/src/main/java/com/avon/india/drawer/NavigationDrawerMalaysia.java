package com.avon.india.drawer;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.R;

import java.util.ArrayList;

public class NavigationDrawerMalaysia extends RelativeLayout implements
        View.OnClickListener {
    public static interface IOnDrawerListner {
        public abstract void onDrawerItemClick(View view);
    }

    public static interface IOnSliderListner {
        public abstract void onSlide();
    }

    private int currentResourceId;
    private int parent_id_with_child;
    private boolean is_parent_view_visible = false;
    private LinearLayout linear_about_subs;
    private ImageButton image_button_about_us;

    private ArrayList<View> _drawer_items;
    private IOnDrawerListner _on_drawer_listner;
    private IOnSliderListner onSliderListner;
    Animation slide_in;
    Animation slide_out;

    private TextView txt_view_logined_user;

    public NavigationDrawerMalaysia(Activity activity) {
        super(activity);
        ((LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.drawer_malaysia, this, true);

        linear_about_subs = (LinearLayout) findViewById(R.id.linear_about_subs);
        image_button_about_us = (ImageButton) findViewById(R.id.image_button_about_us);
        initControls1();
        slide_in = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in);
        slide_out = AnimationUtils
                .loadAnimation(getContext(), R.anim.slide_out);
        for (View view : _drawer_items) {
            view.setOnClickListener(this);
        }
    }

    public void hideViewFromDrawer(int hideViewId, boolean status) {
        Log.e("", "status " + status);
        LinearLayout hideView = (LinearLayout) findViewById(hideViewId);
        if (status)
            hideView.setVisibility(View.VISIBLE);
        else
            hideView.setVisibility(View.GONE);
    }

    private void initControls1() {
        _drawer_items = new ArrayList<View>();
        _drawer_items.add(findViewById(R.id.linear_home));
        _drawer_items.add(findViewById(R.id.linear_bueaty_store));
        _drawer_items.add(findViewById(R.id.linear_products));
        _drawer_items.add(findViewById(R.id.linear_boutique));
//        _drawer_items.add(findViewById(R.id.linear_about_us_one));
        _drawer_items.add(findViewById(R.id.linear_about));
        _drawer_items.add(findViewById(R.id.relative_our_history));
        _drawer_items.add(findViewById(R.id.relative_our_mission));
        _drawer_items.add(findViewById(R.id.relative_key_strengths));
        _drawer_items.add(findViewById(R.id.linear_videos));
        _drawer_items.add(findViewById(R.id.linear_feedback));
        _drawer_items.add(findViewById(R.id.linear_notification));
        _drawer_items.add(findViewById(R.id.linear_current_offers));
        _drawer_items.add(findViewById(R.id.linear_exit));
        currentResourceId = R.id.linear_home;
        parent_id_with_child = R.id.linear_about;
        linear_about_subs.setVisibility(View.GONE);
    }

    public void setLogginedUserName(String first_name) {
        if (txt_view_logined_user != null) {
            txt_view_logined_user.setText(first_name);
        }
    }

    private void setAboutUsStatus(boolean status) {

        if (!status) {
            is_parent_view_visible = false;
            final LinearLayout linear_about_subs = (LinearLayout) findViewById(R.id.linear_about_subs);

            linear_about_subs.startAnimation(slide_out);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    linear_about_subs.setVisibility(View.GONE);
                }
            }, 500);

            image_button_about_us.setRotation(0);

        } else {
            is_parent_view_visible = true;
            linear_about_subs.setVisibility(View.VISIBLE);

            image_button_about_us.setRotation(90);
        }
    }

    public void onClick(final View view) {
        if (view.getId() == parent_id_with_child) {
            if (is_parent_view_visible) {
                is_parent_view_visible = false;
                setAboutUsStatus(false);

            } else {
                LinearLayout linear_about_subs = (LinearLayout) findViewById(R.id.linear_about_subs);

                linear_about_subs.startAnimation(slide_in);
                setAboutUsStatus(true);
            }
            return;
        }
        if ((view.getId() == R.id.relative_our_history)
                || (view.getId() == R.id.relative_our_mission)
                || (view.getId() == R.id.relative_key_strengths)) {
            setAboutUsStatus(true);
        } else
            setAboutUsStatus(false);
        if (view.getId() == currentResourceId && IS_SKIP_SAME) {
            view.setBackgroundResource(R.color.color_drawer_selector);
            if (onSliderListner != null) {
                onSliderListner.onSlide();
            }
            return;
        }

        Log.e("", "onClick-" + view.getId());
        setDrawerView(view);
        IS_SKIP_SAME = true;
    }

    public void setDrawerView(View view) {
        if (onSliderListner != null) {
            onSliderListner.onSlide();
        }
        Log.e("", "setDrawerView-" + view.getId());
        for (int i = 0; i < _drawer_items.size(); i++) {
            if (view.getId() == _drawer_items.get(i).getId()) {
                _on_drawer_listner.onDrawerItemClick(view);
                view.setBackgroundResource(R.color.color_drawer_selector);
            } else {
                _drawer_items.get(i).setBackgroundResource(R.color.color_white);
            }
        }
        currentResourceId = view.getId();
    }

    public void setOnDrawerItemClickListner(IOnDrawerListner iondrawerlistner) {
        _on_drawer_listner = iondrawerlistner;
    }

    public void setOnSlideListner(IOnSliderListner ionsliderlistner) {
        onSliderListner = ionsliderlistner;
    }

    public static boolean IS_SKIP_SAME = true;

    public void setSelectionByResourceId(int i) {
        Log.e("", "setSelectionByResourceId-" + i);
        for (int index = 0; index < _drawer_items.size(); index++) {
            if (i == _drawer_items.get(index).getId()) {
                onClick(_drawer_items.get(index));
                return;
            }
        }
    }

    public void setItemBackgroundSelected(int i) {
        for (int index = 0; index < _drawer_items.size(); index++) {
            if (i == _drawer_items.get(index).getId()) {
                _drawer_items.get(index).setBackgroundResource(
                        R.color.color_drawer_selector);
            } else {
                _drawer_items.get(index).setBackgroundResource(
                        R.color.color_white);
            }
        }

    }
}
