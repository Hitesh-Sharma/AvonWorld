package com.avon.india.drawer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.supports.ConstantsReal;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class DrawerIntializer implements NavigationDrawer.IOnSliderListner,
        com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener,
        com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener {
    public static interface IOnDrawerStateChangeListner {
        public abstract void onStateChange(boolean isOpened);
    }

    private NavigationDrawer _mainMenu;
    private NavigationDrawerMalaysia _mainMenuMA;
    private NavigationDrawerTaiwan _mainMenuTW;
    private SlidingMenu _menuDrawer;
    private IOnDrawerStateChangeListner onDrawerStateChangeListner;
    private boolean isOpen = false;
    private int drawer_offset = 0;
    private Activity activity;

    @SuppressLint("NewApi")
    public DrawerIntializer(Activity activity, DrawerType drawerType) {
        this.activity = activity;
        int width = activity.getResources().getDisplayMetrics().widthPixels;

        drawer_offset = width / 6;

        _menuDrawer = new SlidingMenu(activity);
        _menuDrawer.setMode(0);
        _menuDrawer.setBehindOffset(drawer_offset);
        _menuDrawer.attachToActivity(activity, 1);
        _menuDrawer.setOnOpenedListener(this);
        _menuDrawer.setOnClosedListener(this);
        _menuDrawer.setFadeDegree(0.35f);
        _menuDrawer.setFadeEnabled(true);
        _mainMenu = new NavigationDrawer(activity, drawerType);
        _menuDrawer.setMenu(_mainMenu);
        _mainMenu.setOnSlideListner(this);
    }

    public View getBehindView() {
        return _menuDrawer.getBehindView();
    }

    public View findViewById(int i) {
        return _mainMenu.findViewById(i);
    }

    public SlidingMenu getSlider() {
        return _menuDrawer;
    }

    public void hideViewFromDrawer(int hideViewId, boolean status) {
        _mainMenu.hideViewFromDrawer(hideViewId, status);
    }

    @Override
    public void onClosed() {
        hideKeyboard();
        isOpen = false;
        if (onDrawerStateChangeListner != null) {
            onDrawerStateChangeListner.onStateChange(false);
        }
    }

    @Override
    public void onOpened() {
        hideKeyboard();
        isOpen = true;
        if (onDrawerStateChangeListner != null) {
            onDrawerStateChangeListner.onStateChange(true);
        }
    }

    @Override
    public void onSlide() {
        hideKeyboard();
        if (_menuDrawer != null) {
            _menuDrawer.showContent();
        }
    }

    public void setOnDrawerItemClickListner(
            NavigationDrawer.IOnDrawerListner iondrawerlistner) {
        if (_mainMenu != null) {
            _mainMenu.setOnDrawerItemClickListner(iondrawerlistner);
        }
    }

    public void setOnDrawerStateChangeListner(
            IOnDrawerStateChangeListner iondrawerstatechangelistner) {
        if (_menuDrawer != null) {
            _menuDrawer.setOnOpenedListener(this);
            _menuDrawer.setOnClosedListener(this);
            onDrawerStateChangeListner = iondrawerstatechangelistner;
        }
    }

    public void setSelectionByResourceId(int i) {
        if (_mainMenu != null) {
            _mainMenu.setSelectionByResourceId(i);
            Log.e("mainMenu is null", "no");
        } else {
            Log.e("mainMenu is null", "yes");
        }
    }

    public void setLogginedUserName(String first_name) {
        if (_mainMenu != null) {
            _mainMenu.setLogginedUserName(first_name);
        }
    }

    public void setItemBackgroundSelected(int i) {
        if (_mainMenu != null) {
            _mainMenu.setItemBackgroundSelected(i);
            Log.e("mainMenu is null", "no");
        } else {
            Log.e("mainMenu is null", "yes");
        }
    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
        }
    }

    public void setSlidingEnabled(boolean flag) {
        _menuDrawer.setSlidingEnabled(flag);
    }

    public void showContent() {
        if (_menuDrawer != null) {
            _menuDrawer.showContent();
        }
    }

    public void showMenu() {
        if (_menuDrawer != null) {
            _menuDrawer.showMenu();
        }
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public void closeDrawer() {
        hideKeyboard();
        isOpen = false;
        if (onDrawerStateChangeListner != null) {
            onDrawerStateChangeListner.onStateChange(false);
        }
    }
}
