package com.avon.india.drawer;

/**
 * Created by mukesh on 7/4/16.
 */
public enum DrawerType {

    DEFAULT, MALAYSIA, TAIWAN, PHILIPPINE
}
