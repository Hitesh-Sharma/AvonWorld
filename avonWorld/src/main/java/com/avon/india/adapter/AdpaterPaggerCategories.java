package com.avon.india.adapter;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentCategoryItems;
import com.avon.india.fragments.FragmentSaving;
import com.avon.india.interfaces.IOnFragmentUpdated;
import com.avon.india.itemproperty.Category;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerCategories extends FragmentPagerAdapter implements
        IconPagerAdapter {
    private IOnFragmentUpdated _iOnFragmentUpdated;
    private int mCount;
    private int lastposition;
    private List<Category> categories;

    public AdpaterPaggerCategories(FragmentManager fm,
                                   IOnFragmentUpdated _iOnFragmentUpdated, List<Category> categories) {
        super(fm);
        this._iOnFragmentUpdated = _iOnFragmentUpdated;
        this.categories = categories;
        mCount = categories.size();
        lastposition = mCount * 2;
    }

    @Override
    public Fragment getItem(int position) {

        if (position % 2 == 1 || position == lastposition) {
            final FragmentSaving fr = new FragmentSaving();
            if (position == lastposition)
                fr.setPosition(position, true);
            else
                fr.setPosition(position, false);
            fr.setOnFragmentUpdated(_iOnFragmentUpdated);
            return fr;
        } else {
            if (position > 0)
                position = position / 2;
            Category category = categories.get(position);
            return FragmentCategoryItems.getInstance(
                    category.getCategoryName(), category.getCategoryId());
        }
    }

    @Override
    public int getCount() {
        return (mCount * 2) + 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getIconResId(int index) {
        return 0;
    }
}
