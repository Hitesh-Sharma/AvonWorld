package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.enums.EnumSet.ImageToShowOf;
import com.avon.india.fragments.FragmentOfferDescription;
import com.avon.india.itemproperty.Offers;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerOffers extends FragmentPagerAdapter implements
		IconPagerAdapter {
	private List<Offers> offers_list;

	public AdpaterPaggerOffers(FragmentManager fm, List<Offers> offers_list) {
		super(fm);
		this.offers_list = offers_list;
	}

	@Override
	public Fragment getItem(int position) {
		if (offers_list == null)
			return null;
		FragmentOfferDescription fragment = new FragmentOfferDescription();
		fragment.setImageToShowFor(ImageToShowOf.CURRENT_OFFERS,
				offers_list.get(position), null);
		return fragment;
	}

	@Override
	public int getCount() {
		try {
			return offers_list.size();
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			notifyDataSetChanged();
		}
	}

}
