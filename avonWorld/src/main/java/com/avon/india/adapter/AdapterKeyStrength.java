package com.avon.india.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.itemproperty.KeyStrengthsList;

import java.util.List;

public class AdapterKeyStrength extends ExtendedBaseAdapter {
	private List<KeyStrengthsList> keyStrengthList;

	public AdapterKeyStrength(Activity activity,
			List<KeyStrengthsList> keyStrengthList) {
		super(activity);
		this.keyStrengthList = keyStrengthList;

	}

	@Override
	public int getCount() {
		return keyStrengthList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View rootview, ViewGroup parent) {
		ViewHolder holder;
		View view = rootview;
		if (view == null) {
			view = inflate(R.layout.adapter_key_strength);
			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		// view.setBackgroundColor(keyStrengthList.get(position).getColorCode());
		holder.txt_headings.setText(keyStrengthList.get(position)
				.getTxt_Headings());
		holder.txt_key_number.setText((position + 1) + ". ");
		return view;
	}

	class ViewHolder {
		TextView txt_headings, txt_key_number;

		public ViewHolder(View view) {
			txt_headings = (TextView) view.findViewById(R.id.txt_key_heading);
			txt_key_number = (TextView) view.findViewById(R.id.txt_key_number);

		}
	}

}
