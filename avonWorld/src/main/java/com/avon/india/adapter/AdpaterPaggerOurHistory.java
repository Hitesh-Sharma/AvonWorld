package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentOurHistoryDescription;
import com.avon.india.itemproperty.OurHistory;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerOurHistory extends FragmentPagerAdapter implements
		IconPagerAdapter {
	List<OurHistory> listOurHistory;

	public AdpaterPaggerOurHistory(FragmentManager fm,
			List<OurHistory> listOurHistory) {
		super(fm);
		this.listOurHistory = listOurHistory;
	}

	@Override
	public Fragment getItem(int position) {
		if (listOurHistory == null)
			return null;
		FragmentOurHistoryDescription fragment = new FragmentOurHistoryDescription();
		fragment.setSelectedListPosition(position, listOurHistory);
		return fragment;
	}

	@Override
	public int getCount() {
		try {
			return listOurHistory.size();
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

}
