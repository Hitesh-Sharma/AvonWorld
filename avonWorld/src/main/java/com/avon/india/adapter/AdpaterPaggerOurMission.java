package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentOurMissionDescription;
import com.avon.india.itemproperty.OurMission;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerOurMission extends FragmentPagerAdapter implements
		IconPagerAdapter {
	List<OurMission> listOurMission;

	public AdpaterPaggerOurMission(FragmentManager fm,
			List<OurMission> listOurHistory) {
		super(fm);
		this.listOurMission = listOurHistory;
	}

	@Override
	public Fragment getItem(int position) {
		if (listOurMission == null)
			return null;
		FragmentOurMissionDescription fragment = new FragmentOurMissionDescription();
		fragment.setSelectedListPosition(position, listOurMission);
		return fragment;
	}

	@Override
	public int getCount() {
		try {
			return listOurMission.size();
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

}
