package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentKeyStrengthDescription;
import com.avon.india.itemproperty.KeyStrengthsList;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerKeyStrengthDetail extends FragmentPagerAdapter
		implements IconPagerAdapter {
	List<KeyStrengthsList> keyStrengthList;

	public AdpaterPaggerKeyStrengthDetail(FragmentManager fm,
			List<KeyStrengthsList> keyStrengthList) {
		super(fm);
		this.keyStrengthList = keyStrengthList;
	}

	@Override
	public Fragment getItem(int position) {
		if (keyStrengthList == null)
			return null;
		FragmentKeyStrengthDescription fragment = new FragmentKeyStrengthDescription();
		fragment.setSelectedListPosition(position, keyStrengthList);
		return fragment;
	}

	@Override
	public int getCount() {
		try {
			return keyStrengthList.size();
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

}
