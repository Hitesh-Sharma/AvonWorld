package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentHistoryDescription;

public class AdapterPaggerHistory extends FragmentPagerAdapter {

    private static final String[] CONTENT = new String[]{"ONE", "TWO",
            "THREE", "FOUR", "FIVE"};

    //	private static final int[] ICONS = new int[] { R.drawable.our_history_a,
//			R.drawable.our_history_b, R.drawable.our_history_c,
//			R.drawable.our_history_d, R.drawable.our_history_e };
    private static final int[] ICONS = new int[5];

    public AdapterPaggerHistory(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new FragmentHistoryDescription(ICONS[position]);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return CONTENT.length;

    }
}