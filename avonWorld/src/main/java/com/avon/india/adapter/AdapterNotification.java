package com.avon.india.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.itemproperty.Notifications;
import com.avon.india.supports.DateTimeUtils;
import com.avon.india.supports.SupportUtils;

import java.util.List;

public class AdapterNotification extends ExtendedBaseAdapter {

	List<Notifications> listNotifications;

	public AdapterNotification(Activity activity,
			List<Notifications> listNotifications) {
		super(activity);
		this.listNotifications = listNotifications;
	}

	@Override
	public int getCount() {
		return listNotifications.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View rootView, ViewGroup parent) {

		ViewHolder viewHolder;
		View view = rootView;
		if (view == null) {
			view = inflate(R.layout.adapter_notification);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		Notifications notification = listNotifications.get(position);
		String description = notification.getDescription();
		String time = notification.getTime();
		String title = notification.getTitle();

		if (SupportUtils.isValidString(title)) {
			viewHolder.txt_view_title.setText(title);
		}

		if (SupportUtils.isValidString(description)) {
			viewHolder.txt_view_description.setText(description);
		}

		if (SupportUtils.isValidString(time)) {
			String[] splitTimeDate = time.split("T");
			if (splitTimeDate != null && splitTimeDate.length == 2) {
				String notificationDate = DateTimeUtils
						.getValidDay(splitTimeDate[1]);
				if (checkFor(position)) {
					viewHolder.txt_view_date.setVisibility(View.GONE);
				} else {
					viewHolder.txt_view_date.setVisibility(View.VISIBLE);
					viewHolder.txt_view_date.setText(notificationDate);
				}
			}
		}
		return view;
	}

	private boolean checkFor(int position) {
		boolean isSameDate = false;
		try {
			if (position != 0) {
				String prevDate = "", currentDate = "";
				prevDate = (listNotifications.get(position - 1).getTime())
						.split("T")[1];
				currentDate = listNotifications.get(position).getTime()
						.split("T")[1];
				Log.e("", "prevDate " + prevDate + " currentDate "
						+ currentDate);
				isSameDate = DateTimeUtils.isOfSameDate(prevDate, currentDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSameDate;
	}

	static class ViewHolder {
		final TextView txt_view_description, txt_view_date, txt_view_title;

		public ViewHolder(View view) {
			txt_view_description = (TextView) view
					.findViewById(R.id.txt_view_description);
			txt_view_date = (TextView) view.findViewById(R.id.txt_view_date);
			txt_view_title = (TextView) view.findViewById(R.id.txt_view_title);
		}

	}

}
