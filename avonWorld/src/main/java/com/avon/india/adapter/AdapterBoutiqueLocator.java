package com.avon.india.adapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.customviews.CustomTextViewHelviticaLight;
import com.avon.india.fragments.FragmentBuitiqueLocation;
import com.avon.india.itemproperty.LocationItem;
import com.avon.india.supports.ConstantsReal.IConstants;
import com.avon.india.supports.SupportUtils;

import java.util.List;

public class AdapterBoutiqueLocator extends ExtendedBaseAdapter {
    List<LocationItem> _location_list;
    FragmentBuitiqueLocation fragment;

    public AdapterBoutiqueLocator(FragmentBuitiqueLocation fragment,
                                  List<LocationItem> location_list) {
        super(Avon.getInstance().getActivityInstance());
        this.fragment = fragment;
        this._location_list = location_list;
    }

    @Override
    public int getCount() {
        if (_location_list == null)
            return 0;
        return _location_list.size();
    }

    @Override
    public LocationItem getItem(int position) {
        return _location_list.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int position, View rootView, ViewGroup viewGroup) {
        ViewHolder holder;
        View view = rootView;
        if (view == null) {
            view = inflate(R.layout.adapter_boutique_location);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final LocationItem item_adress = _location_list.get(position);
        holder.txt_vwstore_name.setText(item_adress.getStoreName());
        holder.txt_vw_store_address.setText(item_adress.getLocationAddress());
        if (item_adress.getDistance() == null || item_adress.getDistance().trim().length() == 0)
            holder.txt_store_distance.setVisibility(View.GONE);
        else {
            holder.txt_store_distance.setVisibility(View.VISIBLE);
            holder.txt_store_distance.setText(item_adress.getDistance());
        }
        // String image_url = item_adress.getStoreImageUrl();
        // if (SupportUtils.isValidString(image_url)) {
        // Avon.getInstance().getThumbImageLoader()
        // .startLazyLoading(image_url, holder.img_vw_location_map);
        // }

        holder.img_vw_call.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String phoneNumber = _location_list.get(position)
                        .getMobileNumber();
                try {
                    SupportUtils.actionMakeCall(phoneNumber);
                } catch (Exception e) {

                }
            }
        });

        if (position == _location_list.size() - 1) {
            if (_location_list.size() % IConstants.MAX_COUNT == 0) {
                int pageNo = (position + 1) / IConstants.MAX_COUNT;
                fragment.loadButiqueLocation(pageNo + 1);
            }
        }
        return view;
    }

    private static class ViewHolder {
        final ImageView img_vw_call, img_vw_location_map;
        final CustomTextViewHelviticaLight txt_vwstore_name;
        final CustomTextViewHelviticaLight txt_vw_store_address,
                txt_store_distance;

        public ViewHolder(View view) {
            txt_vwstore_name = (CustomTextViewHelviticaLight) view
                    .findViewById(R.id.txt_vw_store_name);
            txt_vw_store_address = (CustomTextViewHelviticaLight) view
                    .findViewById(R.id.txt_vw_store_address);
            txt_store_distance = (CustomTextViewHelviticaLight) view
                    .findViewById(R.id.txt_store_distance);
            img_vw_call = (ImageView) view.findViewById(R.id.img_vw_call);
            img_vw_location_map = (ImageView) view
                    .findViewById(R.id.img_vw_location_map);

        }
    }

}