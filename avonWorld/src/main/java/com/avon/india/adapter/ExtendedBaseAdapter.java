package com.avon.india.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class ExtendedBaseAdapter extends BaseAdapter {

	// Defining Layout Infaltor
	private LayoutInflater _infaltor;

	/**
	 * @author Android Lead
	 * @param activity
	 *            Activity list of NotificatioItem
	 * */
	public ExtendedBaseAdapter(Activity activity) {
		_infaltor = LayoutInflater.from(activity);
	}

	/**
	 * @author Android Lead
	 * @param resource
	 *            Layout resource
	 * 
	 * */
	public View inflate(int resource) {
		return _infaltor.inflate(resource, null);
	}

	@Override
	public abstract int getCount();

	@Override
	public abstract Object getItem(int arg0);

	@Override
	public abstract long getItemId(int arg0);

	@Override
	public abstract View getView(int arg0, View arg1, ViewGroup arg2);

}
