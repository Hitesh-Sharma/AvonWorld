package com.avon.india.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.itemproperty.States;

import java.util.List;

public class AdapterStateFilterList extends BaseAdapter {
	List<States> _list_states;
	Context _context;
	LayoutInflater inflater;

	public AdapterStateFilterList(Context context, List<States> list_states) {
		_context = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		_context = context;
		_list_states = list_states;
	}

	@Override
	public int getCount() {
		if (_list_states != null)
			return _list_states.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) {
		if (_list_states != null)
			return _list_states.get(position);
		else
			return 0;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public final TextView textview;

		ViewHolder(View convertView) {
			textview = (TextView) convertView.findViewById(R.id.txt_view_state);
		}
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		View view = convertView;
		if (view != null && view.getId() == R.id.linear_states) {
			viewHolder = (ViewHolder) view.getTag();
		} else {
			view = inflater.inflate(R.layout.adapter_state, parent, false);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
		}
		viewHolder.textview.setText(_list_states.get(position).getStateName());

		return view;
	}

}
