package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentBuitiqueAddress;
import com.avon.india.itemproperty.LocationItem;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerBuitiques extends FragmentPagerAdapter implements
		IconPagerAdapter {
	private List<LocationItem> location_list;

	public AdpaterPaggerBuitiques(FragmentManager fm,
			List<LocationItem> location_list) {
		super(fm);
		this.location_list = location_list;
	}

	@Override
	public Fragment getItem(int position) {
		if (location_list == null)
			return null;
		if (location_list.size() < position) {
			return FragmentBuitiqueAddress.newInstance(
					location_list.get(location_list.size() - 1), position - 1);

		}
		return FragmentBuitiqueAddress.newInstance(location_list.get(position),
				position);

	}

	@Override
	public int getCount() {
		try {
			return location_list.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

	public int getItemPosition(Object object) {
		FragmentBuitiqueAddress fragment = (FragmentBuitiqueAddress) object;
		LocationItem title = fragment.getLocation();
		int position = location_list.indexOf(title);
		if (position >= 0) {
			return position;
		} else {
			return POSITION_NONE;
		}
	}
}
