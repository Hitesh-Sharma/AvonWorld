package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.fragments.FragmentSelectedCategoryItems;
import com.viewpagerindicator.IconPagerAdapter;

public class AdpaterPaggerItemsDetail extends FragmentPagerAdapter implements
		IconPagerAdapter {

	private int mCount;
	private String item_category;
	private int _category_id;

	// private boolean isToLoadMore;

	public AdpaterPaggerItemsDetail(FragmentManager fm, String item_category,
			int _category_id, int mCount) {
		super(fm);
		this.item_category = item_category;
		this.mCount = mCount;
		this._category_id = _category_id;
	}

	@Override
	public Fragment getItem(int position) {
		// if (position == mCount - 1 && isToLoadMore) {
		// return new FragmentItemDetailsLoading();
		// } else {
		FragmentSelectedCategoryItems fragment = new FragmentSelectedCategoryItems();
		fragment.setItemPosition(position, item_category, _category_id);
		return fragment;
		// }
	}

	// public void loadMoreItems(boolean isToLoadMore) {
	// this.isToLoadMore = isToLoadMore;
	// }

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

	public void setCount(int count) {
		mCount = count;
		notifyDataSetChanged();
	}

}
