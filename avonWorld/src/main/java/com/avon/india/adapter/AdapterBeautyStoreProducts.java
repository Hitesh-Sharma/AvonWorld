package com.avon.india.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.fragments.paggers.FragmentPaggerBeautyStore;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.ConstantsReal.IConstants;

import java.util.List;

public class AdapterBeautyStoreProducts extends BaseAdapter {
    List<CategoryItem> _list_products;
    Context _context;
    LayoutInflater inflater;
    private FragmentPaggerBeautyStore fragment;

    public AdapterBeautyStoreProducts(Context context,
                                      List<CategoryItem> list_products, FragmentPaggerBeautyStore fragment) {
        _context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        _context = context;
        _list_products = list_products;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return _list_products.size();
    }

    @Override
    public Object getItem(int position) {
        return _list_products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View view = convertView;
        if (view != null && view.getId() == R.id.linear_states) {
            viewHolder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.adapter_state, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }
        viewHolder.textview.setText(_list_products.get(position).getName());

        if (position == _list_products.size() - 1) {
            if (_list_products.size() % IConstants.MAX_COUNT == 0) {
                fragment.callAsync();
            }
        }
        if (_list_products.get(position).isSelected())
            view.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources().getColor(R.color.color_drawer_selector));
        else
            view.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources().getColor(R.color.color_transparent));

        return view;
    }

    public static class ViewHolder {
        public final TextView textview;

        ViewHolder(View convertView) {
            textview = (TextView) convertView.findViewById(R.id.txt_view_state);
        }
    }

}
