package com.avon.india.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.itemproperty.Country;
import com.avon.india.supports.AppLogger;

import java.util.List;

public class AdapterSelectCountry extends ExtendedBaseAdapter {

    private List<Country> listCountry;

    public AdapterSelectCountry(Activity activity, List<Country> listCountry) {
        super(activity);
        this.listCountry = listCountry;
    }

    @Override
    public int getCount() {
        return listCountry.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rootView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View view = rootView;
        if (view == null) {
            view = inflate(R.layout.adapter_spinner_two);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.txt_view_number.setText(""
                + listCountry.get(position).getCountryName());
        viewHolder.radio_button_selected.setVisibility(View.GONE);

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewHolder.txt_view_number.getLayoutParams();
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        viewHolder.txt_view_number.setLayoutParams(lp);


        if (listCountry.get(position).isSelected()) {
            view.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources().getColor(R.color.color_drawer_selector));
        } else
            view.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources().getColor(R.color.color_transparent));
        return view;
    }

    static class ViewHolder {
        final TextView txt_view_number, radio_button_selected;

        public ViewHolder(View view) {
            txt_view_number = (TextView) view
                    .findViewById(R.id.txt_view_number);
            radio_button_selected = (TextView) view
                    .findViewById(R.id.radio_button_selected);
        }
    }

}
