package com.avon.india.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.R;

public class AdapterSpinnerTwo extends ExtendedBaseAdapter {

	private int size = 0;

	public AdapterSpinnerTwo(Activity activity, int size) {
		super(activity);
		this.size = size;
	}

	@Override
	public int getCount() {
		return size;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View rootView, ViewGroup parent) {
		final ViewHolder viewHolder;
		View view = rootView;
		if (view == null) {
			view = inflate(R.layout.adapter_spinner_two);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		viewHolder.txt_view_number.setText("" + position);

		return view;
	}

	static class ViewHolder {
		final TextView txt_view_number;

		public ViewHolder(View view) {
			txt_view_number = (TextView) view
					.findViewById(R.id.txt_view_number);
		}
	}

}
