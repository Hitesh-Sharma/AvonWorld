package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.R;
import com.avon.india.fragments.FragmentHistoryDescription;

public class AdapterPaggerOurMission extends FragmentPagerAdapter {

	private static final String[] CONTENT = new String[] { "ONE", "TWO",
			"THREE" };

	private static final int[] ICONS = new int[] { R.drawable.our_mission,
			R.drawable.our_vision, R.drawable.our_values };

	public AdapterPaggerOurMission(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return new FragmentHistoryDescription(ICONS[position]);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getCount() {
		return CONTENT.length;

	}
}