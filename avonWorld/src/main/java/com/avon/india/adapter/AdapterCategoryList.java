package com.avon.india.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.LazyLoaderLib.ILazyDownloaderListner;
import com.avon.india.LazyLoaderLib.ScaleImageAccordingly;
import com.avon.india.R;
import com.avon.india.dialogs.DialogFragmentSpinnerTwo;
import com.avon.india.interfaces.IOnPagginationCompleted;
import com.avon.india.interfaces.IOnSpinnerItemSelectedListener;
import com.avon.india.interfaces.IOnscrollUp;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.IConstants;
import com.avon.india.supports.SupportUtils;

import java.io.File;
import java.util.List;

public class AdapterCategoryList extends ExtendedBaseAdapter {
    private List<CategoryItem> _item_list;
    private IOnscrollUp onScrollUpListner;
    private IOnPagginationCompleted ionPagging;
    private int MAX_COUNT = IConstants.MAX_COUNT;
//    private int IMAGE_WIDTH = 0, IMAGE_HEIGHT = 0;
//
//    public void getSizeOfScreen(Activity activity) {
//        Display display = activity.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        IMAGE_WIDTH = dp2px(activity, size.x);
//        IMAGE_HEIGHT = dp2px(activity, size.y);
//        Log.e("IMAGE_WIDTH , IMAGE_HEIGHT", IMAGE_WIDTH + " , " + IMAGE_HEIGHT);
//    }
//
//    private int dp2px(Activity activity, int dp) {
//        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
//                activity.getResources().getDisplayMetrics());
//    }

    public AdapterCategoryList(Activity activity,
                               List<CategoryItem> itemList) {
        super(activity);
        this._item_list = itemList;
//        getSizeOfScreen(activity);
//        try {
//            if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA))
//                MAX_COUNT = IConstants.MAX_MAX_COUNT;
//        } catch (Exception e) {
//        }
    }

    @Override
    public int getCount() {
        if (_item_list == null)
            return 0;
        return _item_list.size();
    }

    @Override
    public BaseCategoryItem getItem(int location) {
        return _item_list.get(location);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int location, View contentView, ViewGroup arg2) {
        ViewHolder holder;
        View view = contentView;
        if (view == null) {
            view = inflate(R.layout.adapter_category_item_test);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final BaseCategoryItem item = _item_list.get(location);

        String item_name = item.getName();
        int item_quantity = item.getItemQuantity();
        String image_url = null;
//        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
//        if (country_name != null) {
//            if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
//                image_url = item.getFullImageUrl();
//            } else image_url = item.getImageUrl();
//        } else {
        image_url = item.getImageUrl();
//        downloadImage(holder, image_url);
//        }

        if (item_name != null && !item_name.trim().equalsIgnoreCase(""))
            holder._txt_name.setText(item_name);

        if (item_quantity == 0) {
            holder._txt_drop_down.setHint("QTY");
            holder._txt_drop_down.setText("");
        } else {
            holder._txt_drop_down.setText("" + Integer.toString(item_quantity));
        }
        holder._txt_drop_down.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerTwo(item, location);
            }
        });

        // Avon.getInstance().getLocalImageLoader()
//         .loadImage(item.getImageResourceId(), holder._img_item_image);
        if (image_url != null && !image_url.trim().equalsIgnoreCase("")) {
            // Avon.getInstance().getImageLoader()
            // .startLazyLoading(image_url, holder._img_item_image);
            Avon.getInstance().getFullImageLoader()
                    .startLazyLoading(image_url, holder._img_item_image);
        }

        // if (item.getImageResourceId() == -1)
        // Log.e("", "image not found for-" + item.getName());

        if (location == _item_list.size() - 1) {
            if (_item_list.size() % MAX_COUNT == 0) {
                int pageNo = (location + 1) / MAX_COUNT;
                if (pageNo == 0) {
                    pageNo = 1;
                }
                Log.e("page NO", "" + pageNo);
                if (onScrollUpListner != null && ionPagging != null)
                    onScrollUpListner.onScrolledUp(_item_list.size(),
                            ionPagging, true);
            }
        }
        return view;
    }

//    private void downloadImage(final ViewHolder holder, final String url) {
//        File file = SupportUtils.getFilepathFromUrl(url);
//        if (file != null) {
//            if (file.exists()) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                ScaleImageAccordingly scaleBitmap = new ScaleImageAccordingly(
//                        IMAGE_WIDTH, IMAGE_HEIGHT);
//                if (myBitmap == null)
//                    return;
//                Bitmap returnedBitmap = scaleBitmap.scaleBitmapForFullImage(myBitmap);
//                holder._img_item_image.setImageBitmap(returnedBitmap);
//            } else {
//                holder._img_item_image.setColorFilter(Color.argb(255, 255, 255, 255));
//                Log.e("file.exists() else", "file.exists() else");
//                Avon.getInstance()
//                        .getDownloadImage(Avon.getInstance()
//                                .getActivityInstance())
//                        .registerLoadingCompleteListner(
//                                new ILazyDownloaderListner() {
//
//                                    @Override
//                                    public void onLaziDownloadingStarts() {
//
//                                    }
//
//                                    @Override
//                                    public void onLaziDownloadingComplete(
//                                            boolean isSuccess, File file) {
//                                        if (isSuccess) {
//                                            File file2 = SupportUtils
//                                                    .getFilepathFromUrl(url);
//                                            if (file != null) {
//                                                if (file.exists()) {
//                                                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                                    if (myBitmap == null)
//                                                        return;
//                                                    ScaleImageAccordingly scaleBitmap = new ScaleImageAccordingly(
//                                                            IMAGE_WIDTH, IMAGE_HEIGHT);
//                                                    Bitmap returnedBitmap = scaleBitmap.scaleBitmapForFullImage(myBitmap);
//                                                    holder._img_item_image.setImageBitmap(returnedBitmap);
//                                                }
//                                            } else {
//                                                // Bitmap resized =
//                                                // ThumbnailUtils.extractThumbnail(
//                                                // BitmapFactory
//                                                // .decodeFile(file
//                                                // .getPath()),
//                                                // 50, 50);
//                                                // holder.img_message_chat_cell
//                                                // .setImageBitmap(resized);
//                                                // holder.img_view_retry_dwonload
//                                                // .setVisibility(View.GONE);
//                                            }
//                                        } else {
//                                            Log.e("isSuccess", "isSuccess "
//                                                    + isSuccess);
//                                        }
//                                    }
//                                });
//                Avon.getInstance()
//                        .getDownloadImage(
//                                Avon.getInstance()
//                                        .getActivityInstance())
//                        .startLazyDownloading(url, file);
//            }
//        }
//    }

    private void showSpinnerTwo(final BaseCategoryItem item, final int location) {
        DialogFragmentSpinnerTwo dialogSpinner = new DialogFragmentSpinnerTwo();
        dialogSpinner
                .setOnItemSelectedListener(new IOnSpinnerItemSelectedListener() {

                    @Override
                    public void onItemSelected(int itemQuantity) {
                        Avon.getPurchase().addToPurchase(
                                _item_list.get(location));
                        item.setItemQuantity(itemQuantity);
                        notifyDataSetChanged();
                    }
                });
        Avon.getInstance().getFlowOrganization().add(dialogSpinner, true);
    }

    // private void showSpinner(final Product_details item, final int location)
    // {
    // DialogFragmentSpinner dialogSpinner = new DialogFragmentSpinner();
    // dialogSpinner.setTitleandMessage(item.getName(), item.getImageUrl());
    // dialogSpinner
    // .setOnItemSelectedListener(new IOnSpinnerItemSelectedListener() {
    //
    // @Override
    // public void onItemSelected(int itemQuantity) {
    // Avon.getPurchase().addToPurchase(
    // _item_list.get(location));
    // item.setItem_quantity(itemQuantity);
    // notifyDataSetChanged();
    // }
    // });
    // Avon.getInstance().getFlowOrganization()
    // .addWithAnimation(dialogSpinner, null, true);
    // }

    private static class ViewHolder {
        final TextView _txt_name;
        final TextView _txt_drop_down;
        final ImageView _img_item_image;

        public ViewHolder(View view) {
            _txt_name = (TextView) view.findViewById(R.id.txt_name_grid_item);
            _txt_drop_down = (TextView) view.findViewById(R.id.txt_quantitiy);
            _img_item_image = (ImageView) view
                    .findViewById(R.id.img_thumb_adapter_grid_item);
        }
    }

    public void setOnScrollUpListner(IOnscrollUp onScrollUpListner) {
        this.onScrollUpListner = onScrollUpListner;
    }

    public void setOnPagginationLoaded(IOnPagginationCompleted ionPagging) {
        this.ionPagging = ionPagging;
    }

}
