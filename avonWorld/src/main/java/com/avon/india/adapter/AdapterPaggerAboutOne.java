package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.R;
import com.avon.india.fragments.FragmentHistoryDescription;

public class AdapterPaggerAboutOne extends FragmentPagerAdapter {

    private static final String[] CONTENT = new String[]{"ONE"};

    private static final int[] ICONS = new int[]{R.drawable.our_mission};

    public AdapterPaggerAboutOne(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new FragmentHistoryDescription(ICONS[position]);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return CONTENT.length;

    }
}