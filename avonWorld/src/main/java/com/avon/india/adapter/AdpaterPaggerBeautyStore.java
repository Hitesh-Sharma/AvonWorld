package com.avon.india.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avon.india.enums.EnumSet.ImageToShowOf;
import com.avon.india.fragments.FragmentOfferDescription;
import com.avon.india.itemproperty.CategoryItem;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;

public class AdpaterPaggerBeautyStore extends FragmentPagerAdapter implements
		IconPagerAdapter {
	private List<CategoryItem> location_list;

	public AdpaterPaggerBeautyStore(FragmentManager fm,
			List<CategoryItem> location_list) {
		super(fm);
		this.location_list = location_list;
	}

	@Override
	public Fragment getItem(int position) {
		if (location_list == null)
			return null;
		FragmentOfferDescription fragment = new FragmentOfferDescription();
		fragment.setImageToShowFor(ImageToShowOf.BEAUTY_STORE, null,
				location_list.get(position));
		return fragment;
	}

	@Override
	public int getCount() {
		try {
			return location_list.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}

	@Override
	public int getIconResId(int index) {
		return 0;
	}

}
