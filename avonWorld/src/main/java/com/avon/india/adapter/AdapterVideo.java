package com.avon.india.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.itemproperty.VideoItem;
import com.avon.india.supports.SupportUtils;

import java.util.List;

public class AdapterVideo extends ExtendedBaseAdapter {
    List<VideoItem> _video_item;

    public AdapterVideo(Activity activity, List<VideoItem> video_item) {
        super(Avon.getInstance().getActivityInstance());
        this._video_item = video_item;
    }

    @Override
    public int getCount() {
        if (_video_item == null)
            return 0;
        return _video_item.size();
    }

    @Override
    public VideoItem getItem(int position) {

        return _video_item.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View rootview, ViewGroup viewGroup) {
        ViewHolder holder;
        View view = rootview;
        if (view == null) {
            view = inflate(R.layout.adapter_video_fragment);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String videoName = _video_item.get(position).getName_of_video();
        String videoTime = _video_item.get(position).getTime_of_video();
        String videoThumb = _video_item.get(position).getThumb_image();
        holder.img_thumb_image.setImageBitmap(null);
        if (SupportUtils.isValidString(videoName))
            holder.txt_name_of_video.setText(videoName);

        if (SupportUtils.isValidString(videoTime))
            holder.txt_time_of_video.setText(videoTime);

        if (SupportUtils.isValidString(videoThumb)) {
            Avon.getInstance().getThumbImageLoader()
                    .startLazyLoading(videoThumb, holder.img_thumb_image);
            // Avon.getInstance().getImageLoader()
            // .startLazyLoading(videoThumb, holder.img_thumb_image);
        } else {
            holder.img_thumb_image.setImageBitmap(null);
        }
        /*
         * if (Avon.getInstance().getImageLoader() != null) { Avon.getInstance()
		 * .getImageLoader() .startLazyLoading(
		 * _video_item.get(position).getThumb_image(), holder.img_thumb_image);
		 * 
		 * Avon.getInstance().getImageLoader()
		 * .registerLoadingCompleteListner(new ILazyLoaderListner() {
		 * 
		 * @Override public void onLaziLoadingStarts() { }
		 * 
		 * @Override public void onLaziLoadingComplete(boolean isSuccess) { }
		 * }); }
		 */

        return view;
    }

    static class ViewHolder {
        final TextView txt_name_of_video;
        final TextView txt_time_of_video;
        final ImageView img_thumb_image;

        public ViewHolder(View view) {
            txt_name_of_video = (TextView) view
                    .findViewById(R.id.txt_vw_video_name);
            txt_time_of_video = (TextView) view
                    .findViewById(R.id.txt_vw_video_tym);
            img_thumb_image = (ImageView) view
                    .findViewById(R.id.img_thumb_image);
        }
    }
}
