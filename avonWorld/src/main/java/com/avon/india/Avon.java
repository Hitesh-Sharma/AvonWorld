package com.avon.india;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.SparseArray;

import com.avon.india.LazyLoaderLib.DownloadImageFromUrl;
import com.avon.india.LazyLoaderLib.ImageType;
import com.avon.india.LazyLoaderLib.LocalImageLoader;
import com.avon.india.LazyLoaderLib.PhoneSpecificImage;
import com.avon.india.R;
import com.avon.india.db.Constants.DBConstant;
import com.avon.india.db.DatabaseConfiguration;
import com.avon.india.drawer.DrawerIntializer;
import com.avon.india.drawer.DrawerType;
import com.avon.india.drawer.NavigationDrawer;
import com.avon.india.enums.EnumSet.CountryReason;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.Category;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.LoadProducts;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.FlowOrganizer;
import com.avon.india.supports.FusedLocationManager;
import com.avon.india.supports.PurchaseManager;

import java.util.List;

public class Avon extends Application {

    private static Avon _testPro;
    private AvonMainActivity activity;
    private FlowOrganizer _flowOrganize;
    private FusedLocationManager _location_manager;
    private DrawerIntializer _drawer;
    private DrawerIntializer _drawerPH;
    private DrawerIntializer _drawerTiawan;
    private DrawerIntializer _drawerMalaysia;
    private DatabaseConfiguration _db_configuration;
    private SparseArray<List<CategoryItem>> productsItemsList;
    private List<Category> categoriesList;
    private List<LoadProducts> listLoadProductsStatus;
    private LocalImageLoader _local_image_loader;
    private PhoneSpecificImage fullImageLoader;
    private PhoneSpecificImage thumbImageLoader;
    private static PurchaseManager purchase;
    private CountryReason coutryReason = CountryReason.EAST;
    private boolean isSplashSelectionDone = false;
    public static boolean ON_DEBUG_MODE = true;
    private static boolean isAlreadyDownloadingImages = false;
    private DownloadImageFromUrl downloadImage;

    public boolean isOkDialogShown() {
        return isOkDialogShown;
    }

    public void setIsOkDialogShown(boolean isOkDialogShown) {
        this.isOkDialogShown = isOkDialogShown;
    }

    private boolean isOkDialogShown=true;

    public static Avon getInstance() {
        if (_testPro == null)
            _testPro = new Avon();
        return _testPro;
    }

    public static PurchaseManager getPurchase() {
        if (purchase == null)
            purchase = new PurchaseManager();
        return purchase;
    }

    public static boolean isAlreadyDownloadingImages() {
        return isAlreadyDownloadingImages;
    }

    public static void setIsAlreadyDownloadingImages(boolean isAlreadyDownloadingImages) {
        Avon.isAlreadyDownloadingImages = isAlreadyDownloadingImages;
    }

    public FlowOrganizer getFlowOrganization() {
        if (_flowOrganize == null)
            _flowOrganize = new FlowOrganizer(activity, R.id.frame_parrent);
        return _flowOrganize;
    }

    public AvonMainActivity getActivityInstance() {
        return activity;
    }

    public DrawerIntializer getDrawerInstance(Activity _activity) {
        String country_name = AppPrefrence.getInstance(_activity).getMyCountryName();
        if (country_name == null || country_name.trim().equals("")) {
            AppLogger.e("country_name is", "country_name null");
            return getDrawerInstanceDefault(activity);
        } else {
            AppLogger.e("country_name is", "country_name " + country_name);
            if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
                return getDrawerInstanceDefault(activity);
            } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
                return getDrawerInstanceTiawan(activity);
            } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
                return getDrawerInstanceMalaysia(activity);
            } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES)) {
                return getDrawerInstancePhilippin(activity);
            }
        }
        return getDrawerInstanceDefault(activity);
    }

    public DrawerIntializer getDrawerInstanceDefault(Activity _activity) {
        AppLogger.e("getDrawerInstance is", "getDrawerInstanceDefault ");
        if (_drawer == null)
            _drawer = new DrawerIntializer(activity, DrawerType.DEFAULT);
        return _drawer;
    }

    public DrawerIntializer getDrawerInstancePhilippin(Activity _activity) {
        AppLogger.e("getDrawerInstance is", "getDrawerInstanceDefault ");
        if (_drawerPH == null)
            _drawerPH = new DrawerIntializer(activity, DrawerType.PHILIPPINE);
        return _drawerPH;
    }

    public DrawerIntializer getDrawerInstanceMalaysia(Activity _activity) {
        AppLogger.e("getDrawerInstance is", "getDrawerInstanceMalaysia ");
        if (_drawerMalaysia == null)
            _drawerMalaysia = new DrawerIntializer(activity, DrawerType.MALAYSIA);
        return _drawerMalaysia;
    }

    public DrawerIntializer getDrawerInstanceTiawan(Activity _activity) {
        AppLogger.e("getDrawerInstance is", "getDrawerInstanceTiawan ");
        if (_drawerTiawan == null)
            _drawerTiawan = new DrawerIntializer(activity, DrawerType.TAIWAN);
        return _drawerTiawan;
    }


    public FusedLocationManager getFusedLocationManagerInsance() {
        if (_location_manager == null) {
            _location_manager = new FusedLocationManager(activity);
        }
        return _location_manager;
    }

    public void initDB() {
        _db_configuration = new DatabaseConfiguration.Builder(activity)
                .setName(DBConstant.DB_NAME).build();
        _db_configuration.createDataBase();
    }

    public void initActivity(AvonMainActivity activity) {
        this.activity = activity;
        _location_manager = new FusedLocationManager(activity);
    }

    public DatabaseConfiguration getDatabaseInstance(Context context) {
        if (_db_configuration == null) {
            _db_configuration = new DatabaseConfiguration.Builder(context)
                    .setName(DBConstant.DB_NAME).build();
            _db_configuration.createDataBase();
        }
        return _db_configuration;
    }

    public void destroy() {
        _drawer = null;
        activity = null;
        _flowOrganize = null;
        _testPro = null;
        listLoadProductsStatus = null;
        productsItemsList = null;
    }


//    public LocalImageLoader getLocalImageLoader() {
//        if (_local_image_loader == null)
//            _local_image_loader = new LocalImageLoader(activity);
//        return _local_image_loader;
//    }


    public void setCoutryReason(CountryReason coutryReason) {
        this.coutryReason = coutryReason;
    }

    public PhoneSpecificImage getFullImageLoader() {
        if (fullImageLoader == null) {
            fullImageLoader = new PhoneSpecificImage(activity,
                    ImageType.FULL_SCREEN_IMAGE);
        }
        return fullImageLoader;
    }

//    public PhoneSpecificImage getThumbImageLoader() {
//        if (thumbImageLoader == null) {
//            thumbImageLoader = new PhoneSpecificImage(activity,
//                    ImageType.THUMB_IMAGE);
//        }
//        return thumbImageLoader;
//    }

    public PhoneSpecificImage getThumbImageLoader() {
        if (thumbImageLoader == null) {
            thumbImageLoader = new PhoneSpecificImage(activity,
                    ImageType.FULL_SCREEN_IMAGE);
        }
        return thumbImageLoader;
    }


    //Save lists for picking no of items quantity
    public SparseArray<List<CategoryItem>> getProductsItemsList() {
        return productsItemsList;
    }

    public void setProductsItemsList(
            SparseArray<List<CategoryItem>> productsItemsList) {
        this.productsItemsList = productsItemsList;
    }

    //Save lists for checking is to load more products or not
    public List<LoadProducts> getListLoadProductsStatus() {
        return listLoadProductsStatus;
    }

    public void setListLoadProductsStatus(
            List<LoadProducts> listLoadProductsStatus) {
        this.listLoadProductsStatus = listLoadProductsStatus;
    }

    public void onResume() {
        if (_location_manager != null)
            _location_manager.onResume();
        if (_flowOrganize != null)
            _flowOrganize.onResume();
    }

    public void onPause() {
        if (_flowOrganize != null)
            _flowOrganize.onPause();
    }

    public void onStart() {
        if (_location_manager != null)
            _location_manager.onStart();
    }

    public void onStop() {
        if (_location_manager != null)
            _location_manager.onStop();

    }

    public DownloadImageFromUrl getDownloadImage(Context context) {
        if (downloadImage == null)
            downloadImage = new DownloadImageFromUrl(context);
        return downloadImage;
    }

    public List<Category> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<Category> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public boolean isSplashSelectionDone() {
        return isSplashSelectionDone;
    }

    public void setSplashSelectionDone(boolean isSplashSelectionDone) {
        this.isSplashSelectionDone = isSplashSelectionDone;
    }

}
