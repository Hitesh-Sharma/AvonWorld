package com.avon.india.basehelper;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.util.SparseArray;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.supports.ConstantsReal;

import java.util.ArrayList;
import java.util.List;

public class BaseAboutUsResourceLoader extends Fragment {

    // public List<Integer> initializeListColors() {
    // List<Integer> listColors = new ArrayList<Integer>();
    // listColors.add(Color.parseColor("#cccccc"));
    // listColors.add(Color.parseColor("#ed008c"));
    // listColors.add(Color.parseColor("#9e8eb3"));
    // listColors.add(Color.parseColor("#8bc8e0"));
    // listColors.add(Color.parseColor("#cccccc"));
    // listColors.add(Color.parseColor("#ed008c"));
    // listColors.add(Color.parseColor("#9e8eb3"));
    // listColors.add(Color.parseColor("#8bc8e0"));
    // listColors.add(Color.parseColor("#cccccc"));
    // listColors.add(Color.parseColor("#ed008c"));
    // return listColors;
    // }

    public List<Integer> initializeListBgColors() {
        List<Integer> listBGColors = new ArrayList<Integer>();
        listBGColors.add(Color.parseColor("#0f5971"));
        listBGColors.add(Color.parseColor("#d31920"));
        listBGColors.add(Color.parseColor("#801d80"));
        listBGColors.add(Color.parseColor("#cc539c"));
        listBGColors.add(Color.parseColor("#456b4d"));
        listBGColors.add(Color.parseColor("#00a7a1"));
        listBGColors.add(Color.parseColor("#f06422"));
        listBGColors.add(Color.parseColor("#e7167e"));
        listBGColors.add(Color.parseColor("#2b3688"));
        listBGColors.add(Color.parseColor("#eb1164"));
        return listBGColors;
    }

    public List<Integer> initializeHistoryBgColors() {
        List<Integer> listBGColors = new ArrayList<Integer>();
        listBGColors.add(Color.parseColor("#0f5971"));
        listBGColors.add(Color.parseColor("#456b4d"));
        listBGColors.add(Color.parseColor("#cc539c"));
        listBGColors.add(Color.parseColor("#eb1164"));
        listBGColors.add(Color.parseColor("#2b3688"));
        return listBGColors;
    }

    public List<Integer> initializeKeyImages() {
        List<Integer> listKeyImages = new ArrayList<Integer>();
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        listKeyImages.add(R.drawable.key_1);
        listKeyImages.add(R.drawable.key_2);
        listKeyImages.add(R.drawable.key_3);
        listKeyImages.add(R.drawable.key_4);
        listKeyImages.add(R.drawable.key_5);
        listKeyImages.add(R.drawable.key_6);
        listKeyImages.add(R.drawable.key_7);
        listKeyImages.add(R.drawable.key_8);
        listKeyImages.add(R.drawable.key_9);
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES))
            listKeyImages.add(R.drawable.ph_key_10);
        else
            listKeyImages.add(R.drawable.key_10);
        return listKeyImages;
    }

    public List<Integer> initializeMissionBgColors() {
        List<Integer> listBGColors = new ArrayList<Integer>();
        listBGColors.add(Color.parseColor("#50342E"));
        listBGColors.add(Color.parseColor("#eb1164"));
        listBGColors.add(Color.parseColor("#d31920"));
        return listBGColors;
    }


    public List<Integer> initializeOurMissionImages() {
        List<Integer> listImages = new ArrayList<Integer>();
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES))
            listImages.add(R.drawable.ph_our_mission);
        else
            listImages.add(R.drawable.our_mission);
        listImages.add(R.drawable.app_icon);
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES))
            listImages.add(R.drawable.ph_our_vision);
        else
            listImages.add(R.drawable.our_vision);
        return listImages;
    }

    public List<String> initializeOurMissionTitles() {
        List<String> listTitles = new ArrayList<String>();
        String[] descriptions = getResources().getStringArray(
                R.array.our_mission_title);
        for (String descp : descriptions)
            listTitles.add(descp);
        return listTitles;

    }

    public List<String> initializeOurMissionDescription() {
        List<String> listDesc = new ArrayList<String>();
        String[] descriptions = getResources().getStringArray(
                R.array.our_mission_description);
        for (String descp : descriptions)
            listDesc.add(descp);
        return listDesc;

    }

    public List<String> initializeOutHistoryTitle() {
        List<String> listTitles = new ArrayList<String>();
        String[] descriptions = getResources().getStringArray(
                R.array.our_history_heading);
        for (String descp : descriptions)
            listTitles.add(descp);
        return listTitles;
    }

    // public List<String> initializeOutHistoryDescription() {
    // List<String> listDescriptions = new ArrayList<String>();
    // String[] descriptions = getResources().getStringArray(
    // R.array.our_history_description);
    // for (String descp : descriptions)
    // listDescriptions.add(descp);
    // return listDescriptions;
    // }

    public SparseArray<List<String>> initializeOutHistoryDescription() {
        SparseArray<List<String>> hashMapDescription = new SparseArray<List<String>>();
        List<String> listDescription = null;
        String[] one = getResources().getStringArray(
                R.array.our_history_description_one);
        listDescription = new ArrayList<String>();
        for (String txt : one) {
            listDescription.add(txt);
        }
        hashMapDescription.put(0, listDescription);
        String[] two = getResources().getStringArray(
                R.array.our_history_description_two);
        listDescription = new ArrayList<String>();
        for (String txt : two) {
            listDescription.add(txt);
        }
        hashMapDescription.put(1, listDescription);
        String[] three = getResources().getStringArray(
                R.array.our_history_description_three);
        listDescription = new ArrayList<String>();
        for (String txt : three) {
            listDescription.add(txt);
        }
        hashMapDescription.put(2, listDescription);
        String[] four = getResources().getStringArray(
                R.array.our_history_description_four);
        listDescription = new ArrayList<String>();
        for (String txt : four) {
            listDescription.add(txt);
        }
        hashMapDescription.put(3, listDescription);
        String[] five = getResources().getStringArray(
                R.array.our_history_description_five);
        listDescription = new ArrayList<String>();
        for (String txt : five) {
            listDescription.add(txt);
        }
        hashMapDescription.put(4, listDescription);
        return hashMapDescription;
    }

    public SparseArray<List<Integer>> initializeOurHistoryImages() {
        SparseArray<List<Integer>> hashMapImagesText = new SparseArray<List<Integer>>();
        List<Integer> listPictures = null;
        listPictures = new ArrayList<Integer>();
        listPictures.add(R.drawable.our_history_a_1);
        listPictures.add(R.drawable.our_history_a_2);
        hashMapImagesText.put(0, listPictures);
        listPictures = new ArrayList<Integer>();
        listPictures.add(R.drawable.our_history_b_1);
        listPictures.add(R.drawable.our_history_b_2);
        hashMapImagesText.put(1, listPictures);
        listPictures = new ArrayList<Integer>();
        listPictures.add(R.drawable.our_history_c_1);
        listPictures.add(R.drawable.our_history_c_2);
        hashMapImagesText.put(2, listPictures);
        listPictures = new ArrayList<Integer>();
        listPictures.add(R.drawable.our_history_d_1);
        listPictures.add(R.drawable.our_history_d_2);
        hashMapImagesText.put(3, listPictures);
        listPictures = new ArrayList<Integer>();
        listPictures.add(R.drawable.our_history_e_1);
        listPictures.add(R.drawable.our_history_e_2);
        hashMapImagesText.put(4, listPictures);
        return hashMapImagesText;
    }

    public SparseArray<List<String>> initializeOurHistoryPictureStrings() {
        SparseArray<List<String>> hashMapImagesText = new SparseArray<List<String>>();
        List<String> listPictureTexts = null;
        String[] one = getResources().getStringArray(
                R.array.our_history_image_one);
        listPictureTexts = new ArrayList<String>();
        for (String txt : one) {
            listPictureTexts.add(txt);
        }
        hashMapImagesText.put(0, listPictureTexts);
        String[] two = getResources().getStringArray(
                R.array.our_history_image_two);
        listPictureTexts = new ArrayList<String>();
        for (String txt : two) {
            listPictureTexts.add(txt);
        }
        hashMapImagesText.put(1, listPictureTexts);
        String[] three = getResources().getStringArray(
                R.array.our_history_image_three);
        listPictureTexts = new ArrayList<String>();
        for (String txt : three) {
            listPictureTexts.add(txt);
        }
        hashMapImagesText.put(2, listPictureTexts);
        String[] four = getResources().getStringArray(
                R.array.our_history_image_four);
        listPictureTexts = new ArrayList<String>();
        for (String txt : four) {
            listPictureTexts.add(txt);
        }
        hashMapImagesText.put(3, listPictureTexts);
        String[] five = getResources().getStringArray(
                R.array.our_history_image_five);
        listPictureTexts = new ArrayList<String>();
        for (String txt : five) {
            listPictureTexts.add(txt);
        }
        hashMapImagesText.put(4, listPictureTexts);
        return hashMapImagesText;
    }

}
