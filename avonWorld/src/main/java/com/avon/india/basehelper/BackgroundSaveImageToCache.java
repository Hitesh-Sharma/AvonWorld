package com.avon.india.basehelper;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.LazyLoaderLib.ILazyLoaderListner;
import com.avon.india.db.DbOperations;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;

/**
 * Created by mukesh on 12/2/16.
 */
public class BackgroundSaveImageToCache {

    ImageView imgView = new ImageView(Avon.getInstance().getActivityInstance());
    Handler handler = new Handler();
    List<CategoryItem> categoryItems = new ArrayList<CategoryItem>();


    public void startTheProcess() {
        if (Avon.getInstance().isAlreadyDownloadingImages())
            return;
        if (!AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA))
            return;
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            categoryItems = DbOperations.getProductsListList(Avon.getInstance().getActivityInstance());
            if (categoryItems != null
                    && categoryItems.size() > 0) {
                final String url = categoryItems.get(0).getImageUrl();
                Log.e("BackgroundSaveImageToCache", "handler ");
                Avon.getInstance().getActivityInstance().showLoading();
                startCaching(url, 0);
            }
        }
    }

    int i = 0;

    private void startCaching(String image_url, final int position) {
        Avon.getInstance().getThumbImageLoader().startLazyLoading(image_url, imgView);
        Avon.getInstance().getThumbImageLoader()
                .registerLoadingCompleteListner(new ILazyLoaderListner() {
                    @Override
                    public void onLaziLoadingComplete(boolean isSuccess) {
                        Log.e("BackgroundSaveImageToCache", "BackgroundSaveImageToCache " + position);
                        if (position + 1 < categoryItems.size()) {
                            startCaching(categoryItems.get(position + 1).getImageUrl(), position + 1);
                            return;
                        } else {
                            Avon.getInstance().getActivityInstance().hideLoading();
                            Avon.getInstance().setIsAlreadyDownloadingImages(true);
                            return;
                        }
                    }

                    @Override
                    public void onLaziLoadingComplete(boolean isSuccess, Bitmap bitmap) {

                        Log.e("BackgroundSaveImageToCache", "onLaziLoadingComplete " + position);
                    }

                    @Override
                    public void onLaziLoadingStarts() {

                        Log.e("BackgroundSaveImageToCache", "onLaziLoadingStarts " + position);
                    }
                });
    }

}
