package com.avon.india.basehelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.avon.india.AppPrefrence;
import com.avon.india.supports.ConstantsReal;
import com.google.android.gcm.GCMRegistrar;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.dialogs.DialogProgressBar;
import com.avon.india.gcm.CommonUtilities;
import com.avon.india.gcm.RegisterOnServer;
import com.avon.india.gcm.WakeLocker;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.StringConstants;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.DeviceIdReceiver;

import java.util.List;
import java.util.Locale;

public class BaseFragmentActivity extends FragmentActivity {

    private MenuType menuType;
    private int back_count;
    private DialogProgressBar dialog_progress_bar;

    public void setMenuType(MenuType menuType, View view_menu, View view_title,
                            String title) {
        this.menuType = menuType;
        ((TextView) view_title).setText(title);

        switch (menuType) {
            case DRAWER:
                ((ImageButton) view_menu).setImageResource(R.drawable.menu_icon);
                break;

            case BACK:
                ((ImageButton) view_menu).setImageResource(R.drawable.back_icon);
                break;

            default:
                break;
        }
    }

    public void showDrawer(View view) {
        if (Avon.getInstance().getDrawerInstance(
                Avon.getInstance().getActivityInstance()) != null) {
            Avon.getInstance()
                    .getDrawerInstance(Avon.getInstance().getActivityInstance())
                    .getSlider().toggle();
        }
    }

    public void changeLanguage(String lang) {
        Locale myLocale;
        if (lang.equalsIgnoreCase(""))
            return;
        AppLogger.e("BaseFragmentActivity lang", "" + lang);
        myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            WakeLocker.acquire(getApplicationContext());
            WakeLocker.release();
        }
    };

    public void dealWithGCM() {
        try {
            getGcmRegistration();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getGcmRegistration() {
        String device_id = DeviceIdReceiver.getInstance().getHashedDeivceId(
                getApplicationContext());
        if (CheckNetworkState.isNetworkAvailable(getApplicationContext())) {
            GCMRegistrar.checkDevice(getApplicationContext());
            GCMRegistrar.checkManifest(getApplicationContext());
            registerReceiver(mHandleMessageReceiver, new IntentFilter(
                    CommonUtilities.DISPLAY_MESSAGE_ACTION));
            final String regId = GCMRegistrar
                    .getRegistrationId(getApplicationContext());
            AppLogger.e("GCM ID is", "Id:" + regId);
            if (regId.trim().equals("")) {
                AppLogger.e("GCM ID not received", "yes");
                GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
            } else {
                AppLogger.e("GCM ID  received", "yes " + regId);
                AppLogger.e("device_id  received", "yes " + device_id);
                registerGCMID(regId, device_id);
            }
        } else {
            showNoInternetToast(null);
        }
    }

    public void registerGCMID(String gcm_key, String device_id) {
        if (CheckNetworkState.isNetworkAvailable(getApplicationContext())) {
            RegisterOnServer registerOnServer = new RegisterOnServer(this);
            registerOnServer.registerPush(gcm_key, device_id);
        } else {
            showNoInternetToast(null);
        }
    }

    public enum MenuType {
        DRAWER, BACK
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Avon.getInstance().destroy();
        try {
            System.exit(7);
            try {
                unregisterReceiver(mHandleMessageReceiver);
                GCMRegistrar.onDestroy(this);
            } catch (Exception e) {
                AppLogger.e("UnRegister Receiver Error", "> " + e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (Avon.getInstance().getDrawerInstance(
                Avon.getInstance().getActivityInstance()) != null) {
            if (Avon.getInstance()
                    .getDrawerInstance(Avon.getInstance().getActivityInstance())
                    .isOpen()) {
                Avon.getInstance()
                        .getDrawerInstance(
                                Avon.getInstance().getActivityInstance())
                        .getSlider().close();
                return;
            }
        }
        boolean isToWorkOnBack = true;
        if (Avon.getInstance().getFlowOrganization() != null) {
            List<Fragment> list = getSupportFragmentManager().getFragments();
            if (list != null) {
                for (Fragment f : list) {
                    if (f != null && f instanceof BaseFragmentBackHandler) {
                        isToWorkOnBack = ((BaseFragmentBackHandler) f)
                                .onBackPressed();
                    }
                }
            }
        }
        if (!isToWorkOnBack)
            return;
        if (!Avon.getInstance().getFlowOrganization().hasNoMoreBack())
            super.onBackPressed();
        else {
            back_count++;
            if (back_count == 2) {
                System.exit(1);
                finish();
            } else {
                showToast(StringConstants.EXITMESSAGE);
            }
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    back_count = 0;
                }
            }, 2000);
        }
    }

    public void showToast(String response_message) {
        CharSequence message = null;
        if (response_message != null
                && !response_message.trim().equalsIgnoreCase("")) {
            message = response_message;
        } else {
            message = ToastMessages.NO_MESSAGE_FROM_SERVER;
        }
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_success_message,
                (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        TextView txt_view_message = (TextView) layout
                .findViewById(R.id.txt_view_message);
        txt_view_message.setText(message);

        // Create Custom Toast
        Toast toast = new Toast(Avon.getInstance().getActivityInstance());
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void showNoInternetToast(String response_message) {
        CharSequence message = null;
        String country_name = AppPrefrence.getInstance(this).getMyCountryName();
        if (country_name == null || country_name.trim().equals(""))
            message = ToastMessages.NO_INTERNET;
        else {
            if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
                message = Avon.getInstance().getActivityInstance().getResources().getString(R.string.no_internet);
            } else {
                if (response_message == null
                        || response_message.trim().equalsIgnoreCase("")) {
                    if (Avon.getInstance().getActivityInstance() != null) {
                        message = Avon.getInstance().getActivityInstance().getResources().getString(R.string.no_internet);
                    } else
                        message = ToastMessages.NO_INTERNET;
                } else
                    message = response_message;
            }
        }
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_no_internet_connection,
                (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        TextView txt_view_message = (TextView) layout
                .findViewById(R.id.txt_view_message);
        txt_view_message.setText(message);

        // Create Custom Toast
        Toast toast = new Toast(Avon.getInstance().getActivityInstance());
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void showLoading() {
        dialog_progress_bar = new DialogProgressBar(this);
        dialog_progress_bar.show();
    }

    public void hideLoading() {
        if (dialog_progress_bar != null)
            dialog_progress_bar.hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Avon.getInstance().onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Avon.getInstance().onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        Avon.getInstance().onStart();
    }

    @Override
    public void onStop() {
        Avon.getInstance().onStop();
        super.onStop();
    }

}
