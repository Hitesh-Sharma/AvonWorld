package com.avon.india.basehelper;

import android.support.v4.app.Fragment;

import com.avon.india.interfaces.IOnBackPressedListener;

public class BaseFragmentBackHandler extends Fragment implements
		IOnBackPressedListener {

	@Override
	public boolean onBackPressed() {
		return true;
	}

}
