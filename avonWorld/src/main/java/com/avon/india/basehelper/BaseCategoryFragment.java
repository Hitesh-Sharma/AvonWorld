package com.avon.india.basehelper;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import com.avon.india.Avon;
import com.avon.india.db.Constants;
import com.avon.india.db.DbOperations;
import com.avon.india.fragments.FragmentSaving;
import com.avon.india.interfaces.IOnFragmentUpdated;
import com.avon.india.interfaces.IOnpreparedListener;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.Category;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.LoadProducts;
import com.avon.india.supports.ConstantsReal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BaseCategoryFragment extends Fragment implements
        IOnFragmentUpdated {

    private SparseArray<FragmentSaving> map = new SparseArray<FragmentSaving>();
    private final int MAX_LIMIT = ConstantsReal.IConstants.MAX_COUNT;

    public void requestOnPrepared(final View view,
                                  final IOnpreparedListener onPrepared) {
        view.post(new Runnable() {

            @Override
            public void run() {
                if (onPrepared != null)
                    onPrepared.onViewPrepared(view);
            }
        });
    }

    @Override
    public int onUpdated(int index, Fragment fr) {
        map.put(index, (FragmentSaving) fr);
        return 0;
    }

    public void insertStatusTOLoadProducts() {
        List<Category> listCategories = Avon.getInstance().getCategoriesList();
        List<LoadProducts> listLoadProductsStatus = new ArrayList<LoadProducts>();
        for (Category category : listCategories) {
            LoadProducts product = new LoadProducts();
            product.setCategoryId(category.getCategoryId());
            product.setCategoryName(category.getCategoryName());
            listLoadProductsStatus.add(product);
        }
        if (listLoadProductsStatus != null && listLoadProductsStatus.size() > 0)
            Avon.getInstance()
                    .setListLoadProductsStatus(listLoadProductsStatus);
    }

    // public void saveProductItemsList(Products products) {
    // List<Category> listCategory = products.getCategoriesList();
    // List<CategoryItem> listCategoryItems = products.getCategoryItemsList();
    //
    // SparseArray<List<CategoryItem>> items = new
    // SparseArray<List<CategoryItem>>();
    // List<CategoryItem> subList = null;
    // for (Category category : listCategory) {
    // subList = new ArrayList<CategoryItem>();
    // for (CategoryItem item : listCategoryItems) {
    // if (category.getCategoryId() == item.getCategoy()) {
    // subList.add(item);
    // }
    // }
    // items.put(category.getCategoryId(), subList);
    // }
    // Avon.getInstance().setProductsItemsList(items);
    //
    // checkProductsLoadStatus();
    // }

    public void checkProductsLoadStatus() {
        List<LoadProducts> listLoadProductsStatus = Avon.getInstance()
                .getListLoadProductsStatus();
//        List<Category> listLoadProductsStatus = Avon.getInstance().getCategoriesList();
        SparseArray<List<CategoryItem>> listLoadedProducts = Avon
                .getInstance().getProductsItemsList();
        if (listLoadProductsStatus == null || listLoadProductsStatus.size() == 0)
            return;
        for (int i = 0; i < listLoadProductsStatus.size(); i++) {
            // String categroyName = listLoadProductsStatus.get(i)
            // .getCategoryName();
            int categroyId = listLoadProductsStatus.get(i).getCategoryId();
            Log.e("category id", " - " + categroyId);
            try {
                if (listLoadedProducts.get(categroyId) == null) {
                    Avon.getInstance().getListLoadProductsStatus().get(i)
                            .setIsToLoadMoreProducts(false);
                } else {
                    List<CategoryItem> products = listLoadedProducts
                            .get(categroyId);
                    Log.e("", "products list for " + categroyId + " is ="
                            + products.size());
                    if (products.size() < MAX_LIMIT) {
                        Avon.getInstance().getListLoadProductsStatus().get(i)
                                .setIsToLoadMoreProducts(false);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fetchDataFromDBandInsetToList() {
        List<Category> categories = DbOperations.getCategoriesList(Avon.getInstance().getActivityInstance());
        if (categories == null | categories.size() == 0)
            return;
        Avon.getInstance().setCategoriesList(categories);
        SparseArray<List<CategoryItem>> items = new SparseArray<List<CategoryItem>>();
        for (Category category : categories) {
            int category_id = category.getCategoryId();
            List<CategoryItem> categoryItems = new ArrayList<CategoryItem>();
            categoryItems = DbOperations.getProductsListList(Avon.getInstance().getActivityInstance(), category_id);
            if (categoryItems != null
                    && categoryItems.size() > 0) {
                if (categoryItems != null) {
                    items.put(category_id, categoryItems);
                }
            }
        }
        Avon.getInstance().setProductsItemsList(items);
    }

    public void calculateSaving(int index) {
        List<Category> listCategories = Avon.getInstance().getCategoriesList();
        int itemIndex = (index - 1) / 2;
        int avgPrice = 0;
        int totalSavingIndex = (listCategories.size() * 2);
        if (index == totalSavingIndex) {
            for (int i = 0; i < listCategories.size(); i++) {
                int itemCategoryId = listCategories.get(i).getCategoryId();
                if (Avon.getInstance().getProductsItemsList() != null) {
                    List<CategoryItem> items = Avon.getInstance()
                            .getProductsItemsList().get(itemCategoryId);
                    if (items != null && items.size() > 0) {
                        avgPrice += Avon.getPurchase().getSavings(items);
                    }
                }
            }
        } else {
            int itemCategoryId = listCategories.get(itemIndex).getCategoryId();
            if (Avon.getInstance().getProductsItemsList() != null) {
                List<CategoryItem> items = Avon.getInstance()
                        .getProductsItemsList().get(itemCategoryId);
                if (items != null && items.size() > 0) {
                    avgPrice = Avon.getPurchase().getSavings(items);
                } else {
                    Log.e("", "List Empty");
                }
            }
        }
        final int finalavgPrice = avgPrice;
        final FragmentSaving frsaving = map.get(index);
        if (index == 10)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (frsaving != null)
                        frsaving.setAveragePrice(finalavgPrice);
                }
            }, 500);
        else {
            if (frsaving != null)
                frsaving.setAveragePrice(finalavgPrice);
        }
    }

    @Override
    public void onDestroy() {
        Avon.getInstance().setListLoadProductsStatus(null);
        Avon.getInstance().setProductsItemsList(null);
        Avon.getInstance().setIsOkDialogShown(true);
        super.onDestroy();
    }

}
