package com.avon.india.basehelper;

import android.support.v4.app.Fragment;
import android.view.View;

import com.avon.india.interfaces.IOnpreparedListener;

public abstract class BaseFragment extends Fragment {
	public abstract boolean onBackPress();

	public void requestOnPrepared(final View view,
			final IOnpreparedListener onPrepared) {
		view.post(new Runnable() {

			@Override
			public void run() {
				if (onPrepared != null)
					onPrepared.onViewPrepared(view);
			}
		});
	}

}
