package com.avon.india.LazyLoaderLib;

public enum ImageType {
	THUMB_IMAGE, DEFAULT_IMAGE, FULL_SCREEN_IMAGE, CUSTOM_SIZES_IMAGE
}
