package com.avon.india;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.avon.india.basehelper.BaseFragmentActivity;
import com.avon.india.db.DatabaseConfiguration;
import com.avon.india.dialogs.DialogFragmentNewSelectCountry;
import com.avon.india.drawer.NavigationDrawer.IOnDrawerListner;
import com.avon.india.enums.EnumSet;
import com.avon.india.enums.EnumSet.AboutUsType;
import com.avon.india.enums.EnumSet.CategoryType;
import com.avon.india.fragments.FragmentBuitiqueLocation;
import com.avon.india.fragments.FragmentFeedback;
import com.avon.india.fragments.FragmentHome;
import com.avon.india.fragments.FragmentNotificationNew;
import com.avon.india.fragments.FragmentSplash;
import com.avon.india.fragments.FragmentVideo;
import com.avon.india.fragments.paggers.FragmentPaggerBeautyStore;
import com.avon.india.fragments.paggers.FragmentPaggerCategory;
import com.avon.india.fragments.paggers.FragmentPaggerKeyStrengthDetails;
import com.avon.india.fragments.paggers.FragmentPaggerOffers;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckUserStatus;
import com.avon.india.supports.ConstantsReal.CountryNames;
import com.avon.india.supports.ConstantsReal.LanguageUtils;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.PermissionToAccessExternalStorage;
import com.avon.india.supports.PermissionToMakeCall;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class AvonMainActivity extends BaseFragmentActivity implements
        IOnDrawerListner {

    private RelativeLayout relative_action_bar;
    private EnumSet.SplashType splashType = EnumSet.SplashType.CHOOSE_LOCATION;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Avon.getInstance().initActivity(this);
        // Avon.getInstance().getFlowOrganization().add(new FragmentHome());
//        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
//                .setOnDrawerItemClickListner(this);

        Avon.getInstance().getDrawerInstanceDefault(this);
//        Avon.getInstance().getDrawerInstance(AvonMainActivity.this).getSlider()
//                .setSlidingEnabled(true);
//        Avon.getInstance().setDrawerInstance();
        checkStatus();
        FragmentSplash fragment = new FragmentSplash();
        fragment.setSplashMode(splashType);
        Avon.getInstance().getFlowOrganization().add(fragment);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy tp = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(tp);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

//    public void permissionForMarshmallow() {
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
//
//        } else {
//
//        }
//    }
//
//    private void showDefaultYesNoDialog() throws Exception {
//        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (which) {
//                    case DialogInterface.BUTTON_POSITIVE:
//                        // Yes button clicked
//                        goToSettings();
//                        break;
//
//                    case DialogInterface.BUTTON_NEGATIVE:
//                        // No button clicked
//                        break;
//                }
//            }
//        };
//        AlertDialog.Builder builder = new AlertDialog.Builder(Avon.getInstance().getActivityInstance());
//        builder.setMessage("Please Grant all the permissions to access this app")
//                .setPositiveButton("Setting", dialogClickListener)
//                .setNegativeButton("Cancel", dialogClickListener).show();
//    }
//
//    private void goToSettings() {
//        try {
//            Intent myAppSettings = new Intent(
//                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
//                    Uri.parse("package:"
//                            + Avon.getInstance().getActivityInstance()
//                            .getPackageName()));
//            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
//            myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            Avon.getInstance().getActivityInstance()
//                    .startActivityForResult(myAppSettings,
//                            AppPermissions.MY_PERMISSIONS_ALL);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        if (getSupportFragmentManager() != null) {
            List<Fragment> list = getSupportFragmentManager().getFragments();
            if (list == null)
                return;
            for (Fragment f : list) {
                if (f != null)
                    f.onLowMemory();
            }
        }
    }

    public void checkStatus() {
        AppLogger.e("Status check", "" + CheckUserStatus.getUserStatus(this).toString());
        switch (CheckUserStatus.getUserStatus(this)) {
            case NEW_USER:
                splashType = EnumSet.SplashType.CHOOSE_LOCATION;
//                showLocationChooser(null);
                break;
            case REGISTERED:
                splashType = EnumSet.SplashType.LOCATION_CHOOSEN;
                updateLanguage(AppPrefrence.getInstance(this).getMyCountryName());
                break;
            case NOT_REGISTERED:
                splashType = EnumSet.SplashType.LOCATION_CHOOSEN;
//                dealWithGCM();
                updateLanguage(AppPrefrence.getInstance(this).getMyCountryName());
                break;
            default:
                break;
        }
    }

    public void showLocationChooser(View view) {
        if (!new PermissionToMakeCall().isToMakePhone())
            return;
        Avon.getInstance().setSplashSelectionDone(true);
        DialogFragmentNewSelectCountry dialogFragment = new DialogFragmentNewSelectCountry();
        Avon.getInstance().getFlowOrganization().addWithBottomTopAnimation(dialogFragment, null, true);
    }

    private void setVisiblityOfBuitque(boolean status) {
        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
                .hideViewFromDrawer(R.id.linear_boutique, status);
    }

    private void setVisiblityOfBueatyStore(boolean status) {
        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
                .hideViewFromDrawer(R.id.linear_bueaty_store, status);
        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
                .hideViewFromDrawer(R.id.linear_products, !status);
    }

    private void setVisiblityOfFeedback(boolean status) {
        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
                .hideViewFromDrawer(R.id.linear_feedback, status);
    }

//    private void setVisiblityOfAboutUsForThiwan(boolean status) {
//        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
//                .hideViewFromDrawer(R.id.linear_about_us_one, status);
////        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
////                .hideViewFromDrawer(R.id.linear_current_offers, status);
//        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
//                .hideViewFromDrawer(R.id.linear_about, !status);
//    }

    // private void updateLanguage(int country_id) {
    // switch (country_id) {
    // case CountryIds.INDIA:
    // changeLang(LanguageUtils.ENGLISH_LANG);
    // break;
    // case CountryIds.MALAYSIA:
    // changeLang(LanguageUtils.MALAY_LANG);
    // break;
    // case CountryIds.CHANISE:
    //
    // break;
    // case CountryIds.INDONESIA:
    //
    // break;
    //
    // default:
    // break;
    // }
    // updateViews(country_id);
    // }

    public void updateLanguage(final String country_name) {
        Avon.getInstance().getActivityInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AppLogger.e("country_name", " : " + country_name);
                boolean setBuitqueVisiblity = false, setBueatyStoreVisiblity = true, setAboutUsForThiwan = false, setFeedbackForTaiwan = true;
                String language = LanguageUtils.ENGLISH_LANG;
                if (country_name == null || country_name.trim().equals("")) {
                } else {
                    if (country_name.equalsIgnoreCase(CountryNames.INDIA)) {
                        language = LanguageUtils.ENGLISH_LANG;
                    } else if (country_name.equalsIgnoreCase(CountryNames.TAIWAN)) {
                        language = LanguageUtils.TAIWAN_LANG;
//                        setFeedbackForTaiwan = false;
                        setBuitqueVisiblity = true;
//                setAboutUsForThiwan = true;
                    } else if (country_name.equalsIgnoreCase(CountryNames.MALAYSIA)) {
                        language = LanguageUtils.MALAY_LANG;
                        setBuitqueVisiblity = true;
                        setBueatyStoreVisiblity = false;
                    } else if (country_name.equalsIgnoreCase(CountryNames.PHILIPINNES)) {
                        language = LanguageUtils.PHILIPINNES_LANG;
                        setBuitqueVisiblity = true;
                    }
                }
                AppPrefrence.getInstance(getApplicationContext()).setMyLocalLanguage(language);
                AppPrefrence.getInstance(getApplicationContext()).setAboutUsStatusForThiwan(setAboutUsForThiwan);
                AppPrefrence.getInstance(getApplicationContext()).setBuitqueLoacatorStatus(
                        setBuitqueVisiblity);
                AppPrefrence.getInstance(getApplicationContext()).setBueatyStoreStatus(
                        setBueatyStoreVisiblity);
                changeLanguage(language);
                Avon.getInstance().setProductsItemsList(null);
                Avon.getInstance().setCategoriesList(null);
                Avon.getInstance().setListLoadProductsStatus(null);
                AppLogger.e("Drawer Status",
                        "setBuitqueVisiblity " + setBuitqueVisiblity + ", setBueatyStoreVisiblity " + setBueatyStoreVisiblity + ", setAboutUsForThiwan " + setAboutUsForThiwan);
                initOtherComponents(setBuitqueVisiblity, setBueatyStoreVisiblity, setFeedbackForTaiwan);
            }
        });
    }

    // private void updateViews(int country_id) {
    // initOtherComponents();
    // if (country_id == 1) {
    // DialogLocationChooser dialog = new DialogLocationChooser(Avon
    // .getInstance().getActivityInstance());
    // dialog.setCancelable(false);
    // dialog.show();
    // }
    // if (country_id == 0 || country_id == 2) {
    // setVisiblityOfBuitque(false);
    // } else {
    // setVisiblityOfBuitque(true);
    // }
    // }

    private void initOtherComponents(boolean buitqueStatus,
                                     boolean bueatyStoreStatus, boolean setFeedbackForThiwan) {
        AppLogger.e("Drawer Status",
                "buitqueStatus " + buitqueStatus + ", bueatyStoreStatus " + bueatyStoreStatus + ", setAboutUsForThiwan " + setFeedbackForThiwan);
        Avon.getInstance().initDB();
//        Avon.getInstance().setDrawerInstance();
        Avon.getInstance().getDrawerInstance(AvonMainActivity.this)
                .setOnDrawerItemClickListner(this);
//        Avon.getInstance().getDrawerInstance(AvonMainActivity.this).getSlider()
//                .setSlidingEnabled(true);
        setVisiblityOfBuitque(buitqueStatus);
        setVisiblityOfBueatyStore(bueatyStoreStatus);
        setVisiblityOfFeedback(setFeedbackForThiwan);
        if (Avon.getInstance().isSplashSelectionDone())
            Avon.getInstance().getFlowOrganization()
                    .replace(new FragmentHome());
        dealWithGCM();
    }

    public void hideRealActionBar() {
        relative_action_bar.setVisibility(View.GONE);
    }

    @Override
    public void onDrawerItemClick(View view) {
        Avon.getInstance().getFlowOrganization().clearBackStack();
        switch (view.getId()) {
            case R.id.linear_home:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentHome());
                break;
            case R.id.linear_bueaty_store:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentPaggerBeautyStore());
                break;
            case R.id.linear_products:
                FragmentPaggerCategory fragment = new FragmentPaggerCategory();
                fragment.setCategoryType(CategoryType.PRODUCTS);
                Avon.getInstance().getFlowOrganization().replace(fragment);
                break;

            case R.id.linear_current_offers:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentPaggerOffers());
                break;

            case R.id.linear_feedback:
                String country_name = AppPrefrence.getInstance(this).getMyCountryName();
                if (country_name != null || country_name.trim().length()>0) {
                    AppLogger.e("country_name", " : " + country_name);
                    if (country_name.equalsIgnoreCase(CountryNames.TAIWAN)) {
                        AppLogger.e("country_name", " : " + country_name);
                        String url = "http://mshop.avon.com.tw/";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        this.startActivity(i);
                    }else {
                        Avon.getInstance().getFlowOrganization().clearBackStack();
                        Avon.getInstance().getFlowOrganization()
                                .replace(new FragmentFeedback());
                    }
                } else {
                    Avon.getInstance().getFlowOrganization().clearBackStack();
                    Avon.getInstance().getFlowOrganization()
                            .replace(new FragmentFeedback());
                }
                break;

            case R.id.linear_boutique:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentBuitiqueLocation());
                break;

            case R.id.linear_videos:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentVideo());
                break;
//            case R.id.linear_about_us_one:
//                FragmentOurHistory fragment_about_one = new FragmentOurHistory();
//                fragment_about_one.setCategoryType(CategoryType.ABOUT_ONE);
//                Avon.getInstance().getFlowOrganization().replace(fragment_about_one);
//                break;
            case R.id.relative_our_history:
//                FragmentOurHistory fragment_history = new FragmentOurHistory();
//                fragment_history.setCategoryType(CategoryType.OUR_HISTORY);
//                Avon.getInstance().getFlowOrganization().replace(fragment_history);
                FragmentPaggerKeyStrengthDetails fragment_history = new
                        FragmentPaggerKeyStrengthDetails();
                fragment_history.setAboutUsType(AboutUsType.HISTORY);
                Avon.getInstance().getFlowOrganization().replace(fragment_history);
                break;
            case R.id.relative_our_mission:
//                FragmentOurHistory fragment_mission = new FragmentOurHistory();
//                fragment_mission.setCategoryType(CategoryType.OUR_MISSION);
//                Avon.getInstance().getFlowOrganization().replace(fragment_mission);
                FragmentPaggerKeyStrengthDetails fragment_mission = new
                        FragmentPaggerKeyStrengthDetails();
                fragment_mission.setAboutUsType(AboutUsType.MISSION);
                Avon.getInstance().getFlowOrganization().replace(fragment_mission);
                break;
            case R.id.relative_key_strengths:
                FragmentPaggerKeyStrengthDetails fragment_keyStrength = new FragmentPaggerKeyStrengthDetails();
                fragment_keyStrength.setAboutUsType(AboutUsType.STRENGTH);
                Avon.getInstance().getFlowOrganization()
                        .replace(fragment_keyStrength);
                break;
            case R.id.linear_notification:
                Avon.getInstance().getFlowOrganization()
                        .replace(new FragmentNotificationNew());
                break;
            case R.id.linear_exit:
                onDestroy();
                break;

            default:
                showToast(ToastMessages.UNDER_CONSTRUCTION);
                break;
        }
    }

    public void showDrawer(View view) {
        if (Avon.getInstance().getDrawerInstance(this) != null)
            Avon.getInstance().getDrawerInstance(this).getSlider().toggle();
        if (view != null) {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AvonMain Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.avon.india/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AvonMain Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.avon.india/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
