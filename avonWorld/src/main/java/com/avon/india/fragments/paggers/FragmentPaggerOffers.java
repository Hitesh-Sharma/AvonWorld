package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdpaterPaggerOffers;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.itemproperty.Offers;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.DateTimeUtils;
import com.avon.india.supports.RequestFormater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentPaggerOffers extends Fragment implements
        OnPageChangeListener, ILoadListner {

    private CustomViewPager custom_pager;
    private TextView txt_view_action_bar_text;
    private RelativeLayout relative_blank;
    private TextView txt_vw_current_offers;
    private AdpaterPaggerOffers adapter;
    private List<Offers> offers_list;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_offers, null);

        custom_pager = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager_offers);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.current_offers));
        txt_vw_current_offers = (TextView) rootView
                .findViewById(R.id.txt_vw_current_offers);
        relative_blank = (RelativeLayout) rootView.findViewById(R.id.relative_blank);
        loadOffers();
        return rootView;
    }

    private void loadOffers() {
        if (CheckNetworkState.isNetworkAvailable(getActivity())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            String currentDate = DateTimeUtils.getTodayDate();
            int countryId = AppPrefrence.getInstance(getActivity())
                    .getMyCountryId();
            HashMap<String, String> params = RequestFormater.getCurrentOffers(
                    countryId, currentDate);
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.OFFERS_DETAILS);
            loader.startLoading(WebUrls.URL_CURRENT_OFFERS, params);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet to View Offers");
        }
    }

    private void setAdapter(List<Offers> list_offers) {
        adapter = new AdpaterPaggerOffers(getChildFragmentManager(),
                list_offers);
        custom_pager.setAdapter(adapter);

        custom_pager.setOnPageChangeListener(this);
        if (custom_pager.getAdapter().getCount() > 0) {
            custom_pager.setCurrentItem(0);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        txt_vw_current_offers.setText(offers_list.get(position)
                .getItem_offers_description());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null)
            return;

        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            Avon.getInstance().getActivityInstance()
                    .showToast(response.getResponseMessage());
            return;
        } else {
            offers_list = (ArrayList<Offers>) response.getResponse();
            if (offers_list != null && offers_list.size() > 0) {
                AppLogger.e("", "Response Details of offers Acreen is "
                        + offers_list);
                setAdapter(offers_list);
                relative_blank.setVisibility(View.GONE);
            } else {
                Avon.getInstance().getActivityInstance()
                        .showToast(response.getResponseMessage());
            }
        }

    }

}
