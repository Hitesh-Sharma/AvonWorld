package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.enums.EnumSet.ImageToShowOf;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.Offers;
import com.avon.india.supports.AppLogger;

import java.util.logging.Handler;

@SuppressLint("InflateParams")
public class FragmentOfferDescription extends Fragment implements
        OnClickListener {
    ImageView img_offers_image;
    private Offers _offer;
    private CategoryItem product;
    static FragmentOfferDescription _instance;
    private ImageToShowOf imageOf = ImageToShowOf.CURRENT_OFFERS;
    private String imageUrl;

    public void setImageToShowFor(ImageToShowOf imageOf, Offers _offers,
                                  CategoryItem product) {
        this.imageOf = imageOf;
        this._offer = _offers;
        this.product = product;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_offers_view_pager,
                null);

        img_offers_image = (ImageView) rootView.findViewById(R.id.img_offers);

        try {
            switch (imageOf) {
                case BEAUTY_STORE:
                    imageUrl = product.getImageUrl();
                    AppLogger.e("imageUrl", "imageUrl " + imageUrl);
                    Avon.getInstance().getThumbImageLoader()
                            .startLazyLoading(imageUrl, img_offers_image);
                    break;
                case CURRENT_OFFERS:
                    imageUrl = _offer.getItem_image();
                    img_offers_image.setOnClickListener(this);

                    Avon.getInstance().getFullImageLoader()
                            .startLazyLoading(imageUrl, img_offers_image);
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_offers:
                if (_offer.getItem_image() == null || _offer.getItem_image().trim().equalsIgnoreCase(""))
                    return;
                FragmentScaleableView fragment = new FragmentScaleableView();
                fragment.setImageUrl(_offer.getItem_image());
                Avon.getInstance().getFlowOrganization().add(fragment, null, true);
                break;

            default:
                break;
        }
    }
}
