package com.avon.india.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.avon.india.R;
import com.avon.india.adapter.AdapterKeyStrength;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.itemproperty.KeyStrengthsList;

import java.util.ArrayList;
import java.util.List;

public class FragmentKeyStrength extends BaseFragment {

	ListView keyStrength;
	AdapterKeyStrength adapter_key_strength;
	List<KeyStrengthsList> keyStrengthList;
	List<Integer> listColors = new ArrayList<Integer>();
	List<Integer> listBGColors = new ArrayList<Integer>();
	List<String> listKeyStrengthValuse = new ArrayList<String>();
	List<String> listKeyStrengthQuestions = new ArrayList<String>();
	List<String> listKeyStrengthAnswers = new ArrayList<String>();
	List<Integer> listKeyImages = new ArrayList<Integer>();

	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_key_strength, null);
		keyStrength = (ListView) rootView
				.findViewById(R.id.list_all_key_strengths);
		initializeListColors();
		initializeListBgColors();
		initializeOtherStrings();
		initializeKeyImages();
		keyStrengthList = new ArrayList<KeyStrengthsList>();
		// for (int i = 0; i < StringConstants.KEYSTRENGTHSVALUES.length; i++) {
		// KeyStrengthsList key_heading = new KeyStrengthsList(
		// listKeyStrengthValuse.get(i),
		// listKeyStrengthQuestions.get(i),
		// listKeyStrengthAnswers.get(i), listColors.get(i),
		// listBGColors.get(i), listKeyImages.get(i));
		// keyStrengthList.add(key_heading);
		// }

		adapter_key_strength = new AdapterKeyStrength(getActivity(),
				keyStrengthList);
		keyStrength.setAdapter(adapter_key_strength);
		// keyStrength.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> arg0, View arg1,
		// int position, long arg3) {
		// SupportUtils.hideVirtualKeyboard(getActivity());
		// // FragmentKeyStrengthDetails fragment = new
		// // FragmentKeyStrengthDetails();
		// // fragment.setCurrentPosition(position, keyStrengthList);
		// // Avon.getInstance().getFlowOrganization().add(fragment, true);
		// FragmentPaggerKeyStrengthDetails fragment = new
		// FragmentPaggerKeyStrengthDetails();
		// fragment.setSelectedListPosition(position, keyStrengthList);
		// Avon.getInstance().getFlowOrganization().add(fragment, true);
		//
		// }
		// });
		return rootView;
	}

	private void initializeOtherStrings() {
		String[] questions = getResources().getStringArray(
				R.array.key_strength_questions);
		String[] answers = getResources().getStringArray(
				R.array.key_strength_answers);
		String[] values = getResources().getStringArray(
				R.array.key_strength_values);
		for (String ques : questions)
			listKeyStrengthQuestions.add(ques);
		for (String ans : answers)
			listKeyStrengthAnswers.add(ans);
		for (String val : values)
			listKeyStrengthValuse.add(val);
	}

	private void initializeListColors() {
		listColors.add(Color.parseColor("#cccccc"));
		listColors.add(Color.parseColor("#ed008c"));
		listColors.add(Color.parseColor("#9e8eb3"));
		listColors.add(Color.parseColor("#8bc8e0"));
		listColors.add(Color.parseColor("#cccccc"));
		listColors.add(Color.parseColor("#ed008c"));
		listColors.add(Color.parseColor("#9e8eb3"));
		listColors.add(Color.parseColor("#8bc8e0"));
		listColors.add(Color.parseColor("#cccccc"));
		listColors.add(Color.parseColor("#ed008c"));
	}

	private void initializeListBgColors() {
		listBGColors.add(Color.parseColor("#0f5971"));
		listBGColors.add(Color.parseColor("#d31920"));
		listBGColors.add(Color.parseColor("#801d80"));
		listBGColors.add(Color.parseColor("#cc539c"));
		listBGColors.add(Color.parseColor("#456b4d"));
		listBGColors.add(Color.parseColor("#00a7a1"));
		listBGColors.add(Color.parseColor("#f06422"));
		listBGColors.add(Color.parseColor("#e7167e"));
		listBGColors.add(Color.parseColor("#2b3688"));
		listBGColors.add(Color.parseColor("#eb1164"));
	}

	private void initializeKeyImages() {
		listKeyImages.add(R.drawable.key_1);
		listKeyImages.add(R.drawable.key_2);
		listKeyImages.add(R.drawable.key_3);
		listKeyImages.add(R.drawable.key_4);
		listKeyImages.add(R.drawable.key_5);
		listKeyImages.add(R.drawable.key_6);
		listKeyImages.add(R.drawable.key_7);
		listKeyImages.add(R.drawable.key_8);
		listKeyImages.add(R.drawable.key_9);
		listKeyImages.add(R.drawable.key_10);
	}

	@Override
	public boolean onBackPress() {
		return false;
	}

}
