package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.supports.ConstantsReal.StringConstants;

@SuppressLint("InflateParams")
public class FragmentHistoryDescription extends Fragment implements
		OnClickListener {
	ImageView img_our_history;
	private static final String KEY_CONTENT = StringConstants.KEY_CONTENT_OUR_HISTORY;

	int imageSource;

	public FragmentHistoryDescription(int imageSource) {
		this.imageSource = imageSource;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_CONTENT)) {
			imageSource = savedInstanceState.getInt(KEY_CONTENT);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_history_description,
				null);

		img_our_history = (ImageView) rootView.findViewById(R.id.img_history);
		img_our_history.setBackgroundResource(imageSource);
		img_our_history.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_history:
			// Bundle bundle = new Bundle();
			// bundle.putInt("Images", imageSource);
			FragmentScaleableView fragment = new FragmentScaleableView();
			fragment.setImageId(imageSource);
			Avon.getInstance().getFlowOrganization().add(fragment, null, true);

			break;

		default:
			break;
		}
	}
}
