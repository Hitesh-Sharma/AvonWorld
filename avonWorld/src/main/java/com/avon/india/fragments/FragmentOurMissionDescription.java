package com.avon.india.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.customviews.CustomTypefaceSpan;
import com.avon.india.itemproperty.OurMission;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.FontFiles;

import java.util.List;

public class FragmentOurMissionDescription extends Fragment {

    private TextView txt_view_head, txt_view_message, txt_ms_belief, txt_ms_integrity, txt_ms_respect, txt_ms_trust, txt_ms_humility;
    private ImageView img_view_image;
    private LinearLayout linear_txt_views;
    private int currentPosition;
    private String spanText;
    String textMessage;
    private List<OurMission> listMission;
    private ImageView img_view_tv_bueaty;
    private TextView txt_view_beauty_purpose;

    public void setSelectedListPosition(int currentPosition,
                                        List<OurMission> listMission) {
        this.currentPosition = currentPosition;
        this.listMission = listMission;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(
                R.layout.fragment_our_mission_description, null);

        txt_view_head = (TextView) rootView.findViewById(R.id.txt_view_head);
        txt_view_message = (TextView) rootView
                .findViewById(R.id.txt_view_message);
        txt_ms_belief = (TextView) rootView.findViewById(R.id.txt_ms_belief);
        txt_ms_integrity = (TextView) rootView.findViewById(R.id.txt_ms_integrity);
        txt_ms_respect = (TextView) rootView.findViewById(R.id.txt_ms_respect);
        txt_ms_trust = (TextView) rootView.findViewById(R.id.txt_ms_trust);
        txt_ms_humility = (TextView) rootView.findViewById(R.id.txt_ms_humility);
        img_view_image = (ImageView) rootView.findViewById(R.id.img_view_image);
        linear_txt_views = (LinearLayout) rootView
                .findViewById(R.id.linear_txt_views);
        txt_view_head.setText(listMission.get(currentPosition).getHeading());
        img_view_image.setImageResource(listMission.get(currentPosition).getPicture());
        spanText = getResources().getString(R.string.birth);
        updateViewAccordingPosition(currentPosition);
        textMessage = listMission.get(currentPosition).getDescription();
        setTextToTextView(currentPosition, textMessage);
        updateViewAccordingPosition(currentPosition);
        rootView.setBackgroundColor(listMission.get(currentPosition)
                .getColorBg());
        if (AppPrefrence.getInstance(getActivity()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA))
            setVisiblityOfMSTextViews(true);
        else
            setVisiblityOfMSTextViews(false);
        img_view_tv_bueaty = (ImageView) rootView.findViewById(R.id.img_view_tv_bueaty);
        txt_view_beauty_purpose = (TextView) rootView
                .findViewById(R.id.txt_view_beauty_purpose);
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
            img_view_tv_bueaty.setVisibility(View.VISIBLE);
            txt_view_beauty_purpose.setVisibility(View.GONE);
        } else {
            txt_view_beauty_purpose.setVisibility(View.VISIBLE);
            img_view_tv_bueaty.setVisibility(View.GONE);
        }
        return rootView;
    }

    private void setVisiblityOfMSTextViews(boolean status) {
        if (status) {
            txt_ms_belief.setVisibility(View.VISIBLE);
            txt_ms_integrity.setVisibility(View.VISIBLE);
            txt_ms_respect.setVisibility(View.VISIBLE);
            txt_ms_trust.setVisibility(View.VISIBLE);
            txt_ms_humility.setVisibility(View.VISIBLE);
        } else {
            txt_ms_belief.setVisibility(View.GONE);
            txt_ms_integrity.setVisibility(View.GONE);
            txt_ms_respect.setVisibility(View.GONE);
            txt_ms_trust.setVisibility(View.GONE);
            txt_ms_humility.setVisibility(View.GONE);
        }

    }

    public void setTextToTextView(int currentPosition, String text) {
        switch (currentPosition) {
            case 1:
                try {
                    setSpannableQuestion(text, spanText);
                } catch (Exception e) {
                    txt_view_message.setText(text);
                    e.printStackTrace();
                }
                break;

            default:
                txt_view_message.setText(text);
                break;
        }
    }

    private void updateViewAccordingPosition(int currentPosition) {
        if (currentPosition == 1) {
            linear_txt_views.setVisibility(View.VISIBLE);
            img_view_image.setVisibility(View.GONE);
        } else {
            linear_txt_views.setVisibility(View.GONE);
            img_view_image.setVisibility(View.VISIBLE);
        }
    }

    private void setSpannableQuestion(String spannableString, String spanText) throws Exception {
        int first_index = spannableString.indexOf(spanText);
        int afterFirst = first_index + spanText.length();
        int last_index = (spannableString.substring(afterFirst)).indexOf(spanText);
        int final_index = last_index + spanText.length();

        AppLogger.e("setSpannableQuestion",
                "first_index " + first_index + " , afterFirst " + afterFirst + ", last_index " + last_index + " final_index, " + final_index);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.HELVETICA_NEUE_LIGHT);
        CustomTypefaceSpan type1 = new CustomTypefaceSpan("", font);

        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.HELVETICA_NEUE_BOLD_ITALIC);
        CustomTypefaceSpan type2 = new CustomTypefaceSpan("", font2);

        SpannableStringBuilder SS = new SpannableStringBuilder(spannableString.substring(0, afterFirst));
        SpannableStringBuilder finalSS = new SpannableStringBuilder();
        SS.setSpan(type1, 0, first_index, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(type2, first_index, afterFirst - 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//        finalSS.append(SS);
        SpannableStringBuilder SS2 = new SpannableStringBuilder(spannableString.substring(afterFirst));
        SS2.setSpan(type1, 0, last_index,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS2.setSpan(type2, last_index, final_index,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        finalSS.append(SS2);
        txt_view_message.setText(SS.append(SS2));
    }

}
