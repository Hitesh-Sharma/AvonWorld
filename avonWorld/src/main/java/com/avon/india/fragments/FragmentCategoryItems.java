package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterCategoryList;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.db.Constants.Extras;
import com.avon.india.db.IOnItemReceivedListener;
import com.avon.india.fragments.paggers.FragmentPaggerItemsDetails;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.interfaces.IOnPagginationCompleted;
import com.avon.india.interfaces.IOnscrollUp;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.LoadProducts;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentCategoryItems extends Fragment implements
        IOnItemReceivedListener, OnItemClickListener, IOnscrollUp,
        ILoadListner, IOnPagginationCompleted {

    private GridView grid_view_category_list;
    private String _item_category;
    private int _category_id, country_id;
    private List<CategoryItem> _list_category_items;
    // Defining the category list adapter
    private AdapterCategoryList _adapter_category_list;
    private IOnPagginationCompleted ionPagging;
    private int original_position, list_size;
    private int MAX_LIMIT = 10;
    private ArrayList<CategoryItem> loaded_product_details;

    public static final FragmentCategoryItems getInstance(String category,
                                                          int category_id) {
        FragmentCategoryItems fr = new FragmentCategoryItems();
        Bundle bundle = new Bundle();
        bundle.putString(Extras.EXTRA_CATEGORY, category);
        bundle.putInt(Extras.EXTRA_CATEGORY_ID, category_id);
        AppLogger.e("category", "" + category);
        fr.setArguments(bundle);
        return fr;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_category_item, null);
        grid_view_category_list = (GridView) rootView
                .findViewById(R.id.gridvw_frid_fragment_category);

        Bundle bundle = getArguments();
        if (bundle != null) {
            _item_category = bundle.getString(Extras.EXTRA_CATEGORY);
            _category_id = bundle.getInt(Extras.EXTRA_CATEGORY_ID);
            if (Avon.getInstance().getProductsItemsList() != null) {
                _list_category_items = Avon.getInstance()
                        .getProductsItemsList().get(_category_id);
                onItemReceived(_list_category_items);
            }
        }
        return rootView;
    }

    @Override
    public void onItemReceived(List<CategoryItem> items) {
        if (items != null) {
            _adapter_category_list = new AdapterCategoryList(Avon.getInstance()
                    .getActivityInstance(), _list_category_items);
            _adapter_category_list.setOnScrollUpListner(this);
            _adapter_category_list.setOnPagginationLoaded(this);
            grid_view_category_list.setAdapter(_adapter_category_list);
            grid_view_category_list.setOnItemClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> paramAdapterView, View paramView,
                            int position, long id) {
        FragmentPaggerItemsDetails fragment = new FragmentPaggerItemsDetails();
        fragment.setSelectedCategory(position, _item_category, _category_id);
        fragment.setOnScrollUpListner(this);
        Avon.getInstance().getFlowOrganization().add(fragment, true);
    }

    @Override
    public void onScrolledUp(int lastIndex, IOnPagginationCompleted ionPagging,
                             boolean showLoader) {
        this.ionPagging = ionPagging;
        original_position = lastIndex - 1;
        list_size = lastIndex;
        Log.e("", "length of list and page - " + lastIndex);
        checkForLoadMoreItems(lastIndex, showLoader);
    }

    private void checkForLoadMoreItems(int nextIndex, boolean showLoader) {
        List<LoadProducts> listLoadProductsStatus = Avon.getInstance()
                .getListLoadProductsStatus();
        if (listLoadProductsStatus == null || listLoadProductsStatus.size() == 0)
            return;
        for (int i = 0; i < listLoadProductsStatus.size(); i++) {
            String categroyName = listLoadProductsStatus.get(i)
                    .getCategoryName();
            Log.e("category name", " - " + categroyName);
            if (_item_category.equalsIgnoreCase(categroyName)) {
                if (Avon.getInstance().getListLoadProductsStatus().get(i)
                        .getIsToLoadMoreProducts()) {
                    callAsync(nextIndex, showLoader);
                } else {
                    Log.e("list already loaded", " - " + categroyName);
                }
            }
        }
    }

    private void callAsync(int startIndex, boolean showLoader) {
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            country_id = AppPrefrence.getInstance(getActivity())
                    .getMyCountryId();
            HashMap<String, String> categorieProducts = RequestFormater
                    .getCategoryProductDetails(startIndex, country_id,
                            _category_id);
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance(), showLoader);
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.PRODUCTS_BY_CATEGORY);
            loader.startLoading(WebUrls.URL_PRODUCTS, categorieProducts);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast(null);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null)
            return;

        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            // Avon.getInstance().getActivityInstance()
            // .showToast(response.getResponseMessage());
            updateLoadStatus(0);
            if (ionPagging != null) {
                ionPagging.onPaggingCompleted(false, original_position,
                        list_size);
            }
            return;
        }
        switch (response.getRequestedType()) {
            case PRODUCTS_BY_CATEGORY:
                loaded_product_details = (ArrayList<CategoryItem>) response
                        .getResponse();
                if (loaded_product_details != null
                        && loaded_product_details.size() > 0) {
                    updateLoadStatus(loaded_product_details.size());
                    saveProductItemsList(loaded_product_details);
                    if (ionPagging != null) {
                        ionPagging.onPaggingCompleted(true, original_position,
                                list_size);
                    }
                    _list_category_items = Avon.getInstance()
                            .getProductsItemsList().get(_category_id);
                    _adapter_category_list.notifyDataSetChanged();
                }
                break;

            default:
                break;
        }
    }

    public void saveProductItemsList(List<CategoryItem> details_List) {
        SparseArray<List<CategoryItem>> items = Avon.getInstance()
                .getProductsItemsList();
        List<CategoryItem> subList = null;
        subList = items.get(_category_id);
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (subList != null) {
            for (CategoryItem item : details_List) {
//                if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
                item.setImageUrl(item.getFullImageUrl());
//                }
                subList.add(item);
            }
        }
        Avon.getInstance().getProductsItemsList().remove(_category_id);
        Avon.getInstance().getProductsItemsList().put(_category_id, subList);

    }

    public void updateLoadStatus(int maxItems) {
        List<LoadProducts> listLoadProductsStatus = Avon.getInstance()
                .getListLoadProductsStatus();
        if (listLoadProductsStatus == null || listLoadProductsStatus.size() == 0) {
        } else {
            for (int i = 0; i < listLoadProductsStatus.size(); i++) {
                int categroyId = listLoadProductsStatus.get(i).getCategoryId();
                Log.e("category name", " - " + categroyId);
                if (_category_id == categroyId) {
                    if (maxItems < MAX_LIMIT) {
                        Avon.getInstance().getListLoadProductsStatus().get(i)
                                .setIsToLoadMoreProducts(false);
                    }
                }
            }
        }
    }

    @Override
    public void onPaggingCompleted(boolean isMoreItemsThere, int position,
                                   int list_size) {

    }

}
