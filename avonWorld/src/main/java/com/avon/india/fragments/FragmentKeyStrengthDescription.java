package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.customviews.CustomTypefaceSpan;
import com.avon.india.itemproperty.KeyStrengthsList;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.FontFiles;

import org.w3c.dom.Text;

import java.util.List;

@SuppressLint("InflateParams")
public class FragmentKeyStrengthDescription extends Fragment {
    ImageView img_offers_image;
    private int selectedPosition = 0;
    List<KeyStrengthsList> keyStrengthList;
    private TextView txt_view_question, txt_view_answer, txt_view_title;
    private ImageView img_view_relatedImage, img_view_logo;
    private String malayie_do_you_know;
    private final String ph_board_text = "development and beauty and a platform";
    private boolean isToBold = false;
    private ImageView img_view_tv_bueaty;
    private TextView txt_view_beauty_purpose;

    public void setSelectedListPosition(int selectedPosition,
                                        List<KeyStrengthsList> keyStrengthList) {
        this.selectedPosition = selectedPosition;
        this.keyStrengthList = keyStrengthList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(
                R.layout.fragment_key_strength_description, null);
        if (keyStrengthList != null && keyStrengthList.size() > 0)
            rootView.setBackgroundColor(keyStrengthList.get(selectedPosition)
                    .getBgColorCode());
        malayie_do_you_know = getResources().getString(R.string.did_you_know);
        txt_view_title = (TextView) rootView
                .findViewById(R.id.txt_view_title);
        txt_view_question = (TextView) rootView
                .findViewById(R.id.txt_view_question);
        txt_view_answer = (TextView) rootView
                .findViewById(R.id.txt_view_answer);
        img_view_relatedImage = (ImageView) rootView
                .findViewById(R.id.img_view_relatedImage);
        img_view_logo = (ImageView) rootView.findViewById(R.id.img_view_logo);
        img_view_tv_bueaty = (ImageView) rootView.findViewById(R.id.img_view_tv_bueaty);
        txt_view_beauty_purpose = (TextView) rootView
                .findViewById(R.id.txt_view_beauty_purpose);
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
            img_view_tv_bueaty.setVisibility(View.VISIBLE);
            txt_view_beauty_purpose.setVisibility(View.GONE);
        } else {
            txt_view_beauty_purpose.setVisibility(View.VISIBLE);
            img_view_tv_bueaty.setVisibility(View.GONE);
        }

        if (selectedPosition == 9) {
            if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES))
                isToBold = true;
            else
                isToBold = false;
        } else
            isToBold = false;
        img_view_relatedImage.setImageDrawable(getResources().getDrawable(
                keyStrengthList.get(selectedPosition).getKeyImages()));
        setSpannableQuestion(malayie_do_you_know + " "
                + keyStrengthList.get(selectedPosition).getQuestion());
        txt_view_answer.setText(keyStrengthList.get(selectedPosition)
                .getResponse());
        txt_view_title.setText((selectedPosition + 1) + ". "
                + keyStrengthList.get(selectedPosition)
                .getTxt_Headings());
        return rootView;
    }

    private void setSpannableQuestion(String spannableString) {
        // txt_view_question.setTextSize(getActivity().getResources()
        // .getDimension(R.dimen.text_size_medium));
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.HELVETICA_NEUE_BOLD_ITALIC);
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.TIMES_ITALIC);
        SpannableStringBuilder SS = new SpannableStringBuilder(spannableString);
        SS.setSpan(new CustomTypefaceSpan("", font), 0,
                malayie_do_you_know.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        if (isToBold) {
            int forPh = spannableString.indexOf(ph_board_text);
            Log.e("forPh", "forPh " + forPh);
            SS.setSpan(new CustomTypefaceSpan("", font2),
                    malayie_do_you_know.length(), forPh - 1,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", font), forPh,
                    forPh + ph_board_text.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", font2),
                    ph_board_text.length(), spannableString.length(),
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        } else {
            SS.setSpan(new CustomTypefaceSpan("", font2),
                    malayie_do_you_know.length(), spannableString.length(),
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        txt_view_question.setText(SS);
    }

}
