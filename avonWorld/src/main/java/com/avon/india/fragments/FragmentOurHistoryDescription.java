package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.customviews.CustomTypefaceSpan;
import com.avon.india.itemproperty.OurHistory;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.FontFiles;

import java.util.List;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class FragmentOurHistoryDescription extends Fragment {

    private TextView txt_view_history, txt_view_description,
            txt_view_image_one, txt_view_image_two;
    private LinearLayout linear_view_one, linear_view_two;
    private ImageView img_view_image_one, img_view_image_two;
    private ImageView img_view_tv_bueaty;
    private TextView txt_view_beauty_purpose;
    private View view_between;
    private int selectedPosition = 0;
    private final String NO_CHANGE = "NO";
    private String spam_two_a = "1902:", spam_two_b = "1939:",
            spam_two_c = "1955: ";
    private String spam_three_a = "1961:", spam_three_b = "1974:",
            spam_three_c = "1978:";
    private String spam_four_a = "1998:", spam_four_b = "1992:",
            spam_four_c = NO_CHANGE;
    private String spam_five_a = "1996:", spam_five_b = "2005:",
            spam_five_c = "2011:", spam_five_d = "2012:",
            spam_five_e = "Today:";
    private List<OurHistory> listHistory;

    public void setSelectedListPosition(int selectedPosition,
                                        List<OurHistory> listHistory) {
        this.selectedPosition = selectedPosition;
        this.listHistory = listHistory;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(
                R.layout.fragment_our_history_description, null);

        txt_view_history = (TextView) rootView
                .findViewById(R.id.txt_view_history);
        txt_view_description = (TextView) rootView
                .findViewById(R.id.txt_view_description);
        txt_view_image_one = (TextView) rootView
                .findViewById(R.id.txt_view_image_one);
        txt_view_image_two = (TextView) rootView
                .findViewById(R.id.txt_view_image_two);
        linear_view_one = (LinearLayout) rootView
                .findViewById(R.id.linear_view_one);
        linear_view_two = (LinearLayout) rootView
                .findViewById(R.id.linear_view_two);
        img_view_image_one = (ImageView) rootView
                .findViewById(R.id.img_view_image_one);
        img_view_image_two = (ImageView) rootView
                .findViewById(R.id.img_view_image_two);
        view_between = (View) rootView.findViewById(R.id.view_between);
        setFixedHeightOfImageView(selectedPosition);
        adjustWeightForImages(selectedPosition);
        // adjustDescriptionTextGravity(selectedPosition);
        fillDataFromList(selectedPosition);
        rootView.setBackgroundColor(listHistory.get(selectedPosition)
                .getColorBg());

        img_view_tv_bueaty = (ImageView) rootView.findViewById(R.id.img_view_tv_bueaty);
        txt_view_beauty_purpose = (TextView) rootView
                .findViewById(R.id.txt_view_beauty_purpose);
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (country_name != null && country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
            img_view_tv_bueaty.setVisibility(View.VISIBLE);
            txt_view_beauty_purpose.setVisibility(View.GONE);
        } else {
            txt_view_beauty_purpose.setVisibility(View.VISIBLE);
            img_view_tv_bueaty.setVisibility(View.GONE);
        }

        return rootView;
    }

    private void setFixedHeightOfImageView(int position) {
        try {
            int height_one = 0, height_two = 0;
            Bitmap bmpOne = BitmapFactory.decodeResource(getResources(),
                    listHistory.get(position).getPicturesList().get(0));
            height_one = bmpOne.getHeight();
            Bitmap bmpTwo = BitmapFactory.decodeResource(getResources(),
                    listHistory.get(position).getPicturesList().get(1));
            height_two = bmpTwo.getHeight();
            int size = height_one;
            if (height_one < height_two) {
                size = height_one;
            } else {
                size = height_two;
            }
            AppLogger.e("image sizes", "height_one " + height_one + ", height_two " + height_two + ", size " + size);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, size);
            img_view_image_one.setLayoutParams(params);
            img_view_image_two.setLayoutParams(params);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private void fillDataFromList(int position) {
        txt_view_history.setText(listHistory.get(position).getHeading());
        txt_view_image_one.setText(listHistory.get(position).getPicturesText()
                .get(0));
        img_view_image_one.setImageResource(listHistory.get(position)
                .getPicturesList().get(0));
        txt_view_image_two.setText(listHistory.get(position).getPicturesText()
                .get(1));
        img_view_image_two.setImageResource(listHistory.get(position)
                .getPicturesList().get(1));
        // txt_view_description.setText(
        getDescriptionFromPosition(selectedPosition, listHistory.get(position)
                .getDescription());
        // );
    }

    private String getDescriptionFromPosition(int position,
                                              List<String> description) {
        String finalText = "";
        switch (position) {
            case 0:
                for (String desc : description)
                    finalText += desc;
                txt_view_description.setText(finalText);
                break;
            case 1:
                finalText = setSpannableQuestion(description, spam_two_a,
                        spam_two_b, spam_two_c);
                break;
            case 2:
                finalText = setSpannableQuestion(description, spam_three_a,
                        spam_three_b, spam_three_c);
                break;
            case 3:
                finalText = setSpannableQuestion(description, spam_four_a,
                        spam_four_b, spam_four_c);
                break;
            case 4:
                finalText = setSpannableQuestion(description, spam_five_a,
                        spam_five_b, spam_five_c, spam_five_d, spam_five_e);
                break;

            default:
                break;
        }
        return finalText;
    }

    private String setSpannableQuestion(List<String> stringDescription,
                                        String... spanString) {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.HELVETICA_NEUE_BOLD_ITALIC);
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(),
                FontFiles.TIMES_ITALIC);

        SpannableStringBuilder finalSS = new SpannableStringBuilder();
        for (int i = 0; i < stringDescription.size(); i++) {
            String desc = stringDescription.get(i);
            SpannableStringBuilder SS = new SpannableStringBuilder(desc);
            if (!spanString[i].equalsIgnoreCase(NO_CHANGE)) {
                SS.setSpan(new CustomTypefaceSpan("", font), 0,
                        spanString[i].length(),
                        Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                SS.setSpan(new CustomTypefaceSpan("", font2),
                        spanString[i].length(), desc.length(),
                        Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            }
            finalSS.append(SS);
            finalSS.append("\n");
        }
        txt_view_description.setText(finalSS);
        return finalSS.toString();
    }

    // private String setSpannableQuestion(List<String> stringDescription,
    // String... spanString) {
    // Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
    // FontFiles.HELVETICA_NEUE_BOLD_ITALIC);
    // Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(),
    // FontFiles.TIMES_ITALIC);
    // SpannableStringBuilder SS = new SpannableStringBuilder(
    // stringDescription);
    // // SpannableStringBuilder finalSS = new SpannableStringBuilder();
    // for (int i = 0; i < spanString.length; i++) {
    // int startIndex = stringDescription.indexOf(spanString[i]);
    // int finalIndex = startIndex + spanString[i].length() + i * 2;
    // SS.setSpan(new CustomTypefaceSpan("", font), startIndex,
    // finalIndex, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    // // finalSS.append(SS);
    // if (i < spanString.length - 1) {
    // int newLineIndex = stringDescription.indexOf(spanString[i + 1]);
    // SS.setSpan(new CustomTypefaceSpan("", font2), finalIndex,
    // newLineIndex, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    // // finalSS.append(SS);
    // // finalSS.append("\n");
    // SS.append("\n");
    // } else {
    // SS.setSpan(new CustomTypefaceSpan("", font2), finalIndex,
    // stringDescription.length(),
    // Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    // // finalSS.append(SS);
    // }
    // }
    // return SS.toString();
    // }

    @SuppressLint("InlinedApi")
    private void adjustDescriptionTextGravity(int position) {
        if (position == 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            txt_view_description.setLayoutParams(params);
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.START;
            txt_view_description.setLayoutParams(params);
        }
    }

    private void adjustWeightForImages(int position) {
        float weightLeftView = 1.0f, weightRightView = 0.0f, weightMiddleView = 1.0f;
        switch (position) {
            case 0:
                weightLeftView = 1.0f;
                weightMiddleView = 0.1f;
                weightRightView = 1.0f;
                break;
            case 1:
                weightLeftView = 0.8f;
                weightMiddleView = 0.1f;
                weightRightView = 1.3f;
                break;
            case 2:
                weightLeftView = 0.8f;
                weightMiddleView = 0.1f;
                weightRightView = 1.3f;
                break;
            case 3:
                weightLeftView = 0.9f;
                weightMiddleView = 0.05f;
                weightRightView = 1.1f;
                break;
            case 4:
                weightLeftView = 0.65f;
                weightMiddleView = 0.1f;
                weightRightView = 1.3f;
                break;

            default:
                break;
        }
        LinearLayout.LayoutParams paramLeft = new LinearLayout.LayoutParams(0,
                LayoutParams.WRAP_CONTENT, weightLeftView);
        paramLeft.gravity = Gravity.TOP | Gravity.START;
        LinearLayout.LayoutParams paramMiddle = new LinearLayout.LayoutParams(
                0, (int) getResources()
                .getDimension(R.dimen.padding_very_short),
                weightMiddleView);
        LinearLayout.LayoutParams paramRight = new LinearLayout.LayoutParams(0,
                LayoutParams.WRAP_CONTENT, weightRightView);
        paramRight.gravity = Gravity.TOP | Gravity.END;
        linear_view_one.setLayoutParams(paramLeft);
        linear_view_two.setLayoutParams(paramRight);
        view_between.setLayoutParams(paramMiddle);
    }

}
