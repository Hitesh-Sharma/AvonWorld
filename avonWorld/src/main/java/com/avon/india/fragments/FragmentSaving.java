package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.customviews.CustomTextViewCoalHand;
import com.avon.india.db.Constants.Extras;
import com.avon.india.interfaces.IOnFragmentUpdated;
import com.avon.india.interfaces.IOnpreparedListener;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;

@SuppressLint("InflateParams")
public class FragmentSaving extends BaseFragment implements IOnpreparedListener {

    TextView txt_view_boutique_locator;
    CustomTextViewCoalHand txt_average_price, txt_minimum_price,
            txt_maximum_price;
    private IOnFragmentUpdated _iOnFragmentUpdated;
    int screen_height = 0, circular_thing, current_position, action_bar_height,
            actual_screen_height;
    boolean isTotalSavings = false;

    public void setPosition(int position, boolean isTotalSavings) {
        current_position = position;
        this.isTotalSavings = isTotalSavings;
        AppLogger.e("index for saving", "" + position);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null)
            current_position = bundle.getInt(Extras.EXTRA_INDEX);

        View rootView = inflater.inflate(R.layout.fragment_total_saving, null);
        txt_view_boutique_locator = (TextView) rootView
                .findViewById(R.id.txt_view_boutique_locator);
        txt_average_price = (CustomTextViewCoalHand) rootView
                .findViewById(R.id.txt_first_price);
        txt_minimum_price = (CustomTextViewCoalHand) rootView
                .findViewById(R.id.txt_second_price);
        txt_maximum_price = (CustomTextViewCoalHand) rootView
                .findViewById(R.id.txt_third_price);
        String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
        if (isTotalSavings) {
            // setCircularDimns(6);
            if (country_name == null)
                txt_view_boutique_locator.setVisibility(View.VISIBLE);
            else {
                if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN) ||country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES)) {
                    txt_view_boutique_locator.setVisibility(View.VISIBLE);
                } else
                    txt_view_boutique_locator.setVisibility(View.GONE);
            }
        } else {
            // setCircularDimns(5);
            txt_view_boutique_locator.setVisibility(View.GONE);
        }
        requestOnPrepared(rootView, this);

        txt_view_boutique_locator.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Avon.getInstance().getDrawerInstance(getActivity())
                        .setSelectionByResourceId(R.id.linear_boutique);
            }
        });

        return rootView;
    }

    // private void setCircularDimns(int ratio) {
    // try {
    // screen_height = Avon.getInstance().getActivityInstance()
    // .getResources().getDisplayMetrics().heightPixels;
    // action_bar_height = (int) Avon.getInstance().getActivityInstance()
    // .getResources().getDimension(R.dimen.heights_action_bar);
    // actual_screen_height = screen_height - action_bar_height;
    // circular_thing = actual_screen_height / ratio;
    // LayoutParams params = new LayoutParams(circular_thing,
    // circular_thing);
    // relative_first.setLayoutParams(params);
    // relative_second.setLayoutParams(params);
    // relative_third.setLayoutParams(params);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

    public void setOnFragmentUpdated(IOnFragmentUpdated _iOnFragmentUpdated) {
        this._iOnFragmentUpdated = _iOnFragmentUpdated;
    }

    public void setAveragePrice(int Price) {
        String currencyIcon = Avon.getInstance().getActivityInstance()
                .getResources().getString(R.string.currency);
        if (txt_average_price == null)
            return;
        int maxPrice = Math.round(Avon.getPurchase().getMaxPrice(Price));
        int minPrice = Math.round(Avon.getPurchase().getMinPrice(Price));
        String avgPriceDesc = currencyIcon + " " + Price;
        String maxPriceDesc = currencyIcon + " " + maxPrice;
        String minPriceDesc = currencyIcon + " " + minPrice;
        if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
            avgPriceDesc = Price + " " + currencyIcon;
            maxPriceDesc = maxPrice + " " + currencyIcon;
            minPriceDesc = minPrice + " " + currencyIcon;
        }
        txt_average_price.setText(avgPriceDesc);
        txt_maximum_price.setText(maxPriceDesc);
        Log.e("", "Price is " + "Max price " + maxPrice + "Min Price "
                + minPrice + "Avergae Price" + Price);
        txt_minimum_price.setText(minPriceDesc);
    }

	/*
     * private Spannable getSpan(String source) { String rupeeIcon =
	 * Avon.getInstance().getActivityInstance()
	 * .getResources().getString(R.string.currency);
	 * 
	 * source = rupeeIcon + " " + source; int color =
	 * Avon.getInstance().getActivityInstance().getResources()
	 * .getColor(R.color.color_magenta); Spannable str = new
	 * SpannableString(source); str.setSpan(new
	 * StyleSpan(android.graphics.Typeface.BOLD), source.indexOf(rupeeIcon),
	 * source.indexOf(rupeeIcon) + rupeeIcon.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); str.setSpan(new
	 * ForegroundColorSpan(color), source.indexOf(rupeeIcon),
	 * source.indexOf(rupeeIcon) + rupeeIcon.length(), 0); str.setSpan(new
	 * RelativeSizeSpan(1.5f), source.indexOf(rupeeIcon),
	 * source.indexOf(rupeeIcon) + rupeeIcon.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	 * 
	 * str.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
	 * source.indexOf(rupeeIcon) + rupeeIcon.length() + 1, str.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); str.setSpan(new
	 * ForegroundColorSpan(Color.BLACK), source.indexOf(rupeeIcon) +
	 * rupeeIcon.length() + 1, str.length(), 0);
	 * 
	 * str.setSpan(new RelativeSizeSpan(2.2f), source.indexOf(rupeeIcon) +
	 * rupeeIcon.length() + 1, str.length(),
	 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); return str; }
	 */

    @Override
    public void onViewPrepared(View view) {
        if (_iOnFragmentUpdated != null) {
            int Price = _iOnFragmentUpdated.onUpdated(current_position, this);
            setAveragePrice(Price);
        }
    }

    @Override
    public boolean onBackPress() {
        // TODO Auto-generated method stub
        return false;
    }

}
