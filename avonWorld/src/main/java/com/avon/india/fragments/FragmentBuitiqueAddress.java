package com.avon.india.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.itemproperty.LocationItem;

public class FragmentBuitiqueAddress extends Fragment {
	private LocationItem _locations;
	static FragmentBuitiqueAddress _instance;
	private TextView txt_storeName, txt_storeAddress;

	public static FragmentBuitiqueAddress newInstance(LocationItem content,
			int index) {
		if (_instance == null || _instance._locations == null
				|| !_instance._locations.equals(content)) {
			_instance = new FragmentBuitiqueAddress();
			_instance._locations = content;
		}
		return _instance;
	}

	public LocationItem getLocation() {
		return _locations;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.adapter_buitique_address, null);
		txt_storeName = (TextView) rootView
				.findViewById(R.id.txt_view_store_name);
		txt_storeAddress = (TextView) rootView
				.findViewById(R.id.txt_view_store_address);
		txt_storeName.setText(_locations.getStoreName());
		txt_storeAddress.setText(_locations.getLocationAddress());
		return rootView;

	}
}
