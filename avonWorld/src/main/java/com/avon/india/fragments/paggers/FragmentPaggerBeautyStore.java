package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterBeautyStoreProducts;
import com.avon.india.adapter.AdpaterPaggerBeautyStore;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BackgroundSaveImageToCache;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.db.DbOperations;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.IConstants;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentPaggerBeautyStore extends BaseFragmentBackHandler
        implements OnPageChangeListener, ILoadListner, OnClickListener,
        OnItemClickListener {

    private CustomViewPager custom_pager;
    private TextView txt_view_action_bar_text;
    private TextView txt_view_description, txt_view_select_product;
    private ImageView img_view_down_arrow;
    private RelativeLayout relative_drop_down;
    private ListView list_view_products;
    private AdpaterPaggerBeautyStore adapterPagger;
    private AdapterBeautyStoreProducts adapterProducts;
    private List<CategoryItem> products_list = new ArrayList<CategoryItem>();
    private boolean isListVisible = false, isToLoadMoreProducts = true;
    private int selectedItemPosition = 0;

    @Override
    public boolean onBackPressed() {
        boolean isBack = true;
        if (isListVisible) {
            isListVisible = false;
            animateDownArrow(isListVisible);
            isBack = false;
        }
        return isBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_beauty_store_parent,
                null);

        custom_pager = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager_products);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.bueaty_store));
        txt_view_description = (TextView) rootView
                .findViewById(R.id.txt_view_description);
        txt_view_select_product = (TextView) rootView
                .findViewById(R.id.txt_view_select_product);
        img_view_down_arrow = (ImageView) rootView
                .findViewById(R.id.img_view_down_arrow);
        relative_drop_down = (RelativeLayout) rootView
                .findViewById(R.id.relative_drop_down);
        list_view_products = (ListView) rootView
                .findViewById(R.id.list_view_products);
        list_view_products.setOnItemClickListener(this);
        list_view_products.setVisibility(View.GONE);
        callAsync();
        relative_drop_down.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_drop_down:
                showProductsList();
                break;

            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View view, int position,
                            long id) {
        isListVisible = false;
        selectedItemPosition = position;
        animateDownArrow(isListVisible);
        custom_pager.setCurrentItem(selectedItemPosition);
    }

    private void showProductsList() {
        if (isListVisible) {
            isListVisible = false;
        } else {
            isListVisible = true;
        }
        animateDownArrow(isListVisible);
    }

    private void animateDownArrow(boolean isDropDownListOpen) {
        if (isDropDownListOpen) {
            list_view_products.setVisibility(View.VISIBLE);
            rotatePhoneClockwise();
        } else {
            list_view_products.setVisibility(View.GONE);
            rotatePhoneAntiClockwise();
        }
    }

    private void rotatePhoneClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.semi_anti_rotate_anim);
        img_view_down_arrow.startAnimation(rotate);
    }

    private void rotatePhoneAntiClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.semi_rotate_anim);
        img_view_down_arrow.startAnimation(rotate);
    }


    public void callAsync() {
        try {
//                if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
           List<CategoryItem> dummy_products_list = DbOperations.getProductsListList(Avon.getInstance().getActivityInstance());
            if (dummy_products_list == null || dummy_products_list.size() == 0) {
                isToLoadMoreProducts=true;
            } else {
                isToLoadMoreProducts=false;
                products_list=dummy_products_list;
                setAdapter(products_list);
                return;
            }
//                }
        } catch (Exception e) {
        }

        if (!isToLoadMoreProducts)
            return;
        if (CheckNetworkState.isNetworkAvailable(getActivity())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            int start_index = 0;
            if (products_list.size() > 0)
                start_index = products_list.size();
            int countryId = AppPrefrence.getInstance(getActivity())
                    .getMyCountryId();
            HashMap<String, String> params = RequestFormater
                    .getAllProductDetails(start_index, countryId, "");
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.PRODUCTS_FOR_BUEATY_STORE);
            loader.startLoading(WebUrls.URL_GET_ALL_PRODUCTS, params);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast(null);
        }
    }

    private void setAdapter(List<CategoryItem> list_products) {
        adapterPagger = new AdpaterPaggerBeautyStore(getChildFragmentManager(),
                list_products);
        custom_pager.setAdapter(adapterPagger);

        custom_pager.setOnPageChangeListener(this);
        if (custom_pager.getAdapter().getCount() > selectedItemPosition) {
            custom_pager.setCurrentItem(selectedItemPosition);
        }
        setProductsList(list_products);
    }

    private void setProductsList(List<CategoryItem> list_products) {
        if (list_products != null && list_products.size() > 0) {
            if (adapterProducts == null) {
                adapterProducts = new AdapterBeautyStoreProducts(Avon
                        .getInstance().getActivityInstance(), list_products,
                        this);
                list_view_products.setAdapter(adapterProducts);
            }
            updateSelectedView(list_products, selectedItemPosition);
        }
    }

    private void updateSelectedView(List<CategoryItem> list_products, int position) {
        for (int i = 0; i < list_products.size(); i++) {
            if (i == position)
                list_products.get(i).setIsSelected(true);
            else
                list_products.get(i).setIsSelected(false);
        }
        AppLogger.e("DialogFragmentSelectCountry", "selected position is " + position);
//        list_view_products.smoothScrollToPosition(position);
        if (adapterProducts != null)
            adapterProducts.notifyDataSetChanged();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {

        try {
            String priceDesc = Avon.getInstance().getActivityInstance().getResources().getString(R.string.currency) + " " + products_list.get(position).getPriceAverage() + "/-";
            if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN))
                priceDesc = products_list.get(position).getPriceAverage() + " " + Avon.getInstance().getActivityInstance().getResources().getString(R.string.currency);
            String description = products_list.get(position)
                    .getDesription() + "\n" + products_list.get(position).getWeight() + " " + priceDesc;
            txt_view_description.setText(description);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        if (products_list != null && products_list.size() <= position)
            return;
        txt_view_select_product.setText(products_list.get(position).getName());
        list_view_products.setSelection(position);
        selectedItemPosition = position;
        if (position == products_list.size() - 1) {
            if (products_list.size() % IConstants.MAX_COUNT == 0 && isToLoadMoreProducts) {
                callAsync();
            } else {
                isToLoadMoreProducts = false;
            }
        }
        updateSelectedView(products_list, selectedItemPosition);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null)
            return;

        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            isToLoadMoreProducts = false;
            Avon.getInstance().getActivityInstance()
                    .showToast(response.getResponseMessage());
            return;
        } else {
            List<CategoryItem> newlist = (ArrayList<CategoryItem>) response
                    .getResponse();
            if (newlist == null || newlist.size() == 0) {
                isToLoadMoreProducts = false;
            }
            if (products_list == null || products_list.size() == 0)
                products_list = newlist;
            else
                products_list.addAll(newlist);
            if (products_list != null && products_list.size() > 0) {
//                AppLogger.e("", "Response Details of offers Acreen is "
//                        + products_list);
//                if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
//                    products_list = DbOperations.getProductsListList(Avon.getInstance().getActivityInstance());
//                }
                setAdapter(products_list);
            } else {
                isToLoadMoreProducts = false;
                Avon.getInstance().getActivityInstance()
                        .showToast(response.getResponseMessage());
            }
        }

    }

}
