package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.customviews.TouchImageView;
import com.avon.india.supports.SupportUtils;

@SuppressLint("InflateParams")
public class FragmentScaleableView extends BaseFragment {

	private String image_url;
	private int image_id;

	public void setImageUrl(String image_url) {
		this.image_url = image_url;
	}

	public void setImageId(int image_id) {
		this.image_id = image_id;
	}

	@Override
	public boolean onBackPress() {
		return false;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_scalable_view, null);
		TouchImageView _img = (TouchImageView) view
				.findViewById(R.id.img_fragment_scalable);
		if (SupportUtils.isValidString(image_url)) {
			Avon.getInstance().getFullImageLoader()
					.startLazyLoading(image_url, _img);
		}

		if (image_id != 0) {
			_img.setImageResource(image_id);
		}

		return view;
	}

}
