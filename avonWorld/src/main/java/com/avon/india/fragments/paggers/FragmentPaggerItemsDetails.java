package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdpaterPaggerItemsDetail;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.db.IOnItemReceivedListener;
import com.avon.india.interfaces.IOnPageChangeListener;
import com.avon.india.interfaces.IOnPagginationCompleted;
import com.avon.india.interfaces.IOnscrollUp;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.ConstantsReal.IConstants;

import java.util.List;

public class FragmentPaggerItemsDetails extends Fragment implements
        IOnPageChangeListener, OnClickListener, IOnItemReceivedListener,
        OnPageChangeListener, IOnPagginationCompleted {

    private AdpaterPaggerItemsDetail adapter;
    private CustomViewPager custom_pager_items;
    private ImageButton img_btn_previous, img_btn_next, img_btn_close;
    private int current_position = 0;
    private String item_category;
    private int _category_id;
    private List<CategoryItem> _list_category_items;
    private IOnscrollUp onScrollUpListner;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_items_detail_parent,
                null);
        custom_pager_items = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager_items);

        img_btn_previous = (ImageButton) rootView
                .findViewById(R.id.img_btn_previous);
        img_btn_next = (ImageButton) rootView.findViewById(R.id.img_btn_next);
        img_btn_close = (ImageButton) rootView.findViewById(R.id.img_btn_close);
        img_btn_previous.setOnClickListener(this);
        img_btn_next.setOnClickListener(this);
        img_btn_close.setOnClickListener(this);
        if (item_category != null && !item_category.trim().equalsIgnoreCase("")) {
            if (Avon.getInstance().getProductsItemsList() != null) {
                // _list_category_items = Avon.getInstance()
                // .getProductsItemsList().get(item_category);
                _list_category_items = Avon.getInstance()
                        .getProductsItemsList().get(_category_id);
                onItemReceived(_list_category_items);
            }
        }
        return rootView;
    }

    @Override
    public void onItemReceived(List<CategoryItem> items) {
        if (items != null) {
            setAdapter();
        }
    }

    public void setSelectedCategory(int position, String category_name,
                                    int category_id) {
        current_position = position;
        item_category = category_name;
        _category_id = category_id;
    }

    public void setAdapter() {
        int list_size = _list_category_items.size();
        adapter = new AdpaterPaggerItemsDetail(getChildFragmentManager(),
                item_category, _category_id, list_size);
        custom_pager_items.setAdapter(adapter);
        custom_pager_items.setCurrentItem(current_position);
        updatePreviousNextIcons();
        custom_pager_items.setOnPageChangeListener(this);
    }

    private void updatePreviousNextIcons() {
        if (current_position == 0) {
            if (current_position == (custom_pager_items.getAdapter()
                    .getCount() - 1)) {
                img_btn_previous.setVisibility(View.INVISIBLE);
                img_btn_previous.setEnabled(false);
                img_btn_next.setVisibility(View.INVISIBLE);
                img_btn_next.setEnabled(true);
            } else {
                img_btn_previous.setVisibility(View.VISIBLE);
                img_btn_previous.setEnabled(true);
                img_btn_next.setVisibility(View.VISIBLE);
                img_btn_next.setEnabled(true);
            }
        } else if (current_position == (custom_pager_items.getAdapter()
                .getCount() - 1)) {
            img_btn_previous.setVisibility(View.VISIBLE);
            img_btn_previous.setEnabled(true);
            img_btn_next.setVisibility(View.INVISIBLE);
            img_btn_next.setEnabled(false);
        } else {
            img_btn_previous.setVisibility(View.VISIBLE);
            img_btn_previous.setEnabled(true);
            img_btn_next.setVisibility(View.VISIBLE);
            img_btn_next.setEnabled(true);
        }
    }

    public void changePageTo(int position) {
        custom_pager_items.setCurrentItem(position);
        int mCount = _list_category_items.size();
        if (position == mCount - 1 && mCount % IConstants.MAX_COUNT == 0) {
            Log.e("is at last position", "yes " + position);
            if (onScrollUpListner != null)
                onScrollUpListner.onScrolledUp(position + 1, this, true);
        }
    }

    @Override
    public void onPageChangedTo(int changed_position) {
        changePageTo(changed_position);
    }

    @Override
    public void onPageSelected(int position) {
        current_position = position;
        changePageTo(position);
        updatePreviousNextIcons();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_btn_next:
                if(custom_pager_items.getAdapter()==null)
                    return;
                if (current_position + 1 > 0
                        && current_position + 1 <= custom_pager_items.getAdapter()
                        .getCount() - 1)
                    changePageTo(current_position + 1);
                break;
            case R.id.img_btn_previous:
                if(custom_pager_items.getAdapter()==null)
                    return;
                if (current_position - 1 >= 0
                        && current_position - 1 < custom_pager_items.getAdapter()
                        .getCount() - 1)
                    changePageTo(current_position - 1);
                break;
            case R.id.img_btn_close:
                getActivity().onBackPressed();
                break;

            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setOnScrollUpListner(IOnscrollUp onScrollUpListner) {
        this.onScrollUpListner = onScrollUpListner;
    }

    @Override
    public void onPaggingCompleted(boolean isMoreItemsThere, int position,
                                   int list_size) {
        // _list_category_items = Avon.getInstance().getProductsItemsList()
        // .get(item_category);
        _list_category_items = Avon.getInstance().getProductsItemsList()
                .get(_category_id);
        int total_list_count = _list_category_items.size();
        if (!isMoreItemsThere) {
            total_list_count += 1;
        }
        try {
            custom_pager_items.setAdapter(null);
        }catch (Exception e){
            e.printStackTrace();
        }
        adapter = new AdpaterPaggerItemsDetail(getChildFragmentManager(),
                item_category, _category_id, total_list_count);
        custom_pager_items.setAdapter(adapter);
        custom_pager_items.setCurrentItem(position + 1);
    }

}
