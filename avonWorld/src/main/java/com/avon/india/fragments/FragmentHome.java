package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.fragments.paggers.FragmentPaggerCategory;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.PermissionToAccessExternalStorage;

public final class FragmentHome extends Fragment implements OnClickListener {

    private TextView txt_view_action_bar_text, txt_view_static_text;
    private ImageView img_btn_action_bar_right, img_view_model_image;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, null);

        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources().getString(R.string.home));
        txt_view_static_text = (TextView) rootView
                .findViewById(R.id.txt_view_static_text);
        img_btn_action_bar_right = (ImageView) rootView
                .findViewById(R.id.img_btn_action_bar_right);
        img_btn_action_bar_right.setVisibility(View.VISIBLE);
        img_view_model_image = (ImageView) rootView
                .findViewById(R.id.img_view_model_image);

        initModelImage();

        new PermissionToAccessExternalStorage().isToAccessStorage();

        Avon.getInstance().getDrawerInstance(Avon.getInstance().getActivityInstance()).getSlider().setSlidingEnabled(true);

        rootView.findViewById(R.id.txt_view_start_saving).setOnClickListener(
                this);
        return rootView;
    }

    private void initModelImage() {
        String country_name = AppPrefrence.getInstance(getActivity()).getMyCountryName();
        if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
            img_view_model_image.setBackgroundResource(R.drawable.india_img);
//            txt_view_static_text.setGravity(Gravity.LEFT);
        } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
            img_view_model_image.setBackgroundResource(R.drawable.tw_img);
//            txt_view_static_text.setGravity(Gravity.CENTER);
        } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
            img_view_model_image.setBackgroundResource(R.drawable.home_banner);
//            txt_view_static_text.setGravity(Gravity.LEFT);
        } else if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES)) {
            img_view_model_image.setBackgroundResource(R.drawable.ph_img);
//            txt_view_static_text.setGravity(Gravity.LEFT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_view_start_saving:
                if (new PermissionToAccessExternalStorage().isToAccessStorage())
                    Avon.getInstance().getFlowOrganization()
                            .add(new FragmentPaggerCategory(), true);
                break;
            case R.id.img_btn_menu_icon:
                Avon.getInstance().getDrawerInstance(getActivity()).showMenu();

            default:
                break;
        }

    }


}
