package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.LazyLoaderLib.ILazyDownloaderListner;
import com.avon.india.LazyLoaderLib.ScaleImageAccordingly;
import com.avon.india.R;
import com.avon.india.db.IOnItemReceivedListener;
import com.avon.india.interfaces.IOnPageChangeListener;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.SupportUtils;

import java.io.File;
import java.util.List;

public final class FragmentSelectedCategoryItems extends Fragment implements
        IOnItemReceivedListener {

    IOnPageChangeListener iOnPageChangeListner;
    private ImageView img_view_item;
    private TextView txt_view_item_name, txt_view_item_price;

    private String item_name, item_price, _item_category, image_url;
    private int current_position, _category_id;
    private List<CategoryItem> _list_category_items;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item_details, null);
        img_view_item = (ImageView) rootView.findViewById(R.id.img_view_item);
        txt_view_item_name = (TextView) rootView
                .findViewById(R.id.txt_view_item_name);

        txt_view_item_price = (TextView) rootView
                .findViewById(R.id.txt_view_item_price);

        if (Avon.getInstance().getProductsItemsList() != null) {
            // _list_category_items = Avon.getInstance().getProductsItemsList()
            // .get(_item_category);
            _list_category_items = Avon.getInstance().getProductsItemsList()
                    .get(_category_id);
            onItemReceived(_list_category_items);
        }

        return rootView;
    }

    public void setItemPosition(int position, String item_category,
                                int category_id) {
        current_position = position;
        _item_category = item_category;
        _category_id = category_id;
    }

//    private int IMAGE_WIDTH = 0, IMAGE_HEIGHT = 0;
//
//    public void getSizeOfScreen(Activity activity) {
//        Display display = activity.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        IMAGE_WIDTH = dp2px(activity, size.x);
//        IMAGE_HEIGHT = dp2px(activity, size.y);
//        Log.e("IMAGE_WIDTH , IMAGE_HEIGHT", IMAGE_WIDTH + " , " + IMAGE_HEIGHT);
//    }
//
//    private int dp2px(Activity activity, int dp) {
//        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
//                activity.getResources().getDisplayMetrics());
//    }

    @Override
    public void onItemReceived(List<CategoryItem> items) {
        try {
            if (items != null) {
                item_name = items.get(current_position).getName();
                item_price = items.get(current_position).getPriceAverage();
                // item_image_id = items.get(current_position)
                // .getImageResourceId();
                image_url = items.get(current_position).getImageUrl();
            }
            txt_view_item_name.setText(item_name);
            if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.TAIWAN)) {
                txt_view_item_price.setText(item_price + " " + Avon.getInstance().getActivityInstance().getResources().getString(R.string.currency));
            } else
                txt_view_item_price.setText(Avon.getInstance().getActivityInstance().getResources().getString(R.string.currency) + " " + item_price);
            // Avon.getInstance().getLocalImageLoader()
            // .loadImage(item_image_id, img_view_item);
            // Avon.getInstance().getImageLoader()
            // .startLazyLoading(image_url, img_view_item);
//            getSizeOfScreen(Avon.getInstance().getActivityInstance());
            Avon.getInstance().getThumbImageLoader()
                    .startLazyLoading(image_url, img_view_item);
//            downloadImage(image_url);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void downloadImage(final String url) {
//        File file = SupportUtils.getFilepathFromUrl(url);
//        if (file != null) {
//            if (file.exists()) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                ScaleImageAccordingly scaleBitmap = new ScaleImageAccordingly(
//                        IMAGE_WIDTH, IMAGE_HEIGHT);
//                Bitmap returnedBitmap = scaleBitmap.scaleBitmapForFullImage(myBitmap);
//                img_view_item.setImageBitmap(returnedBitmap);
//            } else {
////                holder._img_item_image
////                        .setImageDrawable(Avon.getInstance()
////                                .getActivityInstance().getResources()
////                                .getDrawable(R.drawable.placeholder_image));
//                Log.e("file.exists() else", "file.exists() else");
//                Avon.getInstance()
//                        .getDownloadImage(Avon.getInstance()
//                                .getActivityInstance())
//                        .registerLoadingCompleteListner(
//                                new ILazyDownloaderListner() {
//
//                                    @Override
//                                    public void onLaziDownloadingStarts() {
//
//                                    }
//
//                                    @Override
//                                    public void onLaziDownloadingComplete(
//                                            boolean isSuccess, File file) {
//                                        if (isSuccess) {
//                                            File file2 = SupportUtils
//                                                    .getFilepathFromUrl(url);
//                                            if (file != null) {
//                                                if (file.exists()) {
//                                                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                                    ScaleImageAccordingly scaleBitmap = new ScaleImageAccordingly(
//                                                            IMAGE_WIDTH, IMAGE_HEIGHT);
//                                                    Bitmap returnedBitmap = scaleBitmap.scaleBitmapForFullImage(myBitmap);
//                                                    img_view_item.setImageBitmap(returnedBitmap);
//                                                }
//                                            } else {
//                                                // Bitmap resized =
//                                                // ThumbnailUtils.extractThumbnail(
//                                                // BitmapFactory
//                                                // .decodeFile(file
//                                                // .getPath()),
//                                                // 50, 50);
//                                                // holder.img_message_chat_cell
//                                                // .setImageBitmap(resized);
//                                                // holder.img_view_retry_dwonload
//                                                // .setVisibility(View.GONE);
//                                            }
//                                        } else {
//                                            Log.e("isSuccess", "isSuccess "
//                                                    + isSuccess);
//                                        }
//                                    }
//                                });
//                Avon.getInstance()
//                        .getDownloadImage(
//                                Avon.getInstance()
//                                        .getActivityInstance())
//                        .startLazyDownloading(url, file);
//            }
//        }
//    }

}
