package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterPaggerAboutOne;
import com.avon.india.adapter.AdapterPaggerHistory;
import com.avon.india.adapter.AdapterPaggerOurMission;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.enums.EnumSet.CategoryType;

public class FragmentOurHistory extends BaseFragment {

    private CustomViewPager custom_pager;
    private TextView txt_view_action_bar_text;
    private CategoryType categoryType;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_history, null);
        custom_pager = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager_history);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        setAdapter();
        return rootView;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    private void setAdapter() {
        switch (categoryType) {
            case OUR_HISTORY:
                txt_view_action_bar_text.setText(Avon.getInstance()
                        .getActivityInstance().getResources()
                        .getString(R.string.our_history));
                AdapterPaggerHistory adapter_history = new AdapterPaggerHistory(
                        getChildFragmentManager());
                custom_pager.setAdapter(adapter_history);
                break;
            case OUR_MISSION:
                txt_view_action_bar_text.setText(Avon.getInstance()
                        .getActivityInstance().getResources()
                        .getString(R.string.our_mission));
                AdapterPaggerOurMission adapter_mission = new AdapterPaggerOurMission(
                        getChildFragmentManager());
                custom_pager.setAdapter(adapter_mission);
                break;
            case ABOUT_ONE:
                txt_view_action_bar_text.setText(Avon.getInstance()
                        .getActivityInstance().getResources()
                        .getString(R.string.about_avon));
                AdapterPaggerAboutOne adapter_about = new AdapterPaggerAboutOne(
                        getChildFragmentManager());
                custom_pager.setAdapter(adapter_about);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onBackPress() {
        return false;
    }

}
