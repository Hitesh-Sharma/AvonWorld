package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avon.india.R;
import com.avon.india.adapter.AdpaterPaggerKeyStrengthDetail;
import com.avon.india.adapter.AdpaterPaggerOurHistory;
import com.avon.india.adapter.AdpaterPaggerOurMission;
import com.avon.india.basehelper.BaseAboutUsResourceLoader;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.enums.EnumSet.AboutUsType;
import com.avon.india.itemproperty.KeyStrengthsList;
import com.avon.india.itemproperty.OurHistory;
import com.avon.india.itemproperty.OurMission;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentPaggerKeyStrengthDetails extends BaseAboutUsResourceLoader
		implements OnPageChangeListener {

	private CustomViewPager custom_pager;
	private TextView txt_view_action_bar_text;
	private AboutUsType aboutType = AboutUsType.HISTORY;

	List<Integer> listBGColors;

	// AboutUsType STRENGTH
	private List<KeyStrengthsList> keyStrengthList;
	private List<String> listKeyStrengthValuse = new ArrayList<String>();
	private List<String> listKeyStrengthQuestions = new ArrayList<String>();
	private List<String> listKeyStrengthAnswers = new ArrayList<String>();
	private List<Integer> listKeyImages;
	private final int KEY_STRENGTH_LENGTH = 10;
	private AdpaterPaggerKeyStrengthDetail adapterStrength;

	// AboutUsType HISTORY
	private List<OurHistory> listOurHistory;
	private List<String> listHeadings = new ArrayList<String>();
	private List<Integer> listHistoryBgColors = new ArrayList<Integer>();
	private SparseArray<List<String>> listDescription = new SparseArray<List<String>>();
	private SparseArray<List<Integer>> listPictures = new SparseArray<List<Integer>>();
	private SparseArray<List<String>> listPicturesName = new SparseArray<List<String>>();
	private AdpaterPaggerOurHistory adapterHistory;

	// AboutUsType MISSION
	private List<OurMission> listOurMission;
	private List<String> listMissionTitles = new ArrayList<String>();
	private List<String> listMissionDesc = new ArrayList<String>();
	private List<Integer> listMissionBgColors = new ArrayList<Integer>();
	private List<Integer> listMissionImages = new ArrayList<Integer>();
	private AdpaterPaggerOurMission adapterMission;

	public void setAboutUsType(AboutUsType aboutType) {
		this.aboutType = aboutType;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_history, null);

		custom_pager = (CustomViewPager) rootView
				.findViewById(R.id.custom_pager_history);
		txt_view_action_bar_text = (TextView) rootView
				.findViewById(R.id.txt_view_action_bar_text);
		customizeDataAccordingType(aboutType);
		return rootView;
	}

	private void customizeDataAccordingType(AboutUsType aboutType) {
		switch (aboutType) {
		case HISTORY:
			initOurHistory();
			break;
		case MISSION:
			initOurMission();
			break;
		case STRENGTH:
			initStrengthItems();
			break;

		default:
			break;
		}
		updateActionBarText(0);
	}

	private void initOurMission() {
		listMissionTitles = initializeOurMissionTitles();
		listMissionDesc = initializeOurMissionDescription();
		listMissionBgColors = initializeMissionBgColors();
		listMissionImages = initializeOurMissionImages();
		listOurMission = new ArrayList<OurMission>();
		for (int i = 0; i < listMissionTitles.size(); i++) {
			OurMission mission = new OurMission(listMissionTitles.get(i),
					listMissionDesc.get(i), listMissionImages.get(i),
					listMissionBgColors.get(i));
			listOurMission.add(mission);
		}
		setMissionAdapter();
	}

	private void setMissionAdapter() {
		adapterMission = new AdpaterPaggerOurMission(getChildFragmentManager(),
				listOurMission);
		custom_pager.setAdapter(adapterMission);
		custom_pager.setOnPageChangeListener(this);
		if (custom_pager.getAdapter().getCount() > 0) {
			custom_pager.setCurrentItem(0);
		}
	}

	private void initOurHistory() {
		listPicturesName = initializeOurHistoryPictureStrings();
		listPictures = initializeOurHistoryImages();
		listDescription = initializeOutHistoryDescription();
		listHeadings = initializeOutHistoryTitle();
		listHistoryBgColors = initializeHistoryBgColors();
		listOurHistory = new ArrayList<OurHistory>();
		for (int i = 0; i < listHeadings.size(); i++) {
			OurHistory history = new OurHistory(listHeadings.get(i),
					listDescription.get(i), listPictures.get(i),
					listPicturesName.get(i), listHistoryBgColors.get(i));
			listOurHistory.add(history);
		}
		setHistoryAdapter();
	}

	private void setHistoryAdapter() {
		adapterHistory = new AdpaterPaggerOurHistory(getChildFragmentManager(),
				listOurHistory);
		custom_pager.setAdapter(adapterHistory);
		// custom_pager.setOnPageChangeListener(this);
		if (custom_pager.getAdapter().getCount() > 0) {
			custom_pager.setCurrentItem(0);
		}
	}

	private void initStrengthItems() {
		listBGColors = initializeListBgColors();
		listKeyImages = initializeKeyImages();
		initializeKeyStrengthStrings();
		keyStrengthList = new ArrayList<KeyStrengthsList>();
		for (int i = 0; i < KEY_STRENGTH_LENGTH; i++) {
			KeyStrengthsList key_heading = new KeyStrengthsList(
					listKeyStrengthValuse.get(i),
					listKeyStrengthQuestions.get(i),
					listKeyStrengthAnswers.get(i), listBGColors.get(i),
					listKeyImages.get(i));
			keyStrengthList.add(key_heading);
		}
		setStrengthAdapter();
	}

	private void setStrengthAdapter() {
		adapterStrength = new AdpaterPaggerKeyStrengthDetail(
				getChildFragmentManager(), keyStrengthList);
		custom_pager.setAdapter(adapterStrength);
		custom_pager.setOnPageChangeListener(this);
		if (custom_pager.getAdapter().getCount() > 0) {
			custom_pager.setCurrentItem(0);
		}
	}

	private void initializeKeyStrengthStrings() {
		String[] questions = getResources().getStringArray(
				R.array.key_strength_questions);
		String[] answers = getResources().getStringArray(
				R.array.key_strength_answers);
		String[] values = getResources().getStringArray(
				R.array.key_strength_values);
		for (String ques : questions)
			listKeyStrengthQuestions.add(ques);
		for (String ans : answers)
			listKeyStrengthAnswers.add(ans);
		for (String val : values)
			listKeyStrengthValuse.add(val);
	}

	@Override
	public void onPageSelected(int position) {

	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		updateActionBarText(position);
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	private void updateActionBarText(int position) {
		String title = "";
		switch (aboutType) {
		case HISTORY:
			title = getResources().getString(R.string.our_history);
			break;
		case STRENGTH:
			title = getResources().getString(R.string.our_strengths);
			break;
		case MISSION:
			title = listOurMission.get(position).getHeading();
			break;

		default:
			break;
		}
		txt_view_action_bar_text.setText(title);
	}
}
