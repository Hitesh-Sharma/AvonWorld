package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.customviews.CustomTextViewTimesBoldItalic;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.StringConstants;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.DeviceIdReceiver;
import com.avon.india.supports.RequestFormater;
import com.avon.india.supports.SupportUtils;

import java.util.HashMap;

@SuppressLint("InflateParams")
public class FragmentFeedback extends Fragment implements OnClickListener,
        ILoadListner {

    EditText edt_email_feedback_fragment , edt_phone_number,
            edt_message_feedback_fragment;
    CustomTextViewTimesBoldItalic txt_view_feedback_submit;
    private TextView txt_view_action_bar_text;

    int i = 10;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_feedback, null);
        edt_email_feedback_fragment = (EditText) rootView
                .findViewById(R.id.edt_email_feedback_fragment);
        edt_phone_number = (EditText) rootView
                .findViewById(R.id.edt_phone_number);
        edt_message_feedback_fragment = (EditText) rootView
                .findViewById(R.id.edt_message_feedback_fragment);
        txt_view_feedback_submit = (CustomTextViewTimesBoldItalic) rootView
                .findViewById(R.id.txt_view_feedback_submit);
        txt_view_feedback_submit.setOnClickListener(this);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.feedback));
        Avon.getInstance()
                .getDrawerInstance(Avon.getInstance().getActivityInstance())
                .getSlider().setSlidingEnabled(true);

        return rootView;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_view_feedback_submit:

                submitFeedback();

                break;

            default:
                break;
        }
    }

    public void submitFeedback() {
        String email = edt_email_feedback_fragment.getText().toString();
        String phoneNumber = edt_phone_number.getText().toString();
        String message = edt_message_feedback_fragment.getText().toString();
        if (!SupportUtils.isValidEmail(email)) {
            SupportUtils.setErrorMsg(StringConstants.EMAIL_INVALID,
                    edt_email_feedback_fragment);
            edt_email_feedback_fragment.requestFocus();
        } else if (!SupportUtils.isValidContact(phoneNumber)) {
            SupportUtils.setErrorMsg(StringConstants.PHONENUMBER_INVALID,
                    edt_phone_number);
            edt_phone_number.requestFocus();
        } else if (!SupportUtils.isValidString(message)) {
            Avon.getInstance().getActivityInstance()
                    .showToast(ToastMessages.INVALID_MESSAGE);
        } else {
            if (CheckNetworkState.isNetworkAvailable(getActivity())) {
                String DeviceId = DeviceIdReceiver.getInstance()
                        .getHashedDeivceId(getActivity());
                String regId = GCMRegistrar.getRegistrationId(getActivity());
                int country_id = AppPrefrence.getInstance(getActivity())
                        .getMyCountryId();

                HashMap<String, String> params = RequestFormater
                        .getSubmittedFeedback(DeviceId, regId, email,
                                phoneNumber, message, country_id);
                MyLoader loader = new MyLoader(Avon.getInstance()
                        .getActivityInstance());
                loader.setOnLoadListner(this);
                loader.setRequestType(RequestType.FEEDBACK);
                loader.startLoading(WebUrls.URL_SEND_FEEDBACK, params);
            } else {
                Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet to Send Feedback");
            }
        }
    }

    @Override
    public void afterLoadComplete(ResponseParser response) {
        if (response == null)
            return;
        if (response.getResponseCode() == IResponseCode.RESPONSE_SUCCESS) {
            edt_email_feedback_fragment.setText("");
            edt_phone_number.setText("");
            edt_message_feedback_fragment.setText("");
        }

        Avon.getInstance().getActivityInstance()
                .showToast(response.getResponseMessage());
    }
}
