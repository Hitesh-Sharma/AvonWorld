package com.avon.india.fragments;
//package src.avonmalaysia.fragments;
//
//import java.util.List;
//
//import src.avonmalaysia.AppPrefrence;
//import src.avonmalaysia.Avon;
//import src.avonmalaysia.R;
//import src.avonmalaysia.adapter.AdapterNotification;
//import src.avonmalaysia.db.DbOperations;
//import src.avonmalaysia.itemproperty.Notifications;
//import src.avonmalaysia.supports.ConstantsReal.ToastMessages;
//import android.annotation.SuppressLint;
//import android.app.NotificationManager;
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.TextView;
//
//import com.swipemenulistview.SwipeMenu;
//import com.swipemenulistview.SwipeMenuCreator;
//import com.swipemenulistview.SwipeMenuItem;
//import com.swipemenulistview.SwipeMenuListView;
//import com.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
//
//public class FragmentNotificationOLD extends Fragment implements
//		OnItemClickListener {
//
//	private SwipeMenuListView listview_notification;
//	private List<Notifications> list_notification;
//	private AdapterNotification adapter;
//	private TextView txt_view_action_bar_text;
//
//	@SuppressLint("InflateParams")
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//
//		View rootView = inflater.inflate(R.layout.fragment_notification, null);
//
//		listview_notification = (SwipeMenuListView) rootView
//				.findViewById(R.id.listview_notification);
//		listview_notification.setOnItemClickListener(this);
//		txt_view_action_bar_text = (TextView) rootView
//				.findViewById(R.id.txt_view_action_bar_text);
//		txt_view_action_bar_text.setText(Avon.getInstance()
//				.getActivityInstance().getResources()
//				.getString(R.string.notification));
//		setAdapter();
//		clearNotificationsFromsTray();
//		return rootView;
//	}
//
//	private void clearNotificationsFromsTray() {
//		String ns = Context.NOTIFICATION_SERVICE;
//		NotificationManager nMgr = (NotificationManager) Avon.getInstance()
//				.getActivityInstance().getSystemService(ns);
//		List<Integer> list_ids = AppPrefrence.getInstance(getActivity())
//				.getNotificationIds();
//		if (list_ids != null) {
//			for (int i = 0; i < list_ids.size(); i++) {
//				nMgr.cancel(list_ids.get(i));
//			}
//		}
//		AppPrefrence.getInstance(getActivity()).clearNotificationIds();
//		Log.e("after clear the notifications ids", ""
//				+ AppPrefrence.getInstance(getActivity()).getNotificationIds());
//	}
//
//	private void setAdapter() {
//		list_notification = DbOperations.getNotificationList(getActivity());
//		if (list_notification != null && list_notification.size() > 0) {
//			adapter = new AdapterNotification(getActivity(), list_notification);
//			listview_notification.setAdapter(adapter);
//			setSwipetoListView();
//		} else {
//			Avon.getInstance().getActivityInstance()
//					.showToast(ToastMessages.NO_DATA);
//		}
//	}
//
//	private void setSwipetoListView() {
//		// step 1. create a MenuCreator
//		SwipeMenuCreator creator = new SwipeMenuCreator() {
//
//			@Override
//			public void create(SwipeMenu menu) {
//				// create "delete" item
//				SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
//				// set item background
//				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xED,
//						0x00, 0x8c)));
//				// set item width
//				deleteItem.setWidth(dp2px(90));
//				// set a icon
//				deleteItem.setIcon(R.drawable.delete_icon);
//				// add to menu
//				menu.addMenuItem(deleteItem);
//			}
//		};
//		// set creator
//		listview_notification.setMenuCreator(creator);
//
//		// step 2. listener item click event
//		listview_notification
//				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//					@Override
//					public boolean onMenuItemClick(int position,
//							SwipeMenu menu, int index) {
//						switch (index) {
//						case 0:
//							deleteNotificationFromList(position);
//							break;
//						}
//						return false;
//					}
//				});
//	}
//
//	private int dp2px(int dp) {
//		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
//				getResources().getDisplayMetrics());
//	}
//
//	private void deleteNotificationFromList(int position) {
//		DbOperations.deleteNotification(getActivity(),
//				list_notification.get(position));
//		list_notification.remove(position);
//		if (adapter != null)
//			adapter.notifyDataSetChanged();
//		Avon.getInstance().getActivityInstance()
//				.showToast(ToastMessages.NOTIFICATION_DELETED);
//	}
//
//	public void loadNotification() {
//		setAdapter();
//		if (adapter != null)
//			adapter.notifyDataSetChanged();
//	}
//
//	@Override
//	public void onItemClick(AdapterView<?> view, View arg1, int position,
//			long id) {
//
//	}
//
// }
