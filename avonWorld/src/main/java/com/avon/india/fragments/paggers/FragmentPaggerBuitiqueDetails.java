package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdpaterPaggerBuitiques;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.itemproperty.LocationItem;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.LocatorNativeMap;
import com.avon.india.supports.SupportUtils;

import java.util.List;

@SuppressLint("InflateParams")
public class FragmentPaggerBuitiqueDetails extends BaseFragmentBackHandler
        implements OnPageChangeListener, OnClickListener {

    private ImageView img_view_phone;
    private TextView txt_view_action_bar_text;
    private TextView txt_view_aciton_bar_left;
    private TextView txt_view_guide_me;
    private GoogleMap googleMap;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private float zoom = 10;
    private CustomViewPager custom_pager;
    private AdpaterPaggerBuitiques adapter;
    private List<LocationItem> locations;
    private LinearLayout linear_left_view, linear_right_view;
    private int current_position = 0;

    public void setCurrentPosition(int current_position,
                                   List<LocationItem> location_list) {
        this.current_position = current_position;
        this.locations = location_list;

    }

    @Override
    public boolean onBackPressed() {
        try {
            SupportMapFragment f = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.buitique_location));
            FragmentTransaction ft = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ft.remove(f).commit();
            super.onDestroyView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onBackPressed();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_buitiques_details,
                null);
        img_view_phone = (ImageView) rootView.findViewById(R.id.img_view_phone);
        img_view_phone.setOnClickListener(this);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_aciton_bar_left = (TextView) rootView
                .findViewById(R.id.txt_view_aciton_bar_left);
        txt_view_guide_me = (TextView) rootView
                .findViewById(R.id.txt_view_guide_me);
        txt_view_guide_me.setOnClickListener(this);
        custom_pager = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager);

        linear_left_view = (LinearLayout) rootView
                .findViewById(R.id.linear_left_view);
        linear_left_view.setOnClickListener(this);

        linear_right_view = (LinearLayout) rootView
                .findViewById(R.id.linear_right_view);

        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.boutique_details));
        try {
            initilizeMap();
            googleMapFeatures();
        } catch (Exception e) {
            e.printStackTrace();
        }
        calculatePositionForAddress();
        setAdapter();
        return rootView;
    }

    private void initilizeMap() {
        if (googleMap == null) {
            AppLogger.e("feature", "in google map feature");
            googleMap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.buitique_location)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            if (googleMap == null) {
                Avon.getInstance().getActivityInstance()
                        .showToast(ToastMessages.UNABLE_TO_CREATE_MAP);
            }
        }
    }

    private void googleMapFeatures() throws Exception {
        latitude = locations.get(current_position).getLatitude();
        longitude = locations.get(current_position).getLongitude();
        Log.e("", "Latitude  Values will be" + latitude
                + "Longitude Values Will be" + longitude);
        if (googleMap != null) {
            googleMap.clear();
            googleMap
                    .addMarker(
                            new MarkerOptions()
                                    .draggable(true)
                                    .position(new LatLng(latitude, longitude))
                                    .title("Boutique location")
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)))
                    .showInfoWindow();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(zoom).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }
    }

    private void rotatePhoneClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.rotate_view_clock_vise);
        img_view_phone.startAnimation(rotate);
    }

    private void rotatePhoneAntiClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.rotate_view_anticlock_vise);
        img_view_phone.startAnimation(rotate);
    }

    private void calculatePositionForAddress() {
        Bitmap bmp = BitmapFactory.decodeResource(Avon.getInstance()
                .getActivityInstance().getResources(), R.drawable.call_icon);
        int margin_top = bmp.getHeight() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) custom_pager
                .getLayoutParams();
        params.setMargins(0, margin_top, 0, 0);
        custom_pager.setLayoutParams(params);
    }

    private void setAdapter() {
        AppLogger.e("", "The items are loaded and set in the adapter");
        adapter = new AdpaterPaggerBuitiques(getChildFragmentManager(),
                locations);
        custom_pager.setAdapter(adapter);
        custom_pager.setOnPageChangeListener(this);
        changePageTo(current_position);
    }

    private void updatePreviousNextIcons(int position) {
        linear_right_view.setVisibility(View.GONE);
        txt_view_aciton_bar_left.setText("Back");
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int changed_position) {
        if (changed_position < current_position) {
            rotatePhoneAntiClockwise();
        } else {
            rotatePhoneClockwise();
        }
        current_position = changed_position;
        try {
            googleMapFeatures();
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatePreviousNextIcons(current_position);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_left_view:
                getActivity().onBackPressed();
                break;
            case R.id.img_view_phone:
                String phoneNumber = locations.get(current_position)
                        .getMobileNumber();
                Log.e("", "Phone number is " + phoneNumber);
                SupportUtils.actionMakeCall(phoneNumber);
            case R.id.txt_view_guide_me:
                LocatorNativeMap map = new LocatorNativeMap(Avon.getInstance()
                        .getActivityInstance());
                try {
                    map.showOnNativeMap(locations.get(current_position)
                            .getLatitude(), locations.get(current_position)
                            .getLongitude());
                    AppLogger.e("Latitude Longitude values", "Latitude is"
                            + locations.get(current_position).getLatitude()
                            + "Longitude is"
                            + locations.get(current_position).getLongitude());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            default:
                break;
        }
    }

    public void changePageTo(int position) {
        custom_pager.setCurrentItem(position);
        updatePreviousNextIcons(position);
        try {
            googleMapFeatures();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
