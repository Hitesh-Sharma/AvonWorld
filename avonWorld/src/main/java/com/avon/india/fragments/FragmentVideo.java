package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterVideo;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.itemproperty.VideoItem;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentVideo extends BaseFragment implements ILoadListner {
    private AdapterVideo _adapter_video_view;
    private List<VideoItem> _video_item;
    private TextView txt_view_action_bar_text;
    private ListView video_list_view;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_video, null);
        video_list_view = (ListView) rootView
                .findViewById(R.id.video_list_view);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.videos));
        _video_item = new ArrayList<VideoItem>();
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            loadVideoList();
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet to Watch Videos");
        }

        video_list_view.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view,
                                    int position, long arg3) {
                // String Url =
                // "http://dsvq2wjt5cqkv.cloudfront.net/aug22v6.mp4";
                playVideo(_video_item.get(position).getVideo_Url());
            }
        });

        return rootView;

    }

    public void loadVideoList() {
        MyLoader loader = new MyLoader(Avon.getInstance().getActivityInstance());
        int country_id = AppPrefrence.getInstance(getActivity())
                .getMyCountryId();
        HashMap<String, String> params = RequestFormater.getVideos(country_id);
        loader.setOnLoadListner(this);
        loader.setRequestType(RequestType.MEDIA);
        loader.startLoading(WebUrls.URL_GET_VIDEOS, params);
        Log.e("", "Url to be forwarded is" + WebUrls.URL_GET_VIDEOS);

    }

    protected void playVideo(String videoUrl) {
        AppLogger.e("videoUrl", "videoUrl " + videoUrl);
        if (videoUrl == null || videoUrl.trim().equalsIgnoreCase("")) {
            Avon.getInstance().getActivityInstance()
                    .showToast(ToastMessages.NO_VIDEO_FILE_RECEIVED);
            return;
        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        Uri data = Uri.parse(videoUrl);
        intent.setDataAndType(data, "video/*");
        startActivity(intent);
    }

    @Override
    public boolean onBackPress() {
        return false;
    }

    private void setAdapter(List<VideoItem> video_List) {
        _adapter_video_view = new AdapterVideo(getActivity(), video_List);

        video_list_view.setAdapter(_adapter_video_view);
        if (_adapter_video_view != null)
            _adapter_video_view.notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null)
            return;

        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            Avon.getInstance().getActivityInstance()
                    .showToast(response.getResponseMessage());
            return;
        } else {
            _video_item = (List<VideoItem>) response.getResponse();
            if (_video_item != null && _video_item.size() > 0) {
                setAdapter(_video_item);
                AppLogger.e("Video title",
                        "Video title" + _video_item.get(0).getName_of_video()
                                + _video_item.size());
            } else {
                Avon.getInstance().getActivityInstance()
                        .showToast(response.getResponseMessage());
            }
        }
    }
}
