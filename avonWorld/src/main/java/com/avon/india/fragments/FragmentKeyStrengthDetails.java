package com.avon.india.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.basehelper.BaseFragment;
import com.avon.india.customviews.TextSwitcherAnimation;
import com.avon.india.customviews.TextSwitcherAnimation.IonAnimationCompleted;
import com.avon.india.itemproperty.KeyStrengthsList;

import java.util.List;
import java.util.Locale;

public class FragmentKeyStrengthDetails extends BaseFragment implements
		IonAnimationCompleted, OnInitListener {
	private int current_position = 0;
	List<KeyStrengthsList> key_strength_list;
	TextSwitcherAnimation txt_view_static_text_question,
			txt_view_static_text_answers;
	private boolean isFirstTime = true;
	private ImageView img_btn_menu_icon;
	private TextToSpeech tts;
	private final int MY_DATA_CHECK_CODE = 200;

	public void setCurrentPosition(int current_position,
			List<KeyStrengthsList> key_strength_list) {
		this.current_position = current_position;
		this.key_strength_list = key_strength_list;

	}

	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_keystrength_details,
				null);

		// tts = new TextToSpeech(Avon.getInstance().getActivityInstance(),
		// this);

		txt_view_static_text_question = (TextSwitcherAnimation) rootView
				.findViewById(R.id.txt_view_static_text_question);

		txt_view_static_text_answers = (TextSwitcherAnimation) rootView
				.findViewById(R.id.txt_view_static_text_answers);
		txt_view_static_text_question.animateText(key_strength_list.get(
				current_position).getQuestion());
		txt_view_static_text_answers = (TextSwitcherAnimation) rootView
				.findViewById(R.id.txt_view_static_text_answers);
		img_btn_menu_icon = (ImageView) rootView
				.findViewById(R.id.img_btn_menu_icon);
		img_btn_menu_icon.setImageResource(R.drawable.back_icon);
		img_btn_menu_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Avon.getInstance().getActivityInstance().onBackPressed();

			}
		});
		txt_view_static_text_question.setCharacterDelay(50);
		txt_view_static_text_answers.setCharacterDelay(50);

		txt_view_static_text_question.setOnAnimationCompleted(this);
		txt_view_static_text_answers.setOnAnimationCompleted(this);

		Intent checkTTSIntent = new Intent();
		checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

		return rootView;
	}

	private void speakTheText(String text) {
		if (tts != null) {
			if (text != null) {
				if (!tts.isSpeaking()) {
					tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
				}
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == MY_DATA_CHECK_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// the user has the necessary data - create the TTS
				tts = new TextToSpeech(getActivity(), this);
			} else {
				// no data - install it now
				Intent installTTSIntent = new Intent();
				installTTSIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installTTSIntent);
			}
		}
	}

	@Override
	public boolean onBackPress() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		return false;
	}

	@Override
	public void onAnimationCompleted(boolean isCompleted) {
		if (isFirstTime) {
			isFirstTime = false;
			txt_view_static_text_answers.animateText(key_strength_list.get(
					current_position).getResponse());
			// speakTheText();
		}
	}

	@Override
	public void onInit(int code) {
		if (code == TextToSpeech.SUCCESS) {
			tts.setLanguage(Locale.getDefault());
			speakTheText(key_strength_list.get(current_position).getQuestion()
					+ key_strength_list.get(current_position).getResponse());
		} else {
			tts = null;
			Toast.makeText(getActivity(), "Failed to initialize TTS engine.",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onDestroy() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

}
