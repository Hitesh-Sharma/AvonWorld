package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterBoutiqueLocator;
import com.avon.india.adapter.AdapterStateFilterList;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.enums.EnumSet.LocationType;
import com.avon.india.fragments.paggers.FragmentPaggerBuitiqueDetails;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.itemproperty.LocationItem;
import com.avon.india.itemproperty.States;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.IConstants;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;
import com.avon.india.supports.SupportUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentBuitiqueLocation extends BaseFragmentBackHandler implements
        OnItemClickListener, OnClickListener, ILoadListner {
    private AdapterBoutiqueLocator _adapter_boutique_location;
    private List<LocationItem> list_buitique_locations = new ArrayList<LocationItem>();
    private ListView list_view_locations;
    private TextView txt_view_action_bar_text, txt_view_search_state;
    private ImageView img_view_down_arrow;
    private ListView list_view_filter_result;
    private AdapterStateFilterList adapter;
    private boolean isDropDownListOpen = false, loadMoreStates = true;
    private LocationType location_type = LocationType.ALL;
    private List<States> list_states;
    private RelativeLayout relative_states_dropdown;
    private int country_id, state_id = -1;
    private double latitude, longitude;

    @Override
    public boolean onBackPressed() {
        boolean isBack = true;
        if (isDropDownListOpen) {
            isDropDownListOpen = false;
            animateDownArrow(isDropDownListOpen);
            isBack = false;
        }
        return isBack;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_boutiques_locator,
                null);
        list_view_locations = (ListView) rootView
                .findViewById(R.id.location_view);
        list_view_locations.setOnItemClickListener(this);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.boutique));
        img_view_down_arrow = (ImageView) rootView
                .findViewById(R.id.img_view_down_arrow);
        txt_view_search_state = (TextView) rootView
                .findViewById(R.id.txt_view_search_state);
        relative_states_dropdown = (RelativeLayout) rootView
                .findViewById(R.id.relative_states_dropdown);
        list_view_filter_result = (ListView) rootView
                .findViewById(R.id.list_view_filter_result);
        list_view_filter_result.setVisibility(View.GONE);

        country_id = AppPrefrence.getInstance(getActivity()).getMyCountryId();

        list_view_filter_result.setOnItemClickListener(this);

        relative_states_dropdown.setOnClickListener(this);
        adjustSearchView();
        loadStatesList();
        getLocation();
        return rootView;
    }

    private void getLocation() {
        if (Avon.getInstance().getFusedLocationManagerInsance() != null) {
            Location myLocation = Avon.getInstance()
                    .getFusedLocationManagerInsance().getLocation();
            if (myLocation != null) {
                latitude = myLocation.getLatitude();
                longitude = myLocation.getLongitude();
                AppLogger.e("", "my location " + myLocation.getLatitude() + ""
                        + myLocation.getLongitude());
            } else {
                AppLogger.e("", "my location null");
            }
        } else {
            AppLogger.e("", "else my location null");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View view, int position,
                            long id) {
        switch (parentView.getId()) {
            case R.id.location_view:
                SupportUtils.hideVirtualKeyboard(getActivity());
                FragmentPaggerBuitiqueDetails fragment = new FragmentPaggerBuitiqueDetails();
                fragment.setCurrentPosition(position, list_buitique_locations);
                Avon.getInstance().getFlowOrganization().add(fragment, true);
                break;
            case R.id.list_view_filter_result:
                if (list_states.get(position).getStateId() == 0) {
                    location_type = LocationType.ALL;
                } else {
                    state_id = list_states.get(position).getStateId();
                    location_type = LocationType.STATE_VISE;
                }
                loadMoreStates = true;
                txt_view_search_state.setText(list_states.get(position)
                        .getStateName());
                list_buitique_locations.clear();
                _adapter_boutique_location = null;
                list_view_locations.setAdapter(null);
                loadButiqueLocation(1);
                isDropDownListOpen = false;
                animateDownArrow(isDropDownListOpen);
                break;

            default:
                break;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_states_dropdown:
                if (isDropDownListOpen) {
                    isDropDownListOpen = false;
                } else {
                    isDropDownListOpen = true;
                }
                animateDownArrow(isDropDownListOpen);
                break;
            default:
                break;
        }
    }

    private void adjustSearchView() {
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (Avon.getInstance().getActivityInstance().getTheme()
                .resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
                    getResources().getDisplayMetrics());
        }
        int extraMargin = (int) Avon.getInstance().getActivityInstance()
                .getResources().getDimension(R.dimen.margin_short);
        actionBarHeight = 8 * extraMargin;
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        params.setMargins(0, actionBarHeight, 0, 0);
        list_view_locations.setLayoutParams(params);
    }

    private void animateDownArrow(boolean isDropDownListOpen) {
        if (isDropDownListOpen) {
            list_view_filter_result.setVisibility(View.VISIBLE);
            rotatePhoneClockwise();
        } else {
            list_view_filter_result.setVisibility(View.GONE);
            rotatePhoneAntiClockwise();
        }
    }

    private void rotatePhoneClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.semi_anti_rotate_anim);
        img_view_down_arrow.startAnimation(rotate);
    }

    private void rotatePhoneAntiClockwise() {
        Animation rotate = AnimationUtils.loadAnimation(Avon.getInstance()
                .getActivityInstance(), R.anim.semi_rotate_anim);
        img_view_down_arrow.startAnimation(rotate);
    }

    public void loadButiqueLocation(int pageNo) {
        AppLogger.e("loadMoreStates", "loadMoreStates " + loadMoreStates);
        if (!loadMoreStates)
            return;
        if (CheckNetworkState.isNetworkAvailable(getActivity())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.BOUTIQUE_LOCATOR);
            HashMap<String, String> params = new HashMap<String, String>();
            switch (location_type) {
                case ALL:
                    params = RequestFormater.getAllBuitiques(
                            list_buitique_locations.size(), country_id, latitude,
                            longitude);
                    break;
                case STATE_VISE:
                    if (state_id < 0) {
                        Avon.getInstance().getActivityInstance()
                                .showToast(ToastMessages.INVALID_STATE_ID);
                        return;
                    }
                    params = RequestFormater.getStatesBuitiques(
                            list_buitique_locations.size(), country_id, state_id);
                    break;
                default:
                    break;
            }
            loader.startLoading(WebUrls.URL_GET_BOUTIQUES, params);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet for Location");
        }
    }

    private void loadStatesList() {
        if (CheckNetworkState.isNetworkAvailable(getActivity())) {
            int country_id = AppPrefrence.getInstance(getActivity())
                    .getMyCountryId();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.STATES_LIST);
            loader.startLoading(WebUrls.URL_GET_STATES, params);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet for Location");
        }
    }

    private void checkForLoadMore() {
        if (list_buitique_locations.size() % IConstants.MAX_COUNT == 0)
            loadMoreStates = true;
        else
            loadMoreStates = false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {
        if (response == null)
            return;

        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            if (response.getRequestedType() == RequestType.BOUTIQUE_LOCATOR) {
                if (list_buitique_locations != null
                        || list_buitique_locations.size() > 0)
                    loadMoreStates = false;
                else
                    Avon.getInstance().getActivityInstance()
                            .showToast(response.getResponseMessage());
            } else
                Avon.getInstance().getActivityInstance()
                        .showToast(response.getResponseMessage());
            return;
        }

        switch (response.getRequestedType()) {
            case BOUTIQUE_LOCATOR:
                if (list_buitique_locations == null
                        || list_buitique_locations.size() == 0) {
                    list_buitique_locations = (List<LocationItem>) response
                            .getResponse();
                } else {
                    list_buitique_locations.addAll((List<LocationItem>) response
                            .getResponse());
                }
                Log.e("", "Now the size of list " + list_buitique_locations.size());
                setLocationAdapter();
                break;

            case STATES_LIST:
                list_states = (List<States>) response.getResponse();
                setStatesList();
                loadButiqueLocation(1);
                break;

            default:
                break;
        }

    }

    @Override
    public void onResume() {
        getLocation();
        super.onResume();
    }

    private void setStatesList() {
        if (list_states != null && list_states.size() > 0) {
            States state = new States();
            state.setStateName(Avon.getInstance().getActivityInstance().getResources().getString(R.string.all));
            list_states.add(0, state);
            adapter = new AdapterStateFilterList(Avon.getInstance()
                    .getActivityInstance(), list_states);
            list_view_filter_result.setAdapter(adapter);
        }
    }

    private void setLocationAdapter() {
        if (_adapter_boutique_location != null)
            _adapter_boutique_location.notifyDataSetChanged();
        else {
            _adapter_boutique_location = new AdapterBoutiqueLocator(this,
                    list_buitique_locations);
            list_view_locations.setAdapter(_adapter_boutique_location);
        }
        if (loadMoreStates)
            checkForLoadMore();
    }

}
