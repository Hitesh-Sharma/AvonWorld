package com.avon.india.fragments.paggers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdpaterPaggerCategories;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BackgroundSaveImageToCache;
import com.avon.india.basehelper.BaseCategoryFragment;
import com.avon.india.customviews.CustomViewPager;
import com.avon.india.db.DbOperations;
import com.avon.india.dialogs.Dialog_Msg;
import com.avon.india.enums.EnumSet.CategoryType;
import com.avon.india.fragments.FragmentHome;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.itemproperty.Category;
import com.avon.india.itemproperty.Products;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.RequestFormater;

import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class FragmentPaggerCategory extends BaseCategoryFragment implements
        OnClickListener, OnPageChangeListener, ILoadListner {

    private CustomViewPager custom_pager;
    private LinearLayout linear_left_view, linear_right_view;
    private ImageButton img_btn_menu_icon;
    private ImageView img_view_aciton_bar_right_arrow;
    private TextView txt_view_aciton_bar_right, txt_view_aciton_bar_left;
    private TextView txt_view_action_bar_text;
    private AdpaterPaggerCategories adapter;
    private int current_position = 0, country_id;
    private CategoryType categoryType = CategoryType.START_SAVING;
    private Dialog_Msg dialog;
    private Products products;
    private List<Category> listCategories;

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dialog = new Dialog_Msg(Avon.getInstance().getActivityInstance());

        View rootView = inflater.inflate(R.layout.fragment_customviewpager,
                null);
        country_id = AppPrefrence.getInstance(getActivity()).getMyCountryId();
        custom_pager = (CustomViewPager) rootView
                .findViewById(R.id.custom_pager);
        linear_left_view = (LinearLayout) rootView
                .findViewById(R.id.linear_left_view);
        linear_left_view.setOnClickListener(this);
        linear_right_view = (LinearLayout) rootView
                .findViewById(R.id.linear_right_view);
        linear_right_view.setOnClickListener(this);
        txt_view_aciton_bar_right = (TextView) rootView
                .findViewById(R.id.txt_view_aciton_bar_right);
        txt_view_aciton_bar_left = (TextView) rootView
                .findViewById(R.id.txt_view_aciton_bar_left);
        txt_view_aciton_bar_right.setVisibility(View.GONE);
        txt_view_aciton_bar_left.setVisibility(View.GONE);
        img_btn_menu_icon = (ImageButton) rootView
                .findViewById(R.id.img_btn_menu_icon);
        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText("");
        img_view_aciton_bar_right_arrow = (ImageView) rootView
                .findViewById(R.id.img_view_aciton_bar_right_arrow);
        callAsync();
        Avon.getInstance()
                .getDrawerInstance(Avon.getInstance().getActivityInstance())
                .getSlider().setSlidingEnabled(true);
//        insertStatusTOLoadProducts();
        return rootView;
    }

    boolean isTOShowNoNEtworkToast = true;

    private void callAsync() {
        try {
//                if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA)) {
            fetchDataFromDBandInsetToList();
            listCategories = Avon.getInstance().getCategoriesList();
            if (listCategories == null || listCategories.size() == 0) {
                isTOShowNoNEtworkToast = true;
            } else {
                insertStatusTOLoadProducts();
                checkProductsLoadStatus();
                updatePreviousNextIcons(0);
                setAdapter();
                isTOShowNoNEtworkToast = false;
                if (Avon.getInstance().isOkDialogShown()) {
                    dialog.show();
                    Avon.getInstance().setIsOkDialogShown(false);
                }
            }
        } catch (Exception e) {
        }

        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            MyLoader loader = null;
            if (isTOShowNoNEtworkToast) {
//                DbOperations.deleteAllCategories(Avon.getInstance().getActivityInstance());
                loader = new MyLoader(Avon.getInstance()
                        .getActivityInstance());
            } else
                loader = new MyLoader(Avon.getInstance()
                        .getActivityInstance(), false);
            HashMap<String, String> categories = RequestFormater
                    .getAllProductDetails(0, country_id, DbOperations.getCountryTimeStamp(Avon.getInstance().getActivityInstance()));
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.PRODUCTS_ALL);
            loader.startLoading(WebUrls.URL_PRODUCTS, categories);
        } else {
            if (isTOShowNoNEtworkToast)
                Avon.getInstance().getActivityInstance().showNoInternetToast(null);
        }
    }

    private void setAdapter() {
        adapter = new AdpaterPaggerCategories(getChildFragmentManager(), this,
                listCategories);
        custom_pager.setAdapter(adapter);
        custom_pager.setOnPageChangeListener(this);
        if (custom_pager.getAdapter().getCount() > 0) {
            custom_pager.setCurrentItem(0);
            txt_view_action_bar_text.setText(listCategories.get(0)
                    .getCategoryName());
        }
    }

    private void updatePreviousNextIcons(int position) {
        try {
            switch (categoryType) {
                case PRODUCTS:
                    if (position == 0) {
                        img_btn_menu_icon.setVisibility(View.VISIBLE);
                        linear_left_view.setVisibility(View.GONE);
                        linear_right_view.setVisibility(View.VISIBLE);
                    } else {
                        img_btn_menu_icon.setVisibility(View.GONE);
                        linear_left_view.setVisibility(View.VISIBLE);
                        linear_right_view.setVisibility(View.VISIBLE);
                    }
                    break;
                case START_SAVING:
                    img_btn_menu_icon.setVisibility(View.GONE);
                    linear_left_view.setVisibility(View.VISIBLE);
                    linear_right_view.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }
            int max_positions = (listCategories.size() * 2);
            if (position % 2 == 1 || position == max_positions) {
                if (position == max_positions) {
                    txt_view_action_bar_text.setText(getResources().getString(
                            R.string.total_saving));
                    linear_left_view.setVisibility(View.VISIBLE);
                    txt_view_aciton_bar_right.setText(getResources().getString(
                            R.string.home));
                    img_view_aciton_bar_right_arrow
                            .setImageResource(R.drawable.home_icon);
                    img_view_aciton_bar_right_arrow.setRotation(0);
                } else {
                    int itemIndex = position / 2;
                    txt_view_action_bar_text.setText(listCategories.get(itemIndex)
                            .getCategoryTotalSavingText());
                    txt_view_aciton_bar_right.setText(getResources().getString(
                            R.string.next));
                    img_view_aciton_bar_right_arrow
                            .setImageResource(R.drawable.back_icon);
                    img_view_aciton_bar_right_arrow.setRotation(180);
                }
                calculateSaving(position);
            } else {
                txt_view_aciton_bar_right.setText(getResources().getString(
                        R.string.next));
                img_view_aciton_bar_right_arrow
                        .setImageResource(R.drawable.back_icon);
                img_view_aciton_bar_right_arrow.setRotation(180);
                txt_view_action_bar_text.setText(listCategories.get(position / 2)
                        .getCategoryName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changePageTo(int position) {
        custom_pager.setCurrentItem(position);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_right_view:
                if (listCategories == null || listCategories.size() == 0)
                    return;
                if (current_position + 1 > 0
                        && current_position + 1 <= custom_pager.getAdapter()
                        .getCount() - 1) {
                    changePageTo(current_position + 1);
                } else {
                    if (Avon.getInstance().getFlowOrganization().hasNoMoreBack()) {
                        Avon.getInstance().getFlowOrganization().clearBackStack();
                        Avon.getInstance().getFlowOrganization()
                                .replace(new FragmentHome());
                    } else
                        Avon.getInstance().getActivityInstance().onBackPressed();
                }
                break;
            case R.id.linear_left_view:
                if (listCategories == null || listCategories.size() == 0) {
                    Avon.getInstance().getActivityInstance().onBackPressed();
                    return;
                }
                if (current_position - 1 >= 0
                        && current_position - 1 < custom_pager.getAdapter()
                        .getCount() - 1) {
                    changePageTo(current_position - 1);
                } else {
                    Avon.getInstance().getActivityInstance().onBackPressed();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onPageSelected(int position) {
        current_position = position;
        updatePreviousNextIcons(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void afterLoadComplete(ResponseParser response) {
        if (response == null)
            return;

        if (response.getResponseCode() == IResponseCode.RESPONSE_ALREADY_THERE) {
            return;
        }
        if (Avon.getInstance().isOkDialogShown()) {
            dialog.show();
            Avon.getInstance().setIsOkDialogShown(false);
        }
        if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
            Avon.getInstance().getActivityInstance()
                    .showToast(response.getResponseMessage());
            return;
        }

        switch (response.getRequestedType()) {
            case PRODUCTS_ALL:
                products = (Products) response.getResponse();
                if (products == null || products.getCategoriesList() == null
                        || products.getCategoriesList().size() == 0) {
                    Avon.getInstance().getActivityInstance()
                            .showToast(response.getResponseMessage());
                    return;
                }
                // saveProductItemsList(products);
                insertStatusTOLoadProducts();
                listCategories = Avon.getInstance().getCategoriesList();
//                new BackgroundSaveImageToCache().startTheProcess();
                checkProductsLoadStatus();
                updatePreviousNextIcons(0);
                break;

            default:
                break;
        }
        setAdapter();
    }
}
