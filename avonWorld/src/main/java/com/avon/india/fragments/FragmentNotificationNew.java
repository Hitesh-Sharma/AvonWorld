package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterNotification;
import com.avon.india.db.DbOperations;
import com.avon.india.itemproperty.Notifications;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import de.timroes.swipetodismiss.SwipeDismissList;
import de.timroes.swipetodismiss.SwipeDismissList.UndoMode;

public class FragmentNotificationNew extends ListFragment implements
        OnItemClickListener {

    private List<Notifications> list_notification;
    private AdapterNotification adapter;
    private TextView txt_view_action_bar_text;
    private SwipeDismissList swipeList;
    private boolean isToShowNoDataToast = true;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_notification, null);

        txt_view_action_bar_text = (TextView) rootView
                .findViewById(R.id.txt_view_action_bar_text);
        txt_view_action_bar_text.setText(Avon.getInstance()
                .getActivityInstance().getResources()
                .getString(R.string.notification));
        clearNotificationsFromsTray();
        if (!CheckNetworkState.isNetworkAvailable(getActivity())) {
            Avon.getInstance().getActivityInstance().showNoInternetToast("Connect to the Internet to Receive Notifications");
            isToShowNoDataToast = false;
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }

    private void clearNotificationsFromsTray() {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) Avon.getInstance()
                .getActivityInstance().getSystemService(ns);
        List<Integer> list_ids = AppPrefrence.getInstance(getActivity())
                .getNotificationIds();
        if (list_ids != null) {
            for (int i = 0; i < list_ids.size(); i++) {
                nMgr.cancel(list_ids.get(i));
            }
        }
        AppPrefrence.getInstance(getActivity()).clearNotificationIds();
        Log.e("after clear the notifications ids", ""
                + AppPrefrence.getInstance(getActivity()).getNotificationIds());
    }

    private void setAdapter() {
        list_notification = DbOperations.getNotificationList(getActivity());
//        list_notification = new ArrayList<Notifications>();
//        Notifications notifications = new Notifications();
//        notifications.setDescription("Hello");
//        notifications.setTime(DateTimeUtils.getDateFormatted("2016-01-25 11:25:60"));
//        notifications.setTitle("Test");
//        list_notification.add(notifications);
        if (list_notification != null && list_notification.size() > 0) {
            adapter = new AdapterNotification(getActivity(), list_notification);
            swipeList = new SwipeDismissList(getListView(), callback,
                    UndoMode.COLLAPSED_UNDO);
            swipeList.setAutoHideDelay(500);
            setListAdapter(adapter);
        } else {
            if (isToShowNoDataToast)
                Avon.getInstance().getActivityInstance()
                        .showToast(ToastMessages.NO_DATA);
        }
    }

    SwipeDismissList.OnDismissCallback callback = new SwipeDismissList.OnDismissCallback() {
        // Gets called whenever the user deletes an item.
        public SwipeDismissList.Undoable onDismiss(AbsListView listView,
                                                   final int position) {
            // Get your item from the adapter (mAdapter being an adapter for
            // MyItem objects)
            final Notifications deletedItem = list_notification.get(position);
            // Delete item from adapter
            list_notification.remove(position);
            adapter.notifyDataSetChanged();
            // Return an Undoable implementing every method
            SwipeDismissList.Undoable swipeDismissList =
                    new SwipeDismissList.Undoable() {

                        // Method is called when user undoes this deletion
                        public void undo() {
                            // Reinsert item to list
                            list_notification.add(position, deletedItem);
                            adapter.notifyDataSetChanged();
                        }

                        // Return an undo message for that item
                        public String getTitle() {
                            return deletedItem.getTitle() + " deleted";
                        }

                        @Override
                        public String getUndoText() {
                            return Avon.getInstance()
                                    .getActivityInstance().getResources()
                                    .getString(R.string.undo);
                        }

                        // Called when user cannot undo the action anymore
                        public void discard() {
                            Avon.getInstance().getActivityInstance()
                                    .showToast(deletedItem.getTitle() + " Deleted.");
                            // list_notification.remove(position);
                            // ListOptions.filterList(list_notification);
                            adapter.notifyDataSetChanged();
                            DbOperations.deleteNotification(Avon.getInstance()
                                    .getActivityInstance(), deletedItem);
                        }
                    };
            return swipeDismissList;
        }
    };

    public void loadNotification() {
        clearNotificationsFromsTray();
        setAdapter();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

    }

    @Override
    public void onDestroy() {
        if (swipeList != null)
            swipeList.discardUndo();
        super.onDestroy();
    }

}
