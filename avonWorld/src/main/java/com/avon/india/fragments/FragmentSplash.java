package com.avon.india.fragments;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.dialogs.DialogFragmentSelectCountry;
import com.avon.india.enums.EnumSet;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.interfaces.IOnCountrySelectionListener;
import com.avon.india.itemproperty.Country;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.CountryNames;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.ConstantsReal.WebUrls;
import com.avon.india.supports.PermissionToAccessLocation;
import com.avon.india.supports.PermissionToMakeCall;
import com.avon.india.supports.SupportUtils;

import java.util.ArrayList;
import java.util.List;

public final class FragmentSplash extends Fragment implements OnClickListener,
        ILoadListner {

    private ImageView img_view_logo;
    private LinearLayout linear_bottom_view;
    private TextView txt_view_select_country;
    TextView txtView_version;
    private boolean isMalaysiaAdded = false;
    private List<Country> listCountry, listDummy, listSubCountry;
    private String east_malaysia, west_malaysia, defaultText;
    private EnumSet.SplashType splashType = EnumSet.SplashType.CHOOSE_LOCATION;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_new_splash, null);
        east_malaysia = getResources().getString(R.string.east_malaysia);
        west_malaysia = getResources().getString(R.string.west_malaysia);
        defaultText = getResources().getString(R.string.select_your_location);
        img_view_logo = (ImageView) rootView.findViewById(R.id.img_view_logo);
        linear_bottom_view = (LinearLayout) rootView
                .findViewById(R.id.linear_bottom_view);
        txtView_version = (TextView) rootView
                .findViewById(R.id.txtView_version);
        txt_view_select_country = (TextView) rootView
                .findViewById(R.id.txt_view_select_country);
        rootView.findViewById(R.id.txt_view_next).setOnClickListener(this);
        rootView.findViewById(R.id.relative_show_location).setOnClickListener(
                this);
        PackageInfo pInfo;

        try {
            pInfo = Avon.getInstance().getActivityInstance().getPackageManager().getPackageInfo(
                    Avon.getInstance().getActivityInstance().getPackageName(), 0);
            String version = pInfo.versionName;
            if (SupportUtils.isValidString(version))
                txtView_version.setText("Version : " + version);
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (splashType) {
            case CHOOSE_LOCATION:
                callAsync();
                break;
            case LOCATION_CHOOSEN:
                rootView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        openHomeScreen();
                    }
                }, 2000);
                break;
        }

        new PermissionToAccessLocation().isToAccessLocation();

        try {
            Avon.getInstance().getDrawerInstance(Avon.getInstance().getActivityInstance()).getSlider().setSlidingEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    private void openHomeScreen() {
        Avon.getInstance().getFlowOrganization()
                .replace(new FragmentHome());
    }

    public void setSplashMode(EnumSet.SplashType splashType) {
        this.splashType = splashType;
    }

    private void callAsync() {
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance(), false);
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.COUNTRY_LIST);
            loader.startLoading(WebUrls.URL_COUNTRIES);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast(null);
        }
    }

    private void startAnimation() {
        linear_bottom_view.setVisibility(View.VISIBLE);
        Animation linearAnim = AnimationUtils.loadAnimation(Avon.getInstance().getActivityInstance(),
                R.anim.slide_view_bottom_up);
        linear_bottom_view.setAnimation(linearAnim);
        Animation imgAnim = AnimationUtils.loadAnimation(Avon.getInstance().getActivityInstance(),
                R.anim.slide_little_bottom_to_top);
        img_view_logo.setAnimation(imgAnim);
    }

    private void showDialog() {
        DialogFragmentSelectCountry dialogFragment = new DialogFragmentSelectCountry();
        dialogFragment
                .setOnItemSelectedListener(new IOnCountrySelectionListener() {

                    @Override
                    public void onCountrySelected(int country_id,
                                                  String country_name, int position) {
                        txt_view_select_country.setText(country_name);
                        AppPrefrence.getInstance(
                                Avon.getInstance().getActivityInstance())
                                .setMyCountryId(country_id);
                        AppPrefrence.getInstance(
                                Avon.getInstance().getActivityInstance())
                                .setMyCountryName(country_name);
                    }
                });
        dialogFragment.setCountryList(listDummy, listSubCountry);
        Avon.getInstance().getFlowOrganization()
                .addWithTopBottomAnimation(dialogFragment, null, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_show_location:
                if (new PermissionToMakeCall().isToMakePhone())
                    showDialog();
                break;
            case R.id.txt_view_next:
                if (txt_view_select_country.getText().toString()
                        .equalsIgnoreCase(defaultText))
                    Avon.getInstance().getActivityInstance()
                            .showToast(ToastMessages.PLEASE_SELECT_COUNTRY_FIRST);
                else {
                    Avon.getInstance().getActivityInstance().checkStatus();
                    openHomeScreen();
                }
            default:
                break;
        }

    }

    @SuppressWarnings("unchecked")
    public void afterLoadComplete(ResponseParser response) {

        if (response == null) {
            Avon.getInstance().getActivityInstance().showToast("");
            return;
        }

        switch (response.getRequestedType()) {
            case COUNTRY_LIST:
                listCountry = (List<Country>) response.getResponse();
                if (listCountry != null && listCountry.size() > 0) {
                    listDummy = new ArrayList<Country>();
                    listSubCountry = new ArrayList<Country>();
                    for (Country county : listCountry) {
                        if (county.getCountryName().contains(east_malaysia)
                                || county.getCountryName().contains(west_malaysia)) {
                            listSubCountry.add(county);
                            if (!isMalaysiaAdded) {
                                county = new Country();
                                county.setCountryId(-1);
                                county.setCountryName(CountryNames.MALAYSIA);
                                listDummy.add(county);
                                isMalaysiaAdded = true;
                            }
                        } else {
                            listDummy.add(county);
                        }
                    }
                    startAnimation();
                }
                break;

            default:
                break;
        }

    }

}
