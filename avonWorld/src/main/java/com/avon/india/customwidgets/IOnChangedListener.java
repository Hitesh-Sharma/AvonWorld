package com.avon.india.customwidgets;

import com.avon.india.customwidgets.RangeSeekBar.SELECTED;

public interface IOnChangedListener {
	public void whichSelected(SELECTED whichSelected);

}
