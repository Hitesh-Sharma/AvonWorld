package com.avon.india.itemproperty;

import java.util.List;

public class OurHistory {

	private String heading;
	private List<String> description;
	private List<Integer> picturesList;
	private List<String> picturesText;
	private int colorBg;

	public OurHistory(String heading, List<String> description,
			List<Integer> picturesList, List<String> picturesText, int colorBg) {
		setHeading(heading);
		setDescription(description);
		setPicturesList(picturesList);
		setPicturesText(picturesText);
		setColorBg(colorBg);
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public List<String> getDescription() {
		return description;
	}

	public void setDescription(List<String> description) {
		this.description = description;
	}

	public List<Integer> getPicturesList() {
		return picturesList;
	}

	public void setPicturesList(List<Integer> picturesList) {
		this.picturesList = picturesList;
	}

	public List<String> getPicturesText() {
		return picturesText;
	}

	public void setPicturesText(List<String> picturesText) {
		this.picturesText = picturesText;
	}

	public int getColorBg() {
		return colorBg;
	}

	public void setColorBg(int colorBg) {
		this.colorBg = colorBg;
	}

}
