package com.avon.india.itemproperty;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class Country {

    private int countryId;
    private String countryName;
    private float maxPercent;
    private float minPercent;
    private boolean isSelected = false;

    public Country() {
    }

    public Country(JSONObject json) {
        if (json == null)
            return;
        try {
            setCountryId(json.getInt(JSONKeys.JSON_KEY_ID));
            setCountryName(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_NAME)));
            setMaxPercent((float) json.getDouble(JSONKeys.JSON_KEY_MAX_PERCENT));
            setMinPercent((float) json.getDouble(JSONKeys.JSON_KEY_MIN_PERCENT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public float getMaxPercent() {
        return maxPercent;
    }

    public void setMaxPercent(float maxPercent) {
        this.maxPercent = maxPercent;
    }

    public float getMinPercent() {
        return minPercent;
    }

    public void setMinPercent(float minPercent) {
        this.minPercent = minPercent;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

}
