package com.avon.india.itemproperty;

import org.json.JSONObject;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import android.os.Parcel;
import android.os.Parcelable;

public class LocationItem implements Parcelable {
    private int item_id;
    private String storeName;
    private String locationAddress;
    private String mobileNumber;
    private Double latitude;
    private Double longitude;
    private String jsonData;
    private String storeImageUrl;
    private String distance;

    public LocationItem() {

    }

    public LocationItem(Parcel in) {
        this.jsonData = in.readString();
        try {
            JSONObject job = new JSONObject(jsonData);
            SetJsonData(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetJsonData(JSONObject json) {
        if (json == null)
            return;
        try {
            setItem_id(json.getInt(JSONKeys.JSON_KEY_ID));
            setStoreName(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_NAME)));
            setLocationAddress(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_ADDRESS)));
            setMobileNumber(json.getString(JSONKeys.JSON_KEY_PHONE));
            setLatitude(json.getDouble(JSONKeys.JSON_KEY_LATITUDE));
            setLongitude(json.getDouble(JSONKeys.JSON_KEY_LONGITUDE));
            setDistance(SupportUtils.getKmFromMeter(json
                    .getDouble(JSONKeys.JSON_KEY_DISTANCE)));
            // setStoreImageUrl(json.getString(JSONKeys.JSON_KEY_STORE_THUMB));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public LocationItem(JSONObject jord) {
        SetJsonData(jord);
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (dest == null)
            return;
        dest.writeString(jsonData);
    }

    public String getStoreImageUrl() {
        return storeImageUrl;
    }

    public void setStoreImageUrl(String storeImageUrl) {
        this.storeImageUrl = storeImageUrl;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

}
