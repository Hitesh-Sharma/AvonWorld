package com.avon.india.itemproperty;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class BaseCategoryItem {

    private int id;
    private String name;
    private String averagePrice;
    private String imageUrl;
    private String fullImageUrl;

    private int itemQuantity = 0;


    BaseCategoryItem() {

    }

    public BaseCategoryItem(JSONObject json) {
        if (json == null)
            return;

        try {
            setId(json.getInt(JSONKeys.JSON_KEY_ID));
            setName(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_NAME)));
            setAveragePrice(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_PRICE)));
            setImageUrl(json.getString(JSONKeys.JSON_KEY_THUMB));
            setFullImageUrl(json.getString(JSONKeys.JSON_KEY_IMAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceAverage() {
        return averagePrice;
    }

    public void setAveragePrice(String price) {
        this.averagePrice = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int item_quantity) {
        this.itemQuantity = item_quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFullImageUrl() {
        return fullImageUrl;
    }

    public void setFullImageUrl(String fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }

}
