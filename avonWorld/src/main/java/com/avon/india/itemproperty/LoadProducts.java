package com.avon.india.itemproperty;

public class LoadProducts {

	private String categoryName;
	private int categoryId;
	private boolean isToLoadMoreProducts = true;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean getIsToLoadMoreProducts() {
		return isToLoadMoreProducts;
	}

	public void setIsToLoadMoreProducts(boolean isToLoadMoreProducts) {
		this.isToLoadMoreProducts = isToLoadMoreProducts;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

}
