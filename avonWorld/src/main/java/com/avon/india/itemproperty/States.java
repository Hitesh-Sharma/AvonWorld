package com.avon.india.itemproperty;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class States {

    private int stateId = 0;
    private String stateName;

    public States() {

    }

    public States(JSONObject json) {
        if (json == null)
            return;
        try {
            setStateId(json.getInt(JSONKeys.JSON_KEY_ID));
            setStateName(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_NAME)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

}
