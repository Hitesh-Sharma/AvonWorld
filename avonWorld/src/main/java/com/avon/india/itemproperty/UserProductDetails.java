package com.avon.india.itemproperty;

import android.os.Parcel;
import android.os.Parcelable;

import com.avon.india.supports.ConstantsReal.JSONKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserProductDetails implements Parcelable {
	private ArrayList<CategoryItem> detailListing;

	public UserProductDetails(JSONObject json) {
		if (json == null)
			return;
		try {
			setDetailListing(json.getJSONArray(JSONKeys.JSON_KEY_PRODUCT_KEY));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

	}

	public ArrayList<CategoryItem> getDetailListing() {
		return detailListing;
	}

	public void setDetailListing(ArrayList<CategoryItem> detailListing) {
		this.detailListing = detailListing;
	}

	public void setDetailListing(JSONArray detailListing) {
		if (detailListing == null || detailListing.length() <= 0)
			return;
		CategoryItem card = null;
		this.detailListing = new ArrayList<CategoryItem>();
		for (int i = 0; i < detailListing.length(); i++) {
			try {
				card = new CategoryItem(detailListing.getJSONObject(i));
				this.detailListing.add(card);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}