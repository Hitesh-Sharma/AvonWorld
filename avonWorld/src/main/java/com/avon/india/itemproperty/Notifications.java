package com.avon.india.itemproperty;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.DateTimeUtils;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class Notifications {
    private String id;
    private String time;
    private String title;
    private String description;

    public Notifications() {
    }

    public Notifications(String pushData) {
        if (!SupportUtils.isValidString(pushData))
            return;

        try {
            JSONObject json = new JSONObject(pushData);
            setTitle(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_TITLE)));
            setDescription(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_MESSAGE)));
            setTime(DateTimeUtils.getDateFormatted(json
                    .getString(JSONKeys.JSON_KEY_DATE)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
