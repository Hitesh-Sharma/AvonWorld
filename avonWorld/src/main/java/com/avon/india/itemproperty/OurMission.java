package com.avon.india.itemproperty;

public class OurMission {

	private String heading;
	private String description;
	private int picture;
	private int colorBg;

	public OurMission(String heading, String description, int picturesList,
			int colorBg) {
		setHeading(heading);
		setDescription(description);
		setPicture(picturesList);
		setColorBg(colorBg);
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getColorBg() {
		return colorBg;
	}

	public void setColorBg(int colorBg) {
		this.colorBg = colorBg;
	}

	public int getPicture() {
		return picture;
	}

	public void setPicture(int picture) {
		this.picture = picture;
	}

}
