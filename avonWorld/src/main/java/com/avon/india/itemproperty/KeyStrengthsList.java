package com.avon.india.itemproperty;

public class KeyStrengthsList {

	private String txt_Headings;
	private String question;
	private String response;
	private int bgColorCode;
	private int keyImages;

	public KeyStrengthsList(String Title, String questions, String answers,
			int bgColorCode, int keyImages) {
		setTxt_Headings(Title);
		setQuestion(questions);
		setResponse(answers);
		setBgColorCode(bgColorCode);
		setKeyImages(keyImages);
	}

	public String getTxt_Headings() {
		return txt_Headings;
	}

	public void setTxt_Headings(String txt_Headings) {
		this.txt_Headings = txt_Headings;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getBgColorCode() {
		return bgColorCode;
	}

	public void setBgColorCode(int bgColorCode) {
		this.bgColorCode = bgColorCode;
	}

	public int getKeyImages() {
		return keyImages;
	}

	public void setKeyImages(int keyImages) {
		this.keyImages = keyImages;
	}

}
