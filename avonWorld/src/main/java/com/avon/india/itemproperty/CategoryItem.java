package com.avon.india.itemproperty;

import org.json.JSONObject;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

public class CategoryItem extends BaseCategoryItem {

    private String desription;
    private int categoyId;
    private String weight;
    private boolean isSelected = false;

    public CategoryItem() {

    }

    public void SetJsonData(JSONObject json) {
        if (json == null)
            return;
        try {
            setDesription(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_DESCRIPTION)));
            setCategoy(json.getInt(JSONKeys.JSON_KEY_CATEGORY));
            setWeight(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_WEIGHT)));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public CategoryItem(JSONObject jord) {
        super(jord);
        SetJsonData(jord);
    }

    public int getCategoy() {
        return categoyId;
    }

    public void setCategoy(int categoy) {
        this.categoyId = categoy;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

}
