package com.avon.india.itemproperty;

import android.os.Parcel;
import android.os.Parcelable;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class VideoItem implements Parcelable {

    private String Video_Url;
    private String time_of_video;
    private String name_of_video;
    private String thumb_image;
    private int item_id;
    private String jsonData;

    public VideoItem(Parcel in) {

        this.jsonData = in.readString();
        try {
            JSONObject job = new JSONObject(jsonData);
            SetJsonData(job);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void SetJsonData(JSONObject json) {

        if (json == null)
            return;
        try {
            setItem_id(json.getInt(JSONKeys.JSON_KEY_ID));
            setName_of_video(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_TITLE)));
            setTime_of_video(json.getString(JSONKeys.JSON_KEY_DURATION));
            setVideo_Url(json.getString(JSONKeys.JSON_KEY_LINK));
            setThumb_image(json.getString(JSONKeys.JSON_KEY_THUMB));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public VideoItem(JSONObject jord) {
        SetJsonData(jord);
    }

    public String getVideo_Url() {
        return Video_Url;
    }

    public void setVideo_Url(String video_Url) {
        Video_Url = video_Url;
    }

    public String getTime_of_video() {
        return time_of_video;
    }

    public void setTime_of_video(String time_of_video) {
        this.time_of_video = time_of_video;
    }

    public String getName_of_video() {
        return name_of_video;
    }

    public void setName_of_video(String name_of_video) {
        this.name_of_video = name_of_video;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (dest == null)
            return;
        dest.writeString(jsonData);
    }

}
