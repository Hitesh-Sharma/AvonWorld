package com.avon.india.itemproperty;

import org.json.JSONObject;

import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import android.os.Parcel;
import android.os.Parcelable;

public class Offers implements Parcelable {

    private int item_id;
    private String item_image;
    private String item_offers_title;
    private String item_offers_description;
    private String Json_data;
    private String startDate;
    private String endDate;

    public Offers(Parcel in) {
        this.Json_data = in.readString();
        try {

            JSONObject job = new JSONObject(Json_data);
            SetJsonData(job);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void SetJsonData(JSONObject json) {

        if (json == null)
            return;
        try {
            setItem_id(json.getInt(JSONKeys.JSON_KEY_ID));
            setItem_image(json.getString(JSONKeys.JSON_KEY_IMAGE));
            setItem_offers_description(SupportUtils.getUtfDecodedString(json
                    .getString(JSONKeys.JSON_KEY_DESCRIPTION)));
            setItem_offers_title(SupportUtils.getUtfDecodedString(json.getString(JSONKeys.JSON_KEY_TITLE)));
            setStartDate(json.getString(JSONKeys.JSON_KEY_START_DATE));
            setEndDate(json.getString(JSONKeys.JSON_KEY_END_DATE));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Offers(JSONObject jord) {
        SetJsonData(jord);
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public String getItem_offers_title() {
        return item_offers_title;
    }

    public void setItem_offers_title(String item_offers_title) {
        this.item_offers_title = item_offers_title;
    }

    public String getItem_offers_description() {
        return item_offers_description;
    }

    public void setItem_offers_description(String item_offers_description) {
        this.item_offers_description = item_offers_description;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (dest == null)
            return;
        dest.writeString(Json_data);
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
