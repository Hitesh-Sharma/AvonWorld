package com.avon.india.itemproperty;

import java.util.List;

public class Products {

	private List<Category> categoriesList;
	private List<CategoryItem> categoryItemsList;
	private List<BaseCategoryItem> baseCategoryItemsList;

	public List<Category> getCategoriesList() {
		return categoriesList;
	}

	public void setCategoriesList(List<Category> categoriesList) {
		this.categoriesList = categoriesList;
	}

	public List<CategoryItem> getCategoryItemsList() {
		return categoryItemsList;
	}

	public void setCategoryItemsList(List<CategoryItem> categoryItemsList) {
		this.categoryItemsList = categoryItemsList;
	}

	public List<BaseCategoryItem> getBaseCategoryItemsList() {
		return baseCategoryItemsList;
	}

	public void setBaseCategoryItemsList(
			List<BaseCategoryItem> baseCategoryItemsList) {
		this.baseCategoryItemsList = baseCategoryItemsList;
	}

}
