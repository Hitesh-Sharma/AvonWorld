package com.avon.india.itemproperty;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.SupportUtils;

import org.json.JSONObject;

public class Category {

    private int categoryId;
    private String categoryName;
    private int categoryCountryID;
    private String categoryTotalSavingText;
    private String selectedCountryName;


    public Category() {

    }

    public Category(JSONObject json) {
        if (json == null)
            return;
        selectedCountryName = AppPrefrence.getInstance(
                Avon.getInstance().getActivityInstance()).getMyCountryName();
        try {
            setCategoryId(json.getInt(JSONKeys.JSON_KEY_ID));
            setCategoryCountryID(json.getInt(JSONKeys.JSON_KEY_COUNTRY));
            // if (selectedCountryName.equalsIgnoreCase(CountryNames.TAIWAN)) {
            setCategoryName(SupportUtils.getUtfDecodedString(json
                    .getString(JSONKeys.JSON_KEY_NAME)));
            setCategoryTotalSavingText(SupportUtils.getUtfDecodedString(json
                    .getString(JSONKeys.JSON_KEY_TOTAL_SAVINGS)));
            // } else {
            // setCategoryName(json.getString(JSONKeys.JSON_KEY_NAME));
            // setCategoryTotalSavingText(json
            // .getString(JSONKeys.JSON_KEY_TOTAL_SAVINGS));
            // }
            setCategoryTotalSavingText(SupportUtils.getUtfDecodedString(json
                    .getString(JSONKeys.JSON_KEY_TOTAL_SAVINGS)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryCountryID() {
        return categoryCountryID;
    }

    public void setCategoryCountryID(int categoryCountryID) {
        this.categoryCountryID = categoryCountryID;
    }

    public String getCategoryTotalSavingText() {
        return categoryTotalSavingText;
    }

    public void setCategoryTotalSavingText(String categoryTotalSavingText) {
        this.categoryTotalSavingText = categoryTotalSavingText;
    }

}
