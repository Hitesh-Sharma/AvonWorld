package com.avon.india.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.customwidgets.IOnChangedListener;
import com.avon.india.customwidgets.RangeSeekBar;
import com.avon.india.customwidgets.RangeSeekBar.SELECTED;
import com.avon.india.enums.EnumSet.CountryReason;
import com.avon.india.interfaces.IOnCountrySelectionListener;
import com.avon.india.itemproperty.Country;
import com.avon.india.supports.ConstantsReal.CountryNames;

import java.util.List;

public class DialogLocationChooser extends Dialog implements OnClickListener,
		IOnChangedListener {

	private TextView txt_view_west_malaysia, txt_view_east_malaysia,
			txt_next_button;
	private String SelectedValue = SELECTED.MINIMUM.name();
	private RangeSeekBar<Integer> chooser;
	private List<Country> listCountry;
	private String east_malaysia, west_malaysia;
	private int country_sub_id = 0;
	private IOnCountrySelectionListener onCountrySelectedListener;
	private int country_id;
	private String country_name;
	private float maxPercent = 0.0f, minPercent = 0.0f;

	private void initControls() {
		txt_view_east_malaysia = (TextView) findViewById(R.id.txt_view_east_malaysia);
		txt_view_west_malaysia = (TextView) findViewById(R.id.txt_view_west_malaysia);
		txt_next_button = (TextView) findViewById(R.id.txt_view_next);
	}

	@SuppressWarnings("unchecked")
	public DialogLocationChooser(Context context, int country_id,
			String country_name, List<Country> listCountry) {
		super(context, R.style.RealCustomDialog);
		this.listCountry = listCountry;
		this.country_id = country_id;
		this.country_name = country_name;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_location_chooser_new);
		initControls();
		chooser = (RangeSeekBar<Integer>) findViewById(R.id.seekbar);

		chooser.setRangeValues(20, 80);
		chooser.setOnChangedListener(DialogLocationChooser.this);
		txt_next_button.setOnClickListener(this);
		if (SelectedValue == SELECTED.MINIMUM.name()) {
			setWestMalaysia();
		}
		chooser.setSelectedMinValue(20);
		chooser.setSelectedMaxValue(80);
		east_malaysia = CountryNames.EAST_MALAYSIA;
		west_malaysia = CountryNames.WEST_MALAYSIA;
		setWestMalaysia();
		// isAlreadySelectedCountry(listCountry);
	}

	// private void isAlreadySelectedCountry(List<Country> listCountries) {
	// int selectedId = AppPrefrence.getInstance(
	// Avon.getInstance().getActivityInstance()).getMyCountryId();
	// for (int i = 0; i < listCountries.size(); i++) {
	// String countryName = listCountries.get(i).getCountryName();
	// int id = listCountries.get(i).getCountryId();
	// if (id == selectedId) {
	// if (countryName.contains(east_malaysia)) {
	// setEastMalaysia();
	// break;
	// } else if (countryName.contains(west_malaysia)) {
	// setWestMalaysia();
	// break;
	// }
	// }
	// }
	// }

	public void setOnItemSelectedListener(
			IOnCountrySelectionListener onCountrySelectedListener) {
		this.onCountrySelectedListener = onCountrySelectedListener;
	}

	@Override
	public void onClick(View view) {

		switch (view.getId()) {

		case R.id.txt_view_next:

			this.dismiss();
			AppPrefrence.getInstance(Avon.getInstance().getActivityInstance())
					.setCalculationMaxPercent(maxPercent);
			AppPrefrence.getInstance(Avon.getInstance().getActivityInstance())
					.setCalculationMinPercent(minPercent);
			if (onCountrySelectedListener != null)
				onCountrySelectedListener.onCountrySelected(country_sub_id,
						CountryNames.MALAYSIA, 0);

			break;

		default:
			break;
		}
	}

	private void setWestMalaysia() {
		setSelectedCountry(west_malaysia);
		txt_view_west_malaysia.setTextColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_pink));
		Avon.getInstance().setCoutryReason(CountryReason.WEST);
		txt_view_east_malaysia.setTextColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_black));
	}

	private void setEastMalaysia() {
		setSelectedCountry(east_malaysia);
		txt_view_east_malaysia.setTextColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_pink));
		Avon.getInstance().setCoutryReason(CountryReason.EAST);
		txt_view_west_malaysia.setTextColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_black));
	}

	@Override
	public void whichSelected(SELECTED whichSelected) {
		Log.e("Selected Location is", "Which option is selected "
				+ whichSelected);

		if (whichSelected == SELECTED.MINIMUM) {
			SelectedValue = SELECTED.MINIMUM.name();
			setWestMalaysia();
		} else if (whichSelected == SELECTED.MAXIMUM) {
			SelectedValue = SELECTED.MAXIMUM.name();
			setEastMalaysia();
		}
	}

	private void setSelectedCountry(String country_name) {
		for (Country county : listCountry) {
			if (county.getCountryName().equalsIgnoreCase(country_name)) {
				country_sub_id = county.getCountryId();
				maxPercent = county.getMaxPercent();
				minPercent = county.getMinPercent();
				break;
			}
		}
	}

}
