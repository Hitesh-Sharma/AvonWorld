package com.avon.india.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterSelectCountry;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.interfaces.IOnCountrySelectionListener;
import com.avon.india.itemproperty.Country;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.CountryNames;
import com.avon.india.supports.ConstantsReal.WebUrls;

import java.util.ArrayList;
import java.util.List;

public class DialogFragmentSelectCountry extends BaseFragmentBackHandler
        implements OnItemClickListener, ILoadListner {

    private ListView list_view_quantity;
    // private RelativeLayout relative_list_view;
    private AdapterSelectCountry adapter;
    private IOnCountrySelectionListener onCountrySelectedListener;
    private boolean isReturn = false, isMalaysiaAdded = false;
    private List<Country> listCountry, listDummy, listSubCountry;
    private String east_malaysia, west_malaysia;
    private View rootView;
    private LinearLayout linear_bottom_view;

    @Override
    public boolean onBackPressed() {
        rootView.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources().getColor(
                R.color.color_transparent));
        return super.onBackPressed();
    }

    public void setCountryList(List<Country> listDummy,
                               List<Country> listSubCountry) {
        this.listDummy = listDummy;
        this.listSubCountry = listSubCountry;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_spinner_two, null);
        // Avon.getInstance().getDrawerInstance(Avon.getInstance().getActivityInstance()).getSlider()
        // .setSlidingEnabled(false);
        list_view_quantity = (ListView) rootView
                .findViewById(R.id.list_view_quantity);
        linear_bottom_view = (LinearLayout) rootView.findViewById(R.id.linear_bottom_view);

        if (listDummy != null) {
            adapter = new AdapterSelectCountry(Avon.getInstance().getActivityInstance(), listDummy);
            if (adapter != null) {
                list_view_quantity.setAdapter(adapter);
            }
            isAlreadySelectedCountry();
        } else {
            callAsync();
        }
        east_malaysia = CountryNames.EAST_MALAYSIA;
        west_malaysia = CountryNames.WEST_MALAYSIA;
        // relative_list_view = (RelativeLayout) rootView
        // .findViewById(R.id.relative_list_view);
        // adjestListViewHeight();
        list_view_quantity.setOnItemClickListener(this);
        rootView.postDelayed(new Runnable() {
            @Override
            public void run() {
                rootView.setBackgroundColor(Avon.getInstance().getActivityInstance().getResources()
                        .getColor(R.color.color_black_transparent));
            }
        }, 500);


        // callAsync();

        return rootView;
    }

    // private void setCustomList() {
    // listCountry = new ArrayList<Country>();
    // Country cntry = new Country();
    // cntry.setCountryName("India");
    // listCountry.add(listCountry.size(), cntry);
    // cntry = new Country();
    // cntry.setCountryName("Malaysia");
    // listCountry.add(listCountry.size(), cntry);
    // cntry = new Country();
    // cntry.setCountryName("China");
    // listCountry.add(listCountry.size(), cntry);
    // cntry = new Country();
    // cntry.setCountryName("Indonesia");
    // listCountry.add(listCountry.size(), cntry);
    // adapter = new AdapterSelectCountry(Avon.getInstance().getActivityInstance(), listCountry);
    // list_view_quantity.setAdapter(adapter);
    // }

    private void callAsync() {
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.COUNTRY_LIST);
            loader.startLoading(WebUrls.URL_COUNTRIES);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast(null);
        }
    }

    // private void adjestListViewHeight() {
    // int screenHeight = SupportUtils.getScreenHeight(Avon.getInstance().getActivityInstance());
    // int margin = (int) (getResources().getDimension(R.dimen.margin_meium)) *
    // 2;
    // RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
    // ViewGroup.LayoutParams.MATCH_PARENT, screenHeight / 2);
    // params.setMargins(margin, 0, margin, 0);
    // params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    // list_view_quantity.setLayoutParams(params);
    // }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        updateSelectedView(position);
        if (listDummy.get(position).getCountryId() < 0) {
            DialogLocationChooser dialog = new DialogLocationChooser(Avon
                    .getInstance().getActivityInstance(), listDummy.get(
                    position).getCountryId(), listDummy.get(position)
                    .getCountryName(), listSubCountry);
            if (onCountrySelectedListener != null)
                dialog.setOnItemSelectedListener(onCountrySelectedListener);
            dialog.show();
        } else {
            if (onCountrySelectedListener != null) {
                onCountrySelectedListener.onCountrySelected(
                        listDummy.get(position).getCountryId(),
                        listDummy.get(position).getCountryName(), position);
                AppPrefrence.getInstance(
                        Avon.getInstance().getActivityInstance())
                        .setCalculationMaxPercent(
                                listDummy.get(position).getMaxPercent());
                AppPrefrence.getInstance(
                        Avon.getInstance().getActivityInstance())
                        .setCalculationMinPercent(
                                listDummy.get(position).getMinPercent());
            }
        }
        AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).setUserRegistationStatus(false);
        isReturn = true;
        Avon.getInstance().getActivityInstance().onBackPressed();
    }

    private void updateSelectedView(int position) {
        for (int i = 0; i < listDummy.size(); i++) {
            if (i == position)
                listDummy.get(i).setIsSelected(true);
            else
                listDummy.get(i).setIsSelected(false);
        }
//        AppLogger.e("DialogFragmentSelectCountry", "selected position is " + position);
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public IOnCountrySelectionListener getOnItemSelectedListener() {
        return onCountrySelectedListener;
    }

    public void setOnItemSelectedListener(
            IOnCountrySelectionListener onCountrySelectedListener) {
        this.onCountrySelectedListener = onCountrySelectedListener;
    }

    private void isAlreadySelectedCountry() {
        rootView.post(new Runnable() {
            @Override
            public void run() {
                int selectedId = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance())
                        .getMyCountryId();
                String selectedCountry = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
                if (selectedCountry == null)
                    return;
                for (int i = 0; i < listDummy.size(); i++) {
                    if (selectedCountry.equalsIgnoreCase(listDummy.get(i).getCountryName())) {
                        updateSelectedView(i);
//                list_view_quantity.setItemChecked(i, true);
                        break;
                    } else if (selectedId == listDummy.get(i).getCountryId()) {
                        updateSelectedView(i);
//                list_view_quantity.setItemChecked(i, true);
                        break;
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null) {
            Avon.getInstance().getActivityInstance().showToast("");
            return;
        }

        try {
            switch (response.getRequestedType()) {
                case COUNTRY_LIST:
                    listCountry = (List<Country>) response.getResponse();
                    if (listCountry != null && listCountry.size() > 0) {
                        listDummy = new ArrayList<Country>();
                        listSubCountry = new ArrayList<Country>();
                        for (Country county : listCountry) {
                            if (county.getCountryName().contains(east_malaysia)
                                    || county.getCountryName().contains(west_malaysia)) {
                                listSubCountry.add(county);
                                if (!isMalaysiaAdded) {
                                    county = new Country();
                                    county.setCountryId(-1);
                                    county.setCountryName(CountryNames.MALAYSIA);
                                    listDummy.add(county);
                                    isMalaysiaAdded = true;
                                }
                            } else {
                                listDummy.add(county);
                            }
                        }
                        isAlreadySelectedCountry();
                        adapter = new AdapterSelectCountry(Avon.getInstance().getActivityInstance(), listDummy);
                        if (adapter != null)
                            list_view_quantity.setAdapter(adapter);
                    }
                    break;

                default:
                    break;
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroyView();
    }
}
