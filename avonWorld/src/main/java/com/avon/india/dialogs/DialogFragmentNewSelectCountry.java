package com.avon.india.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterSelectCountry;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.fragments.FragmentHome;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.interfaces.IOnCountrySelectionListener;
import com.avon.india.itemproperty.Country;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal;
import com.avon.india.supports.ConstantsReal.CountryNames;
import com.avon.india.supports.ConstantsReal.WebUrls;

import java.util.ArrayList;
import java.util.List;


public class DialogFragmentNewSelectCountry extends BaseFragmentBackHandler
        implements ILoadListner, OnClickListener {

    // private RelativeLayout relative_list_view;
    private AdapterSelectCountry adapter;
    private IOnCountrySelectionListener onCountrySelectedListener;
    private boolean isReturn = false, isMalaysiaAdded = false;
    private List<Country> listCountry, listDummy, listSubCountry;
    private String east_malaysia, west_malaysia, defaultText;
    private View rootView;
    private LinearLayout linear_bottom_view;
    private TextView txt_view_select_country;
    private boolean isCountriesAvailable = false;

    @Override
    public boolean onBackPressed() {
        Avon.getInstance().getDrawerInstance(getActivity()).setSlidingEnabled(isReturn);
        return isReturn;
    }


    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_new_location_selection, null);
        // Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
        // .setSlidingEnabled(false);
        linear_bottom_view = (LinearLayout) rootView.findViewById(R.id.linear_bottom_view);
        txt_view_select_country = (TextView) rootView.findViewById(R.id.txt_view_select_country);
        defaultText = getResources().getString(R.string.select_your_location);
        east_malaysia = CountryNames.EAST_MALAYSIA;
        west_malaysia = CountryNames.WEST_MALAYSIA;
        // relative_list_view = (RelativeLayout) rootView

        Avon.getInstance().getDrawerInstance(getActivity()).setSlidingEnabled(false);


        rootView.findViewById(R.id.relative_show_location).setOnClickListener(this);
        rootView.findViewById(R.id.txt_view_next).setOnClickListener(this);

        listCountry = (List<Country>) AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getCountryIds();
        if (listCountry != null && listCountry.size() > 0) {
            isCountriesAvailable = true;
            if (listCountry != null && listCountry.size() > 0) {
                rootView.post(new Runnable() {
                    @Override
                    public void run() {
                        initListOfCountries();
                    }
                });
            }else{
                callAsync();
            }
        }

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_show_location:
                showDialog();
                break;
            case R.id.txt_view_next:
                if (txt_view_select_country.getText().toString()
                        .equalsIgnoreCase(defaultText))
                    Avon.getInstance().getActivityInstance()
                            .showToast(ConstantsReal.ToastMessages.PLEASE_SELECT_COUNTRY_FIRST);
                else {
                    isReturn = true;
                    try {
                        Avon.getInstance().getActivityInstance().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Avon.getInstance().getActivityInstance().checkStatus();
                                openHomeScreen();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        Avon.getInstance().getActivityInstance().checkStatus();
                        Avon.getInstance().getActivityInstance().onBackPressed();
//                        openHomeScreen();
                    }
                }
            default:
                break;
        }
    }


    private void showDialog() {
        if (!CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            if (!isCountriesAvailable) {
                Avon.getInstance().getActivityInstance().showNoInternetToast(null);
                return;
            }
        }
        DialogFragmentSelectCountry dialogFragment = new DialogFragmentSelectCountry();
        dialogFragment
                .setOnItemSelectedListener(new IOnCountrySelectionListener() {

                    @Override
                    public void onCountrySelected(int country_id,
                                                  String country_name, int position) {
                        txt_view_select_country.setText(country_name);
                        AppPrefrence.getInstance(
                                Avon.getInstance().getActivityInstance())
                                .setMyCountryId(country_id);
                        AppPrefrence.getInstance(
                                Avon.getInstance().getActivityInstance())
                                .setMyCountryName(country_name);
                    }
                });
        dialogFragment.setCountryList(listDummy, listSubCountry);
        Avon.getInstance().getFlowOrganization()
                .addWithTopBottomAnimation(dialogFragment, null, true);
//        Avon.getInstance().getFlowOrganization()
//                .add(dialogFragment, null, true);
    }

    private void openHomeScreen() {
        String country_name = txt_view_select_country.getText()
                .toString();
        Avon.getInstance().getActivityInstance()
                .updateLanguage(country_name);
        Avon.getInstance().getFlowOrganization()
                .replace(new FragmentHome());
        Avon.getInstance().getFlowOrganization().clearBackStack();
    }


    private void callAsync() {
        if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
                .getActivityInstance())) {
            MyLoader loader = new MyLoader(Avon.getInstance()
                    .getActivityInstance());
            loader.setOnLoadListner(this);
            loader.setRequestType(RequestType.COUNTRY_LIST);
            loader.startLoading(WebUrls.URL_COUNTRIES);
        } else {
            Avon.getInstance().getActivityInstance().showNoInternetToast(null);
            listCountry = (List<Country>) AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getCountryIds();
            if (listCountry != null && listCountry.size() > 0) {
                isCountriesAvailable = true;
                if (listCountry != null && listCountry.size() > 0) {
                    rootView.post(new Runnable() {
                        @Override
                        public void run() {
                            initListOfCountries();
                        }
                    });
                }
            }
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void afterLoadComplete(ResponseParser response) {

        if (response == null) {
            Avon.getInstance().getActivityInstance().showToast(ConstantsReal.ToastMessages.NO_MESSAGE_FROM_SERVER);
            return;
        }

        try {
            switch (response.getRequestedType()) {
                case COUNTRY_LIST:
                    listCountry = (List<Country>) response.getResponse();
                    if (listCountry != null && listCountry.size() > 0) {
                        rootView.post(new Runnable() {
                            @Override
                            public void run() {
                                initListOfCountries();
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    private void initListOfCountries() {
        listDummy = new ArrayList<Country>();
        listSubCountry = new ArrayList<Country>();
        for (Country county : listCountry) {
            if (county.getCountryName().contains(east_malaysia)
                    || county.getCountryName().contains(west_malaysia)) {
                listSubCountry.add(county);
                if (!isMalaysiaAdded) {
                    county = new Country();
                    county.setCountryId(-1);
                    county.setCountryName(CountryNames.MALAYSIA);
                    listDummy.add(county);
                    isMalaysiaAdded = true;
                }
            } else {
                listDummy.add(county);
            }
        }
        txt_view_select_country.setText(AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName());
    }

    @Override
    public void onDestroy() {
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroyView();
    }
}
