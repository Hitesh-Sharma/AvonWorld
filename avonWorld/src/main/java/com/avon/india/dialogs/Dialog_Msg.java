package com.avon.india.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.avon.india.R;

public class Dialog_Msg extends Dialog implements
		android.view.View.OnClickListener {
	private TextView btn_ok;

	private void initControls() {
		btn_ok = (TextView) findViewById(R.id.btn_ok);
	}

	public Dialog_Msg(Context context) {
		super(context, R.style.TheCustomDialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_msg_saving);
		initControls();

		btn_ok.setOnClickListener(this);
	}

	protected Dialog_Msg(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	public Dialog_Msg(Context context, int theme) {
		super(context, theme);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_ok:

			this.dismiss();

			break;

		default:
			break;
		}
	}

}
