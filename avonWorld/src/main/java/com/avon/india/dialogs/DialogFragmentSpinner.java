package com.avon.india.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.interfaces.IOnSpinnerItemSelectedListener;
import com.avon.india.supports.SupportUtils;

import java.util.ArrayList;

import antistatic.spinnerwheel.AbstractWheel;
import antistatic.spinnerwheel.OnWheelChangedListener;
import antistatic.spinnerwheel.adapters.StringArrayWheelAdapter;

public class DialogFragmentSpinner extends BaseFragmentBackHandler implements
		OnClickListener {

	private AbstractWheel spinnerList;
	private boolean scrolling = false;
	private TextView txt_select, txt_cancel;
	private TextView _txt;
	private ImageView _img;
	private String imageUrl;
	private RelativeLayout relative_transparent, relative_view;
	private String titile;
	private int screenHeight;

	@Override
	public boolean onBackPressed() {
		relative_transparent.setBackgroundColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_transparent));
		Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
				.setSlidingEnabled(true);
		return true;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
				.setSlidingEnabled(false);
		View rootView = inflater.inflate(R.layout.dialog_spinner, null);
		screenHeight = SupportUtils.getScreenHeight(getActivity());
		spinnerList = (AbstractWheel) rootView
				.findViewById(R.id.spnr_quantity_dialog_spinner);
		spinnerList.setVisibleItems(5);

		_txt = (TextView) rootView.findViewById(R.id.txt_title_dialog_spinner);
		_img = (ImageView) rootView.findViewById(R.id.img_item_dialog_spinner);
		relative_transparent = (RelativeLayout) rootView
				.findViewById(R.id.relative_transparent);
		relative_view = (RelativeLayout) rootView
				.findViewById(R.id.relative_view);
		relative_transparent.setOnClickListener(this);
		txt_select = (TextView) rootView
				.findViewById(R.id.txt_select_dialog_spinner);
		txt_select.setOnClickListener(this);
		txt_cancel = (TextView) rootView
				.findViewById(R.id.txt_cancel_dialog_spinner);

		// Avon.getInstance().getLocalImageLoader().loadImage(imageResId, _img);
		// Avon.getInstance().getImageLoader().startLazyLoading(imageUrl, _img);
		Avon.getInstance().getThumbImageLoader()
				.startLazyLoading(imageUrl, _img);

		_txt.setText(titile);
		txt_cancel.setOnClickListener(this);
		spinnerList.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(AbstractWheel wheel, int oldValue,
					int newValue) {
				if (!scrolling) {
				}
			}
		});
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i <= 12; i++) {
			list.add(Integer.toString(i));
		}
		setAdapter(list);

		if (Avon.getInstance().getActivityInstance() != null) {
			spinnerList.setCurrentItem(0);
		}

		rootView.postDelayed(new Runnable() {

			@Override
			public void run() {
				relative_transparent.setBackgroundColor(Avon.getInstance()
						.getActivityInstance().getResources()
						.getColor(R.color.color_black_dark_transparent));
			}
		}, 700);
		setViewHeight();
		return rootView;
	}

	private void setViewHeight() {
		int layoutWidth = (screenHeight / 2) - (screenHeight / 6);
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, layoutWidth);
		param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relative_view.setLayoutParams(param);

	}

	public void setTitleandMessage(String titile, int imageResId) {
		this.titile = titile;
	}

	public void setTitleandMessage(String titile, String imageUrl) {
		this.titile = titile;
		this.imageUrl = imageUrl;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.txt_select_dialog_spinner) {
			if (onItemSelectedListener != null) {
				int current = spinnerList.getCurrentItem();
				onItemSelectedListener.onItemSelected(current);
			}
		} else if (v.getId() == R.id.txt_cancel_dialog_spinner) {
			if (onItemSelectedListener != null) {
				onItemSelectedListener.onItemSelected(0);
			}
		}
		// dismiss();
		relative_transparent.setBackgroundColor(Avon.getInstance()
				.getActivityInstance().getResources()
				.getColor(R.color.color_transparent));
		Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
				.setSlidingEnabled(true);
		getActivity().onBackPressed();
	}

	public whichButtonClicked getButtonClick() {
		return buttonClick;
	}

	public void setButtonClick(whichButtonClicked buttonClick) {
		this.buttonClick = buttonClick;
	}

	private whichButtonClicked buttonClick;

	interface whichButtonClicked {
		public void whichbuttonClicked(String whichButton);
	}

	private IOnSpinnerItemSelectedListener onItemSelectedListener;
	private StringArrayWheelAdapter<String> adapter;

	public void setAdapter(ArrayList<String> list) {
		adapter = new StringArrayWheelAdapter<String>(Avon.getInstance()
				.getActivityInstance(), R.layout.dialog_spinner_one_text_view, list);
		adapter.setTextSize(18);
		adapter.setTextColor(Avon.getInstance().getActivityInstance()
				.getResources().getColor(R.color.color_pink));
		spinnerList.setViewAdapter(adapter);
	}

	/**
	 * @return the onItemSelectedListener
	 */
	public IOnSpinnerItemSelectedListener getOnItemSelectedListener() {
		return onItemSelectedListener;
	}

	/**
	 * @param onItemSelectedListener
	 *            the onItemSelectedListener to set
	 */
	public void setOnItemSelectedListener(
			IOnSpinnerItemSelectedListener onItemSelectedListener) {
		this.onItemSelectedListener = onItemSelectedListener;
	}

}
