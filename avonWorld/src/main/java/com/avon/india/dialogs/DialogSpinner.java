package com.avon.india.dialogs;
//package src.avonmalaysia.dialogs;
//
//import java.util.ArrayList;
//
//import src.avonmalaysia.Avon;
//import src.avonmalaysia.R;
//import src.avonmalaysia.db.Constants;
//import src.avonmalaysia.interfaces.IOnSpinnerItemSelectedListener;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.view.Gravity;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import antistatic.spinnerwheel.AbstractWheel;
//import antistatic.spinnerwheel.OnWheelChangedListener;
//import antistatic.spinnerwheel.adapters.StringArrayWheelAdapter;
//
//public class DialogSpinner extends Dialog implements
//		android.view.View.OnClickListener {
//
//	// defining a window Decor view
//	private View v = null;
//	private AbstractWheel spinnerList;
//	private boolean scrolling = false;
//	private TextView txt_select, txt_cancel;
//	private TextView _txt;
//	private LinearLayout linear_view;
//	private ImageView _img;
//	private int screen_width;
//
//	/**
//	 * The dialog of exit confirmation
//	 * 
//	 * @author Wild Coder
//	 * @param mContext
//	 *            the object of context of the main class
//	 */
//	public DialogSpinner(Context _activity) {
//		// super(_activiyt, R.style.TheCustomDialog);
//		super(_activity);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		setCancelable(true);
//		screen_width = _activity.getResources().getDisplayMetrics().widthPixels;
//		setCanceledOnTouchOutside(true);
//		setContentView(R.layout.dialog_spinner);
//
//		this.getWindow().getAttributes().windowAnimations = R.style.ListDialogAnimation;
//
//		// linear_view = (LinearLayout) findViewById(R.id.linear_view);
//		// LayoutParams params = new LayoutParams(screen_width+50,
//		// LayoutParams.WRAP_CONTENT);
//		// linear_view.setLayoutParams(params);
//
//		Window window = getWindow();
//		WindowManager.LayoutParams wlp = window.getAttributes();
//
//		wlp.gravity = Gravity.BOTTOM;
//		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//		// window.setAttributes(wlp);
//		// v = window.getDecorView();
//		// v.setBackgroundResource(R.color.color_black_transparent);
//		spinnerList = (AbstractWheel) findViewById(R.id.spnr_quantity_dialog_spinner);
//		spinnerList.setVisibleItems(5);
//
//		_txt = (TextView) findViewById(R.id.txt_title_dialog_spinner);
//		_img = (ImageView) findViewById(R.id.img_item_dialog_spinner);
//		// findViewById(R.id.relative_transparent).setOnClickListener(this);
//		txt_select = (TextView) findViewById(R.id.txt_select_dialog_spinner);
//		txt_select.setOnClickListener(this);
//		txt_cancel = (TextView) findViewById(R.id.txt_cancel_dialog_spinner);
//		txt_cancel.setOnClickListener(this);
//		spinnerList.addChangingListener(new OnWheelChangedListener() {
//			public void onChanged(AbstractWheel wheel, int oldValue,
//					int newValue) {
//				if (!scrolling) {
//				}
//			}
//		});
//		ArrayList<String> list = new ArrayList<String>();
//		for (int i = 0; i <= 12; i++) {
//			list.add(Integer.toString(i));
//		}
//		setAdapter(list);
//	}
//
//	public void showDialog(String title, int imageResId) {
//		Avon.getInstance().getLocalImageLoader().loadImage(imageResId, _img);
//		_txt.setText(title);
//		show();
//	}
//
//	@Override
//	public void show() {
//		if (Avon.getInstance().getActivityInstance() != null) {
//			spinnerList.setCurrentItem(0);
//			super.show();
//		}
//	}
//
//	private IOnSpinnerItemSelectedListener onItemSelectedListener;
//	private StringArrayWheelAdapter<String> adapter;
//
//	public void setAdapter(ArrayList<String> list) {
//		adapter = new StringArrayWheelAdapter<String>(getContext(),
//				R.layout.row_text_view, list);
//		adapter.setTextSize(18);
//		adapter.setTextColor(getContext().getResources().getColor(
//				R.color.color_pink));
//		spinnerList.setViewAdapter(adapter);
//	}
//
//	/**
//	 * @return the onItemSelectedListener
//	 */
//	public IOnSpinnerItemSelectedListener getOnItemSelectedListener() {
//		return onItemSelectedListener;
//	}
//
//	/**
//	 * @param onItemSelectedListener
//	 *            the onItemSelectedListener to set
//	 */
//	public void setOnItemSelectedListener(
//			IOnSpinnerItemSelectedListener onItemSelectedListener) {
//		this.onItemSelectedListener = onItemSelectedListener;
//	}
//
//	@Override
//	public void onClick(View v) {
//		if (v.getId() == R.id.txt_select_dialog_spinner) {
//			if (onItemSelectedListener != null) {
//				int current = spinnerList.getCurrentItem();
//				onItemSelectedListener.onItemSelected(current);
//
//			}
//		} else if (v.getId() == R.id.txt_cancel_dialog_spinner) {
//			if (onItemSelectedListener != null) {
//				int current = spinnerList.getCurrentItem();
//				onItemSelectedListener.onItemSelected(current);
//
//			}
//		}
//		dismiss();
//	}
//
//	public whichButtonClicked getButtonClick() {
//		return buttonClick;
//	}
//
//	public void setButtonClick(whichButtonClicked buttonClick) {
//		this.buttonClick = buttonClick;
//	}
//
//	private whichButtonClicked buttonClick;
//
//	interface whichButtonClicked {
//		public void whichbuttonClicked(String whichButton);
//	}
// }