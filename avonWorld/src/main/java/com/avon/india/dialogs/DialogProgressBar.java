package com.avon.india.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

import com.avon.india.Avon;
import com.avon.india.R;

public class DialogProgressBar extends Dialog {
	private View v = null;

	/**
	 * @author ESEC-0054 Preeti
	 * 
	 *         Jul 24, 2015
	 */
	/**
	 * The dialog of exit confirmation
	 * 
	 */
	public DialogProgressBar(Activity _activiyt) {
		super(_activiyt, R.style.MyTheme);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCancelable(true);
		setContentView(R.layout.dialog_progress);
		v = getWindow().getDecorView();
		v.setBackgroundResource(android.R.color.transparent);
	}

	public DialogProgressBar(Activity _activiyt, String text) {
		super(_activiyt, R.style.MyTheme);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCancelable(false);
		setContentView(R.layout.dialog_progress);
		v = getWindow().getDecorView();
		v.setBackgroundResource(android.R.color.transparent);
	}

	public DialogProgressBar(Context context) {
		super(context, R.style.MyTheme);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCancelable(false);
		setContentView(R.layout.dialog_progress);
		v = getWindow().getDecorView();
		v.setBackgroundResource(android.R.color.transparent);
	}

	@Override
	public void show() {
		if (Avon.getInstance().getActivityInstance() != null)
			try {
				super.show();
			} catch (Exception e) {
			}
	}
}