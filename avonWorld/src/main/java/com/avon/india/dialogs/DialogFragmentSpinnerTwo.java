package com.avon.india.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.avon.india.Avon;
import com.avon.india.R;
import com.avon.india.adapter.AdapterSpinnerTwo;
import com.avon.india.basehelper.BaseFragmentBackHandler;
import com.avon.india.interfaces.IOnSpinnerItemSelectedListener;
import com.avon.india.supports.SupportUtils;

public class DialogFragmentSpinnerTwo extends BaseFragmentBackHandler implements
		OnItemClickListener {

	private ListView list_view_quantity;
	// private RelativeLayout relative_list_view;
	private AdapterSpinnerTwo adapter;
	private final int QUANTITY_SIZE = 13;
	private IOnSpinnerItemSelectedListener onItemSelectedListener;

	@Override
	public boolean onBackPressed() {
		Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
				.setSlidingEnabled(true);
		return true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.dialog_spinner_two, null);
		Avon.getInstance().getDrawerInstance(getActivity()).getSlider()
				.setSlidingEnabled(false);
		list_view_quantity = (ListView) rootView
				.findViewById(R.id.list_view_quantity);
		// relative_list_view = (RelativeLayout) rootView
		// .findViewById(R.id.relative_list_view);
		rootView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				getActivity().onBackPressed();
			}
		});
		rootView.setBackgroundColor(getResources().getColor(
				R.color.color_black_dark_transparent));

		adjestListViewHeight();
		list_view_quantity.setOnItemClickListener(this);

		adapter = new AdapterSpinnerTwo(getActivity(), QUANTITY_SIZE);
		list_view_quantity.setAdapter(adapter);

		return rootView;
	}

	private void adjestListViewHeight() {
		int screenHeight = SupportUtils.getScreenHeight(getActivity());
		int margin = (int) (getResources().getDimension(R.dimen.margin_meium)) * 2;
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, screenHeight / 2);
//		params.setMargins(margin, 0, margin, 0);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		list_view_quantity.setLayoutParams(params);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (onItemSelectedListener != null) {
			onItemSelectedListener.onItemSelected(position);
		}
		getActivity().onBackPressed();
	}

	/**
	 * @return the onItemSelectedListener
	 */
	public IOnSpinnerItemSelectedListener getOnItemSelectedListener() {
		return onItemSelectedListener;
	}

	/**
	 * @param onItemSelectedListener
	 *            the onItemSelectedListener to set
	 */
	public void setOnItemSelectedListener(
			IOnSpinnerItemSelectedListener onItemSelectedListener) {
		this.onItemSelectedListener = onItemSelectedListener;
	}

}
