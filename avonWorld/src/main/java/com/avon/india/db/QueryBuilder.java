package com.avon.india.db;

import android.database.Cursor;

import com.avon.india.db.Constants.DBConstant;
import com.avon.india.supports.AppLogger;

public class QueryBuilder implements DBConstant {
	/**
	 * @author Wild Coder
	 * @param tableName
	 *            String table name
	 * @param columnToUpdate
	 *            String column to update
	 * @param valueToUpdate
	 *            Value to update
	 * @param whereKey
	 *            where key
	 */
	public static String createQueryToUpdate(String tableName,
			String columnToUpdate, String valueToUpdate, String whereKey,
			String where) {
		StringBuffer buider = new StringBuffer();
		buider.append(QUERY_UPDATE);
		buider.append(tableName);
		buider.append(ACTION_SET);
		buider.append(columnToUpdate);
		buider.append(VALUE_BLOCK_OPEN);
		buider.append(valueToUpdate);
		buider.append(VALUE_BLOCK_CLOSE);
		// Where to update key and value in table this must be a unique key
		buider.append(CLAUSE_WHERE);
		buider.append(whereKey);
		buider.append(VALUE_BLOCK_OPEN);
		buider.append(where);
		buider.append(VALUE_BLOCK_CLOSE);

		AppLogger.e("", "Query:" + buider.toString());

		return buider.toString();
	}

	/**
	 * @author Wild Coder
	 * @param tableName
	 *            String table name
	 * @param whereKey
	 *            String where key to update
	 * @param whereValue
	 *            Where value to update
	 */
	public static String createQueryToSelect(String tableName, String whereKey,
			String whereValue) {
		StringBuffer buider = new StringBuffer();
		buider.append(QUERY_SELECT_ALL_FROM);
		buider.append(tableName);
		buider.append(CLAUSE_WHERE);
		buider.append(whereKey);
		buider.append(VALUE_BLOCK_OPEN);
		buider.append(whereValue);
		buider.append(VALUE_BLOCK_CLOSE);
		return buider.toString();
	}

	/**
	 * @author Wild Coder
	 * @param tableName
	 *            String table name
	 * @param whereKey
	 *            String where key to update
	 * @param whereValue
	 *            Where value to update
	 */
	public static String createQueryToProject(String tableName,
			String whereKey, String whereValue) {
		StringBuffer buider = new StringBuffer();
		buider.append(QUERY_SELECT_FROM);
		buider.append(tableName);
		buider.append(CLAUSE_WHERE);
		buider.append(whereKey);
		buider.append(VALUE_BLOCK_OPEN);
		buider.append(whereValue);
		buider.append(VALUE_BLOCK_CLOSE);
		return buider.toString();
	}

	/**
	 * @author Wild Coder
	 * @param tableName
	 *            String table name
	 * @param whereKey
	 *            String where to update the column name
	 * @param whereValue
	 *            String value to update
	 */
	public static String createQueryToDelete(String tableName, String whereKey,
			String whereValue) {
		StringBuffer buider = new StringBuffer();
		buider.append(QUERY_DELETE_FROM);
		buider.append(tableName);
		buider.append(CLAUSE_WHERE);
		buider.append(whereKey);
		buider.append(VALUE_BLOCK_OPEN);
		buider.append(whereValue);
		buider.append(VALUE_BLOCK_CLOSE);
		return buider.toString();
	}

	/**
	 * @author Wild Coder
	 * @param columnName
	 *            String column
	 * @param cursor
	 *            Cursor
	 */
	public static String getValue(String columnName, Cursor cursor) {
		try {
			int index = cursor.getColumnIndex(columnName);
			String value = cursor.getString(index);
			return value;
		} catch (Exception e) {
			AppLogger.e(QueryBuilder.class.getName(),
					"Exception-" + e.getMessage());
		}
		return "";
	}

	/**
	 * @author Wild Coder
	 * @param columnName
	 *            String column
	 * @param cursor
	 *            Cursor
	 */
	public static int getIntValue(String columnName, Cursor cursor) {
		try {
			int index = cursor.getColumnIndex(columnName);
			int value = cursor.getInt(index);
			return value;
		} catch (Exception e) {
			AppLogger.e(QueryBuilder.class.getName(),
					"Exception-" + e.getMessage());
		}
		return -1;
	}

}
