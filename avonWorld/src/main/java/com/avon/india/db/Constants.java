package com.avon.india.db;

import com.avon.india.Avon;
import com.avon.india.R;


public class Constants {

    public static String LIST[] = {Cateogy.MAKE_UP, Cateogy.SKIN_CARE,
            Cateogy.FRAGRANE, Cateogy.PERSONAL_CARE, Cateogy.HAIR_CARE};
    public static String CATEGORY_LIST[] = Avon.getInstance()
            .getActivityInstance().getResources()
            .getStringArray(R.array.categories_list);
    public static String SAVING_LIST[] = Avon.getInstance()
            .getActivityInstance().getResources()
            .getStringArray(R.array.savings_list);

    public interface Cateogy {
        // String ALL = "all";
        String FRAGRANE = "Fragrance";
        String HAIR_CARE = "Hair Care";
        String MAKE_UP = "Make Up";
        String PERSONAL_CARE = "Personal Care";
        String SKIN_CARE = "Skin Care";
    }

    public interface ErrorMessages {
        String NO_NOTIFICATION = "No Notification";
        String NO_INTERNET_CONNECTION = "No Internet Connection";
        String TAP_RETRY = "Tap to Retry";
        String DO_NOT_HAVE_ANY_OFFER = "We don't have any offer today.";
        String CONNECTION_FAILED = "Connection fialed please retry";
        String EXIT_MESSAGE = "Press again to exit.";

        String INSERT_EMAI_ID = "Insert an email id";
        String INSERT_FEEDBACK = "Insert  a feedback";
        String INSERT_CONTACT_DETAIL = "Insert contact detail";
        String INSERT_PROPER_EMAIL_ID = "Insert a valid email id";
        String INSERT_PROPER_CONTACT_DETAIL = "Insert a valid contact detail";
        String INSERT_PROPER_FEEDBACK = "Insert a valid feedback";
    }

    // public interface Titles {
    // String ABOUT_AVON = "About Avon";
    // String CURRENT_OFFERS = "Current Offers";
    // String FEEDBACK = "Feedback";
    // String HOME = "Home";
    // String PRODUCT_DETAIL = "The Beauty Store";
    // String SAVINGS = "Savings";
    // String TOTAL_SAVING = "Total Savings";
    // String NOTIFICATION = "Notifications";
    // }

    public interface Extras {
        String EXTRA_CATEGORY = "category";
        String EXTRA_CATEGORY_ID = "category_id";
        String EXTRA_INDEX = "index";
        String EXTRA_RESOURCE_ID = "resourceId";
        String ITEM_QUANTITY = "SharedItemQuantity";

    }

    // Prefrence key set

    public interface PrefrenceKey {
        String PREF_IS_FIRST_TIME = "fristTime";

        String PREF_IS_GCM_REGISTER = "isGcmRegister";
    }

    public interface UrlConstants {

        String URL_PREDEFINE_SERVER = "http://50.16.185.3/";
        String URL_PREDEFINE_DOMAIN = "http://avonindiaapp.com/";

        String MAIN_DOMAIN = URL_PREDEFINE_DOMAIN;

        String URL_FOLDER = "index.php/webservice/";

        String URL_SUBMISSION = MAIN_DOMAIN + URL_FOLDER + "getResponse";
        String URL_GET_OFFERS = MAIN_DOMAIN + URL_FOLDER + "offers?";

    }

    public interface Type {
        String ADD_PUSH_REGISTRATION = "gcm_register";
        String ADD_FEEDBACK = "feedback";
    }

    public interface AvonAction {
        String ACTION_PUSH = "push_action";
    }

    public interface DBConstant {

        String COLUMN_ID = "id";
        String COLUMN_NAME = "name";
        String COLUMN_COUNTRY = "country";
        String COLUMN_TOTAL_SAVINGS = "total_savings";
        String COLUMN_DESCRIPTION = "description";
        String COLUMN_WEIGHT = "weight";
        String COLUMN_IMAGE = "image";
        String COLUMN_CATEGORY = "category";
        String COLUMN_THUMB = "thumb";
        String COLUMN_TIME_STAMP = "timeStamp";
        String COLUMN_PRICE = "price";
        String COLUMN_NOTIFICATION_TIME = "time";
        String COLUMN_NOTIFICATION_TITLE = "title";
        String COLUMN_NOTIFICATION_MESSAGE = "message";

        // Column Name User Tables

        String QUERY_UPDATE = "UPDATE ";
        String QUERY_SELECT_ALL_FROM = "select * from ";
        String QUERY_SELECT_FROM = "select from ";
        String QUERY_DELETE_FROM = "delete from ";
        String QUERY_CREATE = "CREATE TABLE ";

        String ACTION_SET = " SET  ";

        String VALUE_BLOCK_OPEN = " = '";
        String VALUE_BLOCK_CLOSE = "' ";
        String EQUALS = " = ";

        String CLAUSE_WHERE = " where ";

        String DB_NAME = "UpdatedDB.sqlite";

        // Table Names
        String TABLE_NOTIFICATION = "notification";
        String TABLE_CATEGORIES = "categories";
        String TABLE_PRODUCTS = "products";
        String TABLE_TIMESTAMP = "countryTimeStamp";

    }

}
