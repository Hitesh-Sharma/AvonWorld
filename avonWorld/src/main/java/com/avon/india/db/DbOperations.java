package com.avon.india.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.db.Constants.DBConstant;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.Category;
import com.avon.india.itemproperty.CategoryItem;
import com.avon.india.itemproperty.Notifications;
import com.avon.india.itemproperty.Products;
import com.avon.india.supports.AppLogger;
import com.avon.india.supports.ConstantsReal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbOperations implements DBConstant {


    public static List<Notifications> getNotificationList(Context c) {
        List<Notifications> listApps = new ArrayList<Notifications>();
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_NOTIFICATION,
                    null);

            if (cur.getCount() > 0) {
                cur.moveToFirst();
                for (int i = 0; i < cur.getCount(); i++) {
                    Notifications content = new Notifications();
                    content.setId(cur.getString(0));
                    content.setTitle(cur.getString(1));
                    content.setDescription(cur.getString(2));
                    content.setTime(cur.getString(3));
                    // SepratedDate sepreateDate;
                    //
                    // if (cur.getString(3) != null) {
                    // sepreateDate = TimeCalculator.getDateAndTime(cur
                    // .getString(3));
                    // } else {
                    // sepreateDate = TimeCalculator.getDateAndTime(Long
                    // .toString(System.currentTimeMillis()));
                    // }
                    // content.setDate(sepreateDate.getDate());
                    // content.setTime(sepreateDate.getTime());
                    listApps.add(content);
                    cur.moveToNext();
                }
            } else {

            }
            cur.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
        if (listApps != null) {
            Collections.reverse(listApps);
            // ListOptions.filterList(listApps);
        }
        return listApps;
    }

    /**
     * @author Wild Coder
     * @param context
     *            Context
     *
     */
    // static class ChatSortByTime implements Comparator<CategoryItem> {
    // public int compare(CategoryItem s1, CategoryItem s2) {
    // try {
    // return s1.getId() - s2.getId();
    // } catch (Exception e) {
    // return 0;
    // }
    // }
    // }

    /**
     * @param c            Context
     * @param notification NotificatioItem to add to teable
     * @author Wild Coder
     */

    public static void setNotificationList(Context c, Notifications notification) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            ContentValues values = new ContentValues();
            Log.e("", "random string " + System.currentTimeMillis());
            values.put(COLUMN_ID, System.currentTimeMillis() + "");
            values.put(COLUMN_NOTIFICATION_TITLE, notification.getTitle());
            values.put(COLUMN_NOTIFICATION_MESSAGE,
                    notification.getDescription());
            // values.put(COLUMN_NOTIFICATION_TIME,
            // Long.toString(System.currentTimeMillis()));
            values.put(COLUMN_NOTIFICATION_TIME, notification.getTime());
            database.insert(TABLE_NOTIFICATION, null, values);
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static void insertTimeStamp(Context c, String timeStamp) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);

        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            String whereArgs = COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId();
            ContentValues values = new ContentValues();
            values.put(COLUMN_TIME_STAMP, timeStamp);
            values.put(COLUMN_COUNTRY, AppPrefrence.getInstance(c).getMyCountryId());
            long returnValue = -1;
            if (getTimeStamp(c)) {
                returnValue = database.update(TABLE_TIMESTAMP, values, whereArgs, null);
                AppLogger.e("update TimeStamp", "" + returnValue);
            } else {
                returnValue = database.insert(TABLE_TIMESTAMP, null, values);
                AppLogger.e("insert TimeStamp", "" + returnValue);
            }
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static boolean getTimeStamp(Context c) {
        boolean isProduct = false;
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_CATEGORIES + " where " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId(),
                    null);

            if (cur.getCount() > 0) {
                isProduct = true;
            }
            cur.close();
//            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            DatabaseConfiguration.closedatabase();
        }
        return isProduct;
    }

    public static String getCountryTimeStamp(Context c) {
        String timeStamp = "";
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_TIMESTAMP + " where " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId(),
                    null);

            if (cur.getCount() > 0) {
                cur.moveToFirst();
                timeStamp = cur.getString(1);
            }
            cur.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
        return timeStamp;
    }

    public static void insertCategory(Context c, Products products) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);

        if (products == null)
            return;
        List<Category> categories = products.getCategoriesList();
        // String sql = null;
        if (categories == null || categories.size() == 0)
            return;
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            for (Category category : categories) {
                String whereArgs = COLUMN_ID + " = " + category.getCategoryId() + " AND " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId();
                ContentValues values = new ContentValues();
                values.put(COLUMN_ID, category.getCategoryId());
                values.put(COLUMN_NAME, category.getCategoryName());
                values.put(COLUMN_COUNTRY, category.getCategoryCountryID());
                values.put(COLUMN_TOTAL_SAVINGS,
                        category.getCategoryTotalSavingText());
                long returnValue = -1;
                if (getCategoy(c, category.getCategoryId())) {
                    returnValue = database.update(TABLE_CATEGORIES, values, whereArgs, null);
                    AppLogger.e("update Category", "" + returnValue);
                } else {
                    returnValue = database.insert(TABLE_CATEGORIES, null, values);
                    AppLogger.e("insert Category", "" + returnValue);
                }
            }
            database.close();
            insertProducts(c, products);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static boolean getCategoy(Context c, int categoryID) {
        boolean isProduct = false;
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_CATEGORIES + " where " + COLUMN_ID + " = " + categoryID + " AND " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId(),
                    null);

            if (cur.getCount() > 0) {
                isProduct = true;
            }
            cur.close();
//            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            DatabaseConfiguration.closedatabase();
        }
        return isProduct;
    }

    public static List<Category> getCategoriesList(Context c) {
        List<Category> listApps = new ArrayList<Category>();
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            String query = QUERY_SELECT_ALL_FROM + TABLE_CATEGORIES + " where " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId() + " order by " + COLUMN_ID + " DESC";
            AppLogger.e("query", " " + query);
            cur = database.rawQuery(query,
                    null);

            if (cur.getCount() > 0) {
                cur.moveToFirst();
                for (int i = 0; i < cur.getCount(); i++) {
                    Category content = new Category();
                    content.setCategoryId(cur.getInt(0));
                    content.setCategoryName(cur.getString(1));
                    content.setCategoryCountryID(cur.getInt(2));
                    content.setCategoryTotalSavingText(cur.getString(3));
                    listApps.add(content);
                    cur.moveToNext();
                }
            }
            cur.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
        if (listApps != null) {
            Collections.reverse(listApps);
            // ListOptions.filterList(listApps);
        }
        return listApps;
    }

    public static void deleteAllCategories(Context c) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        SQLiteDatabase database = null;
        try {
            String sql = "delete from " + TABLE_CATEGORIES + " where " + COLUMN_COUNTRY + " " + AppPrefrence.getInstance(c).getMyCountryId();
            Log.e("", "Query-" + sql);
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            database.execSQL(sql);
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static void insertProducts(Context c, List<CategoryItem> productItems) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        if (productItems == null || productItems.size() == 0)
            return;
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            for (CategoryItem product : productItems) {
                String whereArgs = COLUMN_CATEGORY + " = " + product.getCategoy() + " && " + COLUMN_ID + " = " + product.getId() + " && " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId();
                ContentValues values = new ContentValues();
                values.put(COLUMN_ID, product.getId());
                values.put(COLUMN_NAME, product.getName());
                values.put(COLUMN_DESCRIPTION, product.getDesription());
                values.put(COLUMN_WEIGHT,
                        product.getWeight());
                String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
//                if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
                    values.put(COLUMN_IMAGE, product.getFullImageUrl());
//                } else values.put(COLUMN_IMAGE, product.getImageUrl());
                values.put(COLUMN_IMAGE, product.getFullImageUrl());
                values.put(COLUMN_CATEGORY, product.getCategoy());
                values.put(COLUMN_PRICE,
                        product.getPriceAverage());
                values.put(COLUMN_COUNTRY, AppPrefrence.getInstance(c).getMyCountryId());
                long returnValue = -1;
                if (getProduct(c, product.getCategoy(), product.getId())) {
                    returnValue = database.update(TABLE_PRODUCTS, values, whereArgs, null);
                    AppLogger.e("update ", "" + returnValue);
                } else {
                    returnValue = database.insert(TABLE_PRODUCTS, null, values);
                    AppLogger.e("insert ", "" + returnValue);
                }
            }
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }


    public static void insertProducts(Context c, Products products) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        if (products == null)
            return;
        List<CategoryItem> productItems = products.getCategoryItemsList();
        // String sql = null;
        if (productItems == null || productItems.size() == 0)
            return;
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            for (CategoryItem product : productItems) {
                String whereArgs = COLUMN_CATEGORY + " = " + product.getCategoy() + " AND " + COLUMN_ID + " = " + product.getId() + " AND " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId();
//                String[] whereAttbs = {product.getCategoy() + "", product.getId() + "", AppPrefrence.getInstance(c).getMyCountryId() + ""};
                ContentValues values = new ContentValues();
                values.put(COLUMN_ID, product.getId());
                values.put(COLUMN_NAME, product.getName());
                values.put(COLUMN_DESCRIPTION, product.getDesription());
                values.put(COLUMN_WEIGHT,
                        product.getWeight());
                String country_name = AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName();
//                if (country_name.equalsIgnoreCase(ConstantsReal.CountryNames.PHILIPINNES) || country_name.equalsIgnoreCase(ConstantsReal.CountryNames.MALAYSIA)) {
                    values.put(COLUMN_IMAGE, product.getFullImageUrl());
                    AppLogger.e("product.getName() && product.getFullImageUrl() ", product.getName() + " & " + product.getFullImageUrl());
//                } else {
//                    values.put(COLUMN_IMAGE, product.getImageUrl());
//                    AppLogger.e("product.getName() && product.getImageUrl() ", product.getName() + " & " + product.getImageUrl());
//                }
                values.put(COLUMN_CATEGORY, product.getCategoy());
                values.put(COLUMN_PRICE,
                        product.getPriceAverage());
                values.put(COLUMN_COUNTRY, AppPrefrence.getInstance(c).getMyCountryId());
                long returnValue = -1;
                if (getProduct(c, product.getCategoy(), product.getId())) {
                    returnValue = database.update(TABLE_PRODUCTS, values, whereArgs, null);
                    AppLogger.e("update ", "" + returnValue);
                } else {
                    returnValue = database.insert(TABLE_PRODUCTS, null, values);
                    AppLogger.e("insert ", "" + returnValue);
                }
            }
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static void deleteAllProducts(Context c) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        SQLiteDatabase database = null;
        try {

            String sql = "delete from " + TABLE_PRODUCTS + " where " + COLUMN_COUNTRY + " " + AppPrefrence.getInstance(c).getMyCountryId();
            Log.e("", "Query-" + sql);
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            database.execSQL(sql);
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static boolean getProduct(Context c, int categoryID, int productId) {
        boolean isProduct = false;
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_PRODUCTS + " where " + COLUMN_CATEGORY + " = " + categoryID + " AND " + COLUMN_ID + " = " + productId + " AND " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId(),
                    null);

            if (cur.getCount() > 0) {
                isProduct = true;
            }
            cur.close();
//            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            DatabaseConfiguration.closedatabase();
        }
        return isProduct;
    }

    public static List<CategoryItem> getProductsListList(Context c, int categoryID) {
        List<CategoryItem> listApps = new ArrayList<CategoryItem>();
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_PRODUCTS + " where " + COLUMN_CATEGORY + " = " + categoryID + " AND " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId() + " order by " + COLUMN_ID + " DESC",
                    null);

            if (cur.getCount() > 0) {
                cur.moveToFirst();
                for (int i = 0; i < cur.getCount(); i++) {
                    CategoryItem content = new CategoryItem();
                    content.setId(cur.getInt(0));
                    content.setName(cur.getString(1));
                    content.setDesription(cur.getString(2));
                    content.setWeight(cur.getString(3));
                    content.setImageUrl(cur.getString(4));
                    content.setCategoy(cur.getInt(5));
                    content.setAveragePrice(cur.getString(7));
                    listApps.add(content);
                    cur.moveToNext();
                }
            }
            cur.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
        if (listApps != null) {
            Collections.reverse(listApps);
            // ListOptions.filterList(listApps);
        }
        return listApps;
    }

    public static List<CategoryItem> getProductsListList(Context c) {
        List<CategoryItem> listApps = new ArrayList<CategoryItem>();
        Cursor cur = null;
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        SQLiteDatabase database = null;
        try {
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            // All installed Application
            cur = database.rawQuery(QUERY_SELECT_ALL_FROM + TABLE_PRODUCTS + " where " + COLUMN_COUNTRY + " = " + AppPrefrence.getInstance(c).getMyCountryId() + " order by " + COLUMN_CATEGORY + " DESC , " + COLUMN_ID + " DESC",
                    null);

            if (cur.getCount() > 0) {
                cur.moveToFirst();
                for (int i = 0; i < cur.getCount(); i++) {
                    CategoryItem content = new CategoryItem();
                    content.setId(cur.getInt(0));
                    content.setName(cur.getString(1));
                    content.setDesription(cur.getString(2));
                    content.setWeight(cur.getString(3));
                    content.setImageUrl(cur.getString(4));
                    content.setCategoy(cur.getInt(5));
                    content.setAveragePrice(cur.getString(7));
                    listApps.add(content);
                    cur.moveToNext();
                }
            }
            cur.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
        if (listApps != null) {
            Collections.reverse(listApps);
            // ListOptions.filterList(listApps);
        }
        return listApps;
    }

    /**
     * @param c            Context
     * @param notification NotificatioItem to delete from teable
     * @author Wild Coder
     */

    public static void deleteNotification(Context c, Notifications notification) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        SQLiteDatabase database = null;
        try {

            String sql = QueryBuilder.createQueryToDelete(TABLE_NOTIFICATION,
                    COLUMN_ID, notification.getId());
            Log.e("", "Query-" + sql);
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            database.execSQL(sql);
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }

    public static void deleteAllNotification(Context c) {
        DatabaseConfiguration dbhelperShopCart = Avon.getInstance()
                .getDatabaseInstance(c);
        // String sql = null;
        SQLiteDatabase database = null;
        try {

            String sql = "delete from " + TABLE_NOTIFICATION;
            Log.e("", "Query-" + sql);
            dbhelperShopCart.createDataBase();
            database = DatabaseConfiguration.openDataBase();
            database.execSQL(sql);
            database.close();
            // ReportDBOperation.insertReport(c, _AppName, _PackageName,
            // "NULL");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseConfiguration.closedatabase();
        }
    }
}
