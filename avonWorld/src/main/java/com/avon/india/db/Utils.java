package com.avon.india.db;

import android.content.Context;

import com.avon.india.supports.AppLogger;

import java.util.regex.Pattern;

public class Utils {

	private static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(

	"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
			+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
			+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
			+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
			+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$");

	public static boolean isEmailValid(String emailIserted) {
		return EMAIL_ADDRESS_PATTERN.matcher(emailIserted).matches();
	}

	public static String toStartWithUper(String str) {
		char[] chars = str.toCharArray();
		if (chars != null && chars.length > 0) {
			chars[0] = Character.toUpperCase(chars[0]);
			return new String(chars);
		}
		return "";
	}

	/**
	 * Returns Identifier of String into it's ID as defined in R.java file.
	 * 
	 * @param pContext
	 * @param pString
	 *            defnied in Strings.xml resource name e.g: action_item_help
	 * @return
	 */
	public static int getResourceId(Context pContext, String pString) {
		try {
			return pContext.getResources().getIdentifier(pString, "drawable",
					pContext.getPackageName());
		} catch (Exception e) {
			AppLogger.e("", pString + " not found");
			e.printStackTrace();
			return -1;
		}

	}

	public static int getResourseId(Context pContext, String pVariableName,
			String pResourcename) {
		try {
			return pContext.getResources().getIdentifier(pVariableName,
					pResourcename, pContext.getPackageName());
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
}
