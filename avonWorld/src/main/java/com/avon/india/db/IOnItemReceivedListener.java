package com.avon.india.db;

import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;

import java.util.List;

public interface IOnItemReceivedListener {

	public void onItemReceived(List<CategoryItem> items);
}
