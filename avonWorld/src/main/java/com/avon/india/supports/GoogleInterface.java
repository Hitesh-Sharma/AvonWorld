package com.avon.india.supports;

public interface GoogleInterface {
	public void onStart();

	public void onStop();

	public void buildGoogleApiClient();

	public void onResume();

	public void onDestroy();
}
