package com.avon.india.supports;

public class ConstantsReal {

    public interface ToastMessages {
        String NO_MESSAGE_FROM_SERVER = "Some server issue, try again later";
        String NO_INTERNET = "No internet connection";
        String UNDER_CONSTRUCTION = "Under Development";
        String UNABLE_TO_CREATE_MAP = "Sorry! unable to create maps";
        String INVALID_MESSAGE = "Enter valid message";
        String INVALID_NUMBER = "Invalid phone Number";
        String NO_VIDEO_FILE_RECEIVED = "No video link received";
        String NOTIFICATION_DELETED = "Notification deleted";
        String NO_DATA = "No data found";
        String NO_COUNTRY_SELECTED = "No country selected";
        String INVALID_STATE_ID = "Invalid state id selected";
        String PLEASE_SELECT_COUNTRY_FIRST = "Please select country first";
    }

    public interface LanguageUtils {
        String ENGLISH_LANG = "en";
        String MALAY_LANG = "ms";
        String TAIWAN_LANG = "zh";
        String PHILIPINNES_LANG = "ph";
    }

    public interface CountryIds {
        int INDIA = 0;
        int MALAYSIA = 1;
        int TAIWAN = 2;
        int PHILIPINNES = 3;
    }

    public interface CountryNames {
        String INDIA = "India";
        String MALAYSIA = "Malaysia";
        String TAIWAN = "Taiwan";
        String PHILIPINNES = "Philippines";
        String EAST_MALAYSIA = "East Malaysia";
        String WEST_MALAYSIA = "West Malaysia";
    }

    public interface IResponseCode {
        int RESPONSE_SUCCESS = 200;
        int RESPONSE_ALREADY_THERE = 202;
        int RESPONSE_ERROR = 201;
        int RESPONSE_SESSION = 301;
        int RESPONSE_DATABASE_NOT_FOUND = 100;
        int RESPONSE_JSON_ERROR = 401;
        int RESPONSE_JSON_ERROR_NEXT = 403;
    }

    public interface IConstants {
        int MAX_COUNT = 10;
        int MAX_MAX_COUNT = 20;
    }


    public interface WebUrls {
        //        String SERVER_TYPE = "DEV";
        String SERVER_TYPE = "PRO";
//        String SERVER_IP_50 = "http://50.16.185.3/avonV2/api/"; //Dev server

        String SERVER_IP_50 = "https://avonindiaapp.com/avonglobal/api/"; //Pro Server
        String URL_COUNTRIES = SERVER_IP_50 + "getCountries";
        String URL_GCM_REGISTER = SERVER_IP_50 + "registerUser";
        String URL_PRODUCTS = SERVER_IP_50 + "getProducts";
        String URL_GET_PERCENT = SERVER_IP_50 + "getPercent";
        String URL_CURRENT_OFFERS = SERVER_IP_50 + "getOffers";
        String URL_GET_VIDEOS = SERVER_IP_50 + "getVideos";
        String URL_SEND_FEEDBACK = SERVER_IP_50 + "insertFeedback";
        String URL_GET_ALL_PRODUCTS = SERVER_IP_50 + "getAllProducts";
        String URL_GET_STATES = SERVER_IP_50 + "getStates";
        String URL_GET_BOUTIQUES = SERVER_IP_50 + "getBoutiques";
    }

    public interface FontFiles {
        String HELVETICA_NEUE_BOLD = "fonts/" + "HELVETICANEUEBOLD.TTF";
        String HELVETICA_NEUE_LIGHT = "fonts/" + "HELVETICANEUELIGHT.TTF";
        String TIMES_BOLD_ITALIC = "fonts/" + "TIMESBI.TTF";
        String CHL_AVON = "fonts/" + "CHL_AVON.ttf";
        String HELVETICA_NEUE_BOLD_ITALIC = "fonts/"
                + "HELVETICANEUE-BOLDITALIC.OTF";
        String TIMES_ITALIC = "fonts/" + "TIMESI.TTF";
    }

    public interface StringConstants {
        String EMAIL_INVALID = "Enter valid Email Address";
        String PHONENUMBER_INVALID = "Enter valid Phone Number";
        String EXITMESSAGE = "Press back twice to exit";
        String KEY_CONTENT_OUR_HISTORY = "FragmentOurHistoryDescription:Content";
        String KEY_CONTENT_OUR_MISSION = "FragmentOurMissionDescription:Content";
        String CALLING_PARSE = "tel:";
    }

    public interface PreferenceKeys {
        String PREF_MAIN = "AvonPreference";
        String PREF_NOTIFY_ID = "notify_id";
        String PREF_NOTIFICATIONS_IDS = "notifications_ids";
        String PREF_COUNTRY_IDS = "country_ids";
        String PREF_COUNTRY_ID = "country_id";
        String PREF_COUNTRY_NAME = "country_name";
        String PREF_LOCALE_LANGUAGE = "local_language";
        String PREF_TIMESTAMP = "timeStamp";
        String PREF_IS_COUNTRY_SELECTED = "is_country_selected";
        String PREF_IS_USER_REGISTERD = "is_user_registered";
        String PREF_IS_COUNRY_HAVE_BUITIQUES = "is_country_has_buitiques";
        String PREF_IS_COUNRY_HAVE_BUEATY_STORE = "is_country_has_bueaty_store";
        String PREF_IS_ABOUT_US_THIWAN = "is_about_us_thiwan";
        String PREF_MAX_PERCENTAGE = "max_percent";
        String PREF_MIN_PERCENTAGE = "min_percent";
        String PREF_SELECTED_MODEL_ID = "selected_model";
    }

    public interface AppPermissions {
        int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
        int MY_PERMISSIONS_REQUEST_CAMERA = 2;
        int MY_PERMISSIONS_REQUEST_C2D_MESSAGE = 3;
        int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 4;
        int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5;
        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 6;
        int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 7;
        int MY_PERMISSIONS_REQUEST_CALL_PHONE = 8;
        int MY_PERMISSIONS_ALL = 9;
    }

    public interface JSONKeys {
        boolean _DEBUG = true;

        String JSON_KEY_CURRENT_DATE = "currentDate";
        String JSON_OBJECT_RESPONSE = "response";
        String JSON_ARRAY_OFFERS = "offers";
        String JSON_DATE_CREATED = "date_created";

        String JSON_KEY_CODE = "response_code";
        String JSON_KEY_CODE_MESSAGE = "response_message";
        String JSON_KEY_RESPONSE = "response";
        String JSON_KEY_FEEDBACK = "feedback";
        String JSON_KEY_EMAIL = "email";
        String JSON_KEY_CONTACT = "contact";
        String JSON_KEY_PHONE = "phone";

        String JSON_KEY_ID = "id";
        String JSON_KEY_DESCRIPTION = "description";
        String JSON_KEY_WEIGHT = "weight";
        String JSON_KEY_START_DATE = "start_date";
        String JSON_KEY_END_DATE = "end_date";

        String JSON_OBJECT_REQUEST = "request";
        String JSON_KEY_TYPE = "type";
        String JSON_KEY_EAST_MALAYSIA = "east_malaysia";
        String JSON_KEY_WEST_MALAYSIA = "west_malaysia";

        String JSON_OBJECT_REQUEST_INFO = "requestinfo";

        String JSON_KEY_REGISTRATION_ID = "registration_id";
        String JSON_KEY_ACTIVE = "active";
        String JSON_KEY_NAME = "name";
        String JSON_KEY_CATEGORIES = "categories";
        String JSON_KEY_PRODUCTS = "products";

        String JSON_KEY_DATA = "data";
        String JSON_KEY_COUNT = "count";
        String JSON_KEY_PAGE = "page";
        String JSON_KEY_OFFSET = "offset";
        String JSON_KEY_LIMIT = "limit";
        // JSON KEYS FOR PRODUCT DETAILS LIST
        String JSON_KEY_PRODUCT_KEY = "Product List";
        String JSON_KEY_ITEM_Id = "id";
        String JSON_KEY_COUNTRY = "country";
        String JSON_KEY_COUNTRY_ID = "countryId";
        String JSON_KEY_CATEGORY_ID = "category_id";
        String JSON_KEY_TIMESTAMP = "timestamp";
        String JSON_KEY_ITEM_NAME = "item_name";
        String JSON_KEY_IMAGE = "image";
        String JSON_KEY_THUMB = "thumb";
        String JSON_KEY_ORDER = "order";
        String JSON_KEY_LAT = "lat";
        String JSON_KEY_LONG = "long";
        String JSON_KEY_ITEM_DESCRIPTION = "item_description";
        String JSON_KEY_TOTAL_SAVINGS = "total_savings";
        String JSON_KEY_ITEM_MASS = "item_mass";
        String JSON_KEY_CATEGORY = "category";
        String JSON_KEY_ITEM_SUB_CATEGORY = "item_sub_category";
        String JSON_KEY_PRICE = "price";
        String JSON_KEY_ITEM_EST_PRICE = "item_est_price";
        String JSON_KEY_ITEM_WST_PRICE = "item_wst_price";

        // JSON KEYS FOR OFFERS

        String JSON_KEY_OFFERS = "Offer List";
        String JSON_KEY_OFFERS_TITLE = "offer_title";
        String JSON_KEY_OFFER_START_DATE = "offer_start_date";
        String JSON_KEY_OFFER_END_DATE = "offer_end_date";
        String JSON_KEY_OFFER_IMAGE = "offer_image";
        String JSON_KEY_OFFER_DESCRIPTION = "offer_description";

        // JSON KEY FOR NOTIFICATION
        String JSON_KEY_NOTIFICATION_KEY = "Notification List";
        String JSON_KEY_DEVICE_ID = "deviceId";
        String JSON_KEY_DEVICE_TYPE = "device_type";
        String JSON_KEY_GCM_ID = "gcmId";
        String JSON_KEY_MESSAGE = "message";
        String JSON_KEY_TITLE = "title";
        String JSON_KEY_DATE = "date";
        String JSON_PLATFORM = "platform";

        // JSON KEY FOR BOUTIQUE LOCATOR
        String JSON_KEY_LOCATION_KEY = "Location List";
        String JSON_KEY_LOCALITY = "locality";
        String JSON_KEY_STORE_IMAGE = "store_image";
        String JSON_KEY_STORE_THUMB = "store_thumb";
        String JSON_KEY_STORE_STATE = "store_state";
        String JSON_KEY_STATE_ID = "stateId";
        String JSON_KEY_STORE_CITY = "store_city";
        String JSON_KEY_ADDRESS = "address";
        String JSON_KEY_DISTANCE = "distance";
        String JSON_KEY_STORE_EMAIL = "store_email";
        String JSON_KEY_LATITUDE = "latitude";
        String JSON_KEY_LONGITUDE = "longitude";

        // Json key for Video listing
        String JSON_KEY_VIDEO_KEY = "Media List";
        String JSON_KEY_VIDEO_TITLE = "video_title";
        String JSON_KEY_DURATION = "duration";
        String JSON_KEY_VIDEO_THUMB = "video_thumb";
        String JSON_KEY_LINK = "link";

        String JSON_KEY_FEEDBACK_LIST = "Feedback List";
        String JSON_KEY_FEED_NAME = "feed_name";
        String JSON_KEY_FEED_EMAIL = "feed_email";
        String JSON_KEY_FEED_PHONE = "feed_phone";
        String JSON_KEY_FEED_MESSAGE = "feed_message";

        // Json key for State Listings.
        String JSON_KEY_STATES = "states";

        // Json key for Calculate Percentage.
        String JSON_KEY_MAX_PERCENT = "max_percent";
        String JSON_KEY_MIN_PERCENT = "min_percent";

    }

}
