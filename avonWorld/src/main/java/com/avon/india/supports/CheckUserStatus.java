package com.avon.india.supports;

import com.avon.india.AppPrefrence;
import com.avon.india.enums.EnumSet.UserStatus;

import android.content.Context;
import android.util.Log;

public class CheckUserStatus {

    public static UserStatus getUserStatus(Context context) {
        UserStatus userStatus = UserStatus.NEW_USER;
        boolean userRegistered = AppPrefrence.getInstance(context)
                .getUserRegistationStatus();
        int country_id = AppPrefrence.getInstance(context).getMyCountryId();

        Log.e("", " country_id " + country_id + " userRegistered " + userRegistered);

        if (userRegistered && country_id > 0) {
            userStatus = UserStatus.REGISTERED;
        } else if (!userRegistered && country_id > 0) {
            userStatus = UserStatus.NOT_REGISTERED;
        } else {
            userStatus = UserStatus.NEW_USER;
        }
        return userStatus;
    }

}
