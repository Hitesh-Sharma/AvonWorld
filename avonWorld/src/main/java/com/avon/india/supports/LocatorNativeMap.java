package com.avon.india.supports;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class LocatorNativeMap {
	private Activity mActivity;

	/**
	 * @author ESEC-0054 Preeti
	 * 
	 *         Jul 24, 2015
	 */
	/**
	 * 
	 * @param mActivity
	 *            Activity
	 */
	public LocatorNativeMap(Activity mActivity) {
		this.mActivity = mActivity;
	}

	/**
	 * 
	 * @param dest
	 *            LatLng
	 * @param address
	 *            Name of Destination
	 */
	public void showOnNativeMap(LatLng dest, String address) {
		String uri = String.format(Locale.ENGLISH,
				"http://maps.google.com/maps?daddr=%f,%f (%s)", dest.latitude,
				dest.longitude, address);
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		intent.setClassName("com.google.android.apps.maps",
				"com.google.android.maps.MapsActivity");
		mActivity.startActivity(intent);
	}

	/**
	 * 
	 * @param dest
	 *            LatLng
	 * @param address
	 *            Name of Destination
	 */
	public void showOnNativeMap(double lat, double longi) {
		Log.e("latlng", "lat " + lat + " long " + longi);
		/*
		 * String uri = String.format(Locale.ENGLISH,
		 * "http://maps.google.com/maps?f=d&daddr=%f,%f (%s)", lat, longi,
		 * address);
		 */
		String uri = String.format(Locale.ENGLISH,
				"http://maps.google.com/maps?f=d&daddr=%f,%f", lat, longi);
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		intent.setClassName("com.google.android.apps.maps",
				"com.google.android.maps.MapsActivity");
		mActivity.startActivity(intent);
	}

	public class LatLng {
		public String latitude;
		public String longitude;

		/**
		 * 
		 * @param latitude
		 *            String
		 * @param longitude
		 *            String
		 */
		public LatLng(String latitude, String longitude) {
			this.latitude = latitude;
			this.longitude = longitude;
		}

		/**
		 * 
		 * @param latitude
		 *            double
		 * @param longitude
		 *            double
		 */
		public LatLng(double latitude, double longitude) {
			this.latitude = "" + latitude;
			this.longitude = "" + longitude;
		}

		/**
		 * 
		 * @param latitude
		 *            long
		 * @param longitude
		 *            long
		 */
		public LatLng(long latitude, long longitude) {
			this.latitude = "" + latitude;
			this.longitude = "" + longitude;
		}
	}
}
