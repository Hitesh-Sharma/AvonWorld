package com.avon.india.supports;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.avon.india.Avon;
import com.avon.india.Manifest;

public class PermissionToAccessExternalStorage {

    public boolean isToAccessStorage() {
        return requestPhoneStatePermissions();
    }

    private boolean requestPhoneStatePermissions() {
        boolean isPermission = true;
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                int hasWriteContactsPermission = ContextCompat
                        .checkSelfPermission(Avon.getInstance().getActivityInstance(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                    isPermission = false;
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(
                            Avon.getInstance().getActivityInstance(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showMessageOKCancel(
                                "You need to allow access to External Storage",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        ActivityCompat
                                                .requestPermissions(
                                                        Avon.getInstance().getActivityInstance(),
                                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                        ConstantsReal.AppPermissions.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                                    }
                                });
                        return isPermission;
                    } else
                        ActivityCompat.requestPermissions(Avon.getInstance().getActivityInstance(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                ConstantsReal.AppPermissions.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    return isPermission;
                }
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isPermission;
    }

    private void showMessageOKCancel(String message,
                                     DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Avon.getInstance().getActivityInstance())
                .setMessage(message).setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null).create().show();
    }

//	public boolean requestPhoneStatePermissions() {
//		boolean isPermission = true;
//		try {
//			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
//				// Here, thisActivity is the current activity
//				if (ContextCompat.checkSelfPermission(FetchiziApp.getInstanse()
//						.getActivityInstanse(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//					isPermission = false;
//					// Should we show an explanation?
//					if (ActivityCompat.shouldShowRequestPermissionRationale(
//							Avon.getInstance().getActivityInstance(),
//							Manifest.permission.CAMERA)) {
//						ActivityCompat.requestPermissions(FetchiziApp
//								.getInstanse().getActivityInstanse(),
//								new String[] { Manifest.permission.CAMERA },
//								AppPermissions.MY_PERMISSIONS_REQUEST_CAMERA);
//						AppPermissionPrefrence.getInstance()
//								.setAppPermissionToCamera(false);
//					} else {
//						ActivityCompat.requestPermissions(FetchiziApp
//								.getInstanse().getActivityInstanse(),
//								new String[] { Manifest.permission.CAMERA },
//								AppPermissions.MY_PERMISSIONS_REQUEST_CAMERA);
//						if (!AppPermissionPrefrence.getInstance()
//								.getAppPermissionToCamera())
//							showDefaultYesNoDialog();
//					}
//				} else {
//					AppPermissionPrefrence.getInstance()
//							.setAppPermissionToReadContacts(true);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		new PermissionToAccessExternalStorage().isToAccessExternalStorage();
//		return isPermission;
//	}

//	private void showDefaultYesNoDialog() throws Exception {
//		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				switch (which) {
//				case DialogInterface.BUTTON_POSITIVE:
//					// Yes button clicked
//					goToSettings();
//					break;
//
//				case DialogInterface.BUTTON_NEGATIVE:
//					// No button clicked
//					break;
//				}
//			}
//		};
//
//		AlertDialog.Builder builder = new AlertDialog.Builder(FetchiziApp
//				.getInstanse().getActivityInstanse());
//		builder.setMessage(IToastMessages.PERMISSION_ACCESS)
//				.setPositiveButton("Setting", dialogClickListener)
//				.setNegativeButton("Cancel", dialogClickListener).show();
//	}

    private void goToSettings() {
        try {
            Intent myAppSettings = new Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:"
                            + Avon.getInstance().getActivityInstance()
                            .getPackageName()));
            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
            myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Avon.getInstance().getActivityInstance()
                    .startActivityForResult(myAppSettings,
                            ConstantsReal.AppPermissions.MY_PERMISSIONS_ALL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
