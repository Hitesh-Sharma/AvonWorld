package com.avon.india.supports;

import com.avon.india.db.Constants;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * @author ESEC-0054 Preeti
 * 
 *         Jul 24, 2015
 */
public class CheckNetworkState {
	Context context;

	public CheckNetworkState(Context context) {
		this.context = context;

	}

	public static boolean isNetworkAvailable(Context context) {
		boolean outcome = false;

		if (context != null) {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo[] networkInfos = cm.getAllNetworkInfo();

			for (NetworkInfo tempNetworkInfo : networkInfos) {
				if (tempNetworkInfo.isConnected()) {
					outcome = true;
					break;
				}
			}
		}

		return outcome;
	}

	public static void showToast(Context context) {
		Toast.makeText(context, Constants.ErrorMessages.NO_INTERNET_CONNECTION,
				Toast.LENGTH_LONG).show();
	}

}
