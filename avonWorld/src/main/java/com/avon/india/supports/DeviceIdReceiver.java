package com.avon.india.supports;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class DeviceIdReceiver {

	static DeviceIdReceiver _instance;

	private DeviceIdReceiver() {
	}

	public static DeviceIdReceiver getInstance() {
		if (_instance == null)
			_instance = new DeviceIdReceiver();
		return _instance;
	}

	public String getHashedDeivceId(Context context) {

		String imei = null;
		TelephonyManager telephony = (TelephonyManager) context
				.getSystemService("phone");
		String imeinumber = telephony.getDeviceId();
		if (!isValidIMEI(imeinumber))
			imeinumber = Secure.getString(context.getContentResolver(),
					"android_id");
		MessageDigest mdEnc;
		try {
			mdEnc = MessageDigest.getInstance("MD5");
			mdEnc.update(imeinumber.getBytes(), 0, imeinumber.length());
			imei = asHex(mdEnc.digest());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return imei;
	}

	public class GoogleAdsParam {
		public String id = null;
		public String isLimitAdTrackingEnabled = "n";
		public String deviceId = "";
	}

	public GoogleAdsParam getDefaultIDs(Context context) {
		GoogleAdsParam item = new GoogleAdsParam();
		item.id = getHashedDeivceId(context);
		item.deviceId = item.id;
		item.isLimitAdTrackingEnabled = "n";
		return item;
	}

	// public GoogleAdsParam getIdThread(Context mContext) {
	// Info adInfo = null;
	// try {
	// adInfo = AdvertisingIdClient.getAdvertisingIdInfo(mContext);
	// } catch (IOException e) {
	// e.printStackTrace();
	// } catch (GooglePlayServicesNotAvailableException e) {
	// // Google Play services is not available entirely.
	// } catch (IllegalStateException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (GooglePlayServicesRepairableException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// GoogleAdsParam item = new GoogleAdsParam();
	// item.deviceId = getHashedDeivceId(mContext);
	// if (adInfo == null) {
	// item.id = item.deviceId;
	// return item;
	// }
	// item.id = adInfo.getId();
	// item.isLimitAdTrackingEnabled = adInfo.isLimitAdTrackingEnabled() ? "y"
	// : "n";
	// return item;
	// }

	private boolean isValidIMEI(String string) {
		try {
			long l = Long.valueOf(string);
			if (l != 0)
				return true;
			else
				return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private String asHex(byte[] buf) {
		char[] HEX_CHARS = "0123456789abcdef".toCharArray();
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}
}
