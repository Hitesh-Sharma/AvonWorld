package com.avon.india.supports;

import java.io.File;
import java.net.URLDecoder;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.avon.india.Avon;
import com.avon.india.supports.ConstantsReal.StringConstants;
import com.avon.india.supports.ConstantsReal.ToastMessages;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;

@SuppressLint("SimpleDateFormat")
@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
public class SupportUtils {

    static String AppFolderName = ".AvonCaches";

    // hide soft keyboard
    public static void hideVirtualKeyboard(Activity activity) {
        if (activity == null)
            return;
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            View focused = activity.getCurrentFocus();
            if (focused != null && focused.getWindowToken() != null) {
                inputManager.hideSoftInputFromWindow(focused.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
        }
    }

    // Email validity check
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // contact Number Validity check
    public static boolean isValidContact(String contact) {
        if (contact != null && contact.length() > 6) {
            return true;
        }
        return false;
    }

    // custom error message
    public static void setErrorMsg(String msg, EditText edtViewId) {
        int ecolor = Color.RED;
        String estring = msg;
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
        ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
        edtViewId.setError(ssbuilder);
    }

    public static void actionMakeCall(String phone_number) {
        if (phone_number == null || phone_number.trim().equalsIgnoreCase("")) {
            Avon.getInstance().getActivityInstance()
                    .showToast(ToastMessages.INVALID_NUMBER);
            return;
        }
        Intent in = new Intent(Intent.ACTION_CALL);
        in.setData(Uri.parse(StringConstants.CALLING_PARSE + phone_number));
        if(new PermissionToMakeCall().isToMakePhone())
        Avon.getInstance().getActivityInstance().startActivity(in);
    }

    public static boolean isValidString(String text) {
        if (text != null && !text.trim().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    public static int getScreenHeight(FragmentActivity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        return height;
    }

    public static int getScreenWidth(FragmentActivity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static String getRandomString() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static double getDistance(LatLng LatLng1, LatLng LatLng2) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(LatLng2.latitude);
        locationB.setLongitude(LatLng2.longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    public static String getDistanceInDimentions(LatLng LatLng1, LatLng LatLng2) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(LatLng2.latitude);
        locationB.setLongitude(LatLng2.longitude);
        distance = locationA.distanceTo(locationB);
        return getKmFromMeter(distance);
    }

    public static String getKmFromMeter(double distance) {
        String sending_meters = "";
        long meters = 0;

        Log.e("distance", "" + distance);
        String final_string = "";
        if (distance < 0) {
            meters = (long) (distance * 1000);
            Log.e("meters", "" + meters);
            sending_meters = String.valueOf(meters);
            Log.e("sending_meters", "" + sending_meters);
            final_string = sending_meters.concat(" m");
        } else {
            meters = (int) distance;
            sending_meters = String.valueOf(meters);
            final_string = sending_meters.concat(" Km");
        }

        return final_string;

    }

    public static String getUtfDecodedString(String convertingString) {
        if (convertingString == null || convertingString.trim().contains("null") || convertingString.trim().contains("NULL") || convertingString.trim().contains("Null") || convertingString.trim().equalsIgnoreCase(""))
            return "";
        else {
            try {
                return URLDecoder.decode(convertingString, "UTF-8");
            } catch (Exception ee) {
                ee.printStackTrace();
                ;
                return convertingString;
            }
        }
    }

    public static File getFilepathFromUrl(String url) {
        try {
            if (url != null && !url.trim().equalsIgnoreCase("")
                    && !url.contains("http"))
                return new File(url);
            File storagePath = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + AppFolderName + "/");
            storagePath.mkdirs();
            String fileName = getFileName(url);
            if (fileName != null && !fileName.trim().equalsIgnoreCase("")) {
                final File myImage = new File(storagePath, fileName);
                Log.e("myImage", "Support utils myImage " + myImage);
                Log.e("myImage", "myImage.exists() " + myImage.exists());

                if (myImage != null)
                    return myImage;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getFileName(String url)
            throws Exception {
        if (url != null && !url.trim().equalsIgnoreCase("")) {
            int initialIndex = url.lastIndexOf("/");
            int lastIndex = url.length();
            String fileName = url.substring(initialIndex+1, lastIndex);
            Log.e("fileName ", "Support utils fileName " + fileName
                    + " initialIndex " + initialIndex + " lastIndex "
                    + lastIndex + "\n url " + url);
            return fileName;
        }
        return null;
    }

}
