package com.avon.india.supports;

import java.util.ArrayList;
import java.util.List;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.itemproperty.BaseCategoryItem;
import com.avon.india.itemproperty.CategoryItem;
import android.util.Log;

public class PurchaseManager {
	// private float MAX_PERCENT = 75;
	// private float MIN_PERCENT = 50;

	private float MAX_PERCENT = AppPrefrence.getInstance(
			Avon.getInstance().getActivityInstance())
			.getCalculationMaxPercent();
	private float MIN_PERCENT = AppPrefrence.getInstance(
			Avon.getInstance().getActivityInstance())
			.getCalculationMinPercent();
	private List<CategoryItem> purchaseList = new ArrayList<CategoryItem>();

	public void addToPurchase(CategoryItem categoryItem) {
		for (BaseCategoryItem item : purchaseList) {
			if (item.getId() == categoryItem.getId()) {
				purchaseList.remove(item);
				purchaseList.add(categoryItem);
				return;
			}
		}
		if (categoryItem.getItemQuantity() > 0)
			purchaseList.add(categoryItem);
	}

	public void setItemQuantityClear(CategoryItem categoryItem) {
		if (categoryItem.getItemQuantity() > 0) {
			categoryItem.setItemQuantity(0);
			return;
		}
	}

	public void clearPurchaseList() {
		purchaseList.clear();
	}

	public void clearPurchase(CategoryItem categoryItem) {
		if (purchaseList.contains(categoryItem))
			purchaseList.remove(categoryItem);

	}

	// public void clearItemByCategory(String category) {
	// for (CategoryItem items : purchaseList) {
	// if (items.getCategoy().equalsIgnoreCase(category)) {
	// purchaseList.remove(category);
	// }
	// }
	// }

	public float getMaxPrice(float price) {
		float maxPrice = (price / 100) * MAX_PERCENT;
		return maxPrice;
	}

	public float getMinPrice(float price) {
		float maxPrice = (price / 100) * MIN_PERCENT;
		return maxPrice;
	}

	public int getSavings() {
		return getSavings(purchaseList);
	}

	public int getSavings(List<CategoryItem> purchaseList) {
		int saving = 0;
		for (BaseCategoryItem item : purchaseList) {
			if (item.getItemQuantity() > 0) {
				double price = 0;
				try {
					price = Double.parseDouble(item.getPriceAverage());
					Log.e("",
							item.getName() + " Items Avegare Price is"
									+ item.getPriceAverage());
					int averagePrice = (int) price * item.getItemQuantity();
					saving += averagePrice;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return saving;
	}
}
