package com.avon.india.supports;

import android.annotation.SuppressLint;
import android.util.Log;

import com.avon.india.Avon;
import com.avon.india.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("SimpleDateFormat")
public class DateTimeUtils {
//    public String TODAY = Avon.getInstance().getActivityInstance().getResources().getString(R.string.today),
//            YESTERDAY = Avon.getInstance().getActivityInstance().getResources().getString(R.string.yesterday);

    public static String getDateFormatted(String timeDate) {
        String finalDate = timeDate;

        String splitDate = null, splitTime = null;

        if (timeDate.contains(" ")) {
            String[] splitTimeDate = timeDate.split(" ");
            if (splitTimeDate != null && splitTimeDate.length == 2) {
                splitDate = splitTimeDate[0];
                splitTime = splitTimeDate[1];
            }
            if (SupportUtils.isValidString(splitTime)) {
                splitTime = getTimeStringWithAmPm(splitTime);
            }
            if (SupportUtils.isValidString(splitDate)) {
                splitDate = getDateWithTextMonth(splitDate);
            }
            if (SupportUtils.isValidString(splitTime)
                    && SupportUtils.isValidString(splitDate)) {
                finalDate = splitTime + "T" + splitDate;
                Log.e("", "timeDate " + timeDate + " , finalDate " + finalDate);
            }
        }

        return finalDate;
    }

    public static String getTimeStringWithAmPm(String time) {
        AppLogger.e("time in support method=>", "" + time);
        if (time.contains(":")) {
            String[] split = time.split(":");
            if (split != null) {
                try {
                    AppLogger.e("hourinsupport", "" + split[0]);
                    String timewithap_pm = setAmPm(Integer.parseInt(split[0]),
                            Integer.parseInt(split[1]));
                    AppLogger.e("witham_pm", "" + timewithap_pm);
                    return timewithap_pm;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private static String setAmPm(int hour, int mins) {
        String timeSet = "";
        if (hour > 12) {
            hour -= 12;
            timeSet = "PM";
        } else if (hour == 0) {
            hour += 12;
            timeSet = "AM";
        } else if (hour == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String aTime = new StringBuilder().append(hour).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        return aTime;
    }

    @SuppressLint("SimpleDateFormat")
    private static String getDateWithTextMonth(String convertingdate) {
        try {
            SimpleDateFormat initialformatter = new SimpleDateFormat(
                    "yyyy-MM-dd",Locale.US);
            SimpleDateFormat finalformatter = new SimpleDateFormat(
                    "dd-MMM-yyyy",Locale.US);
            Date date = initialformatter.parse(convertingdate);
            String convertedDate = finalformatter.format(date);
            return convertedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertingdate;
    }

    public static boolean isOfSameDate(String datePrevious, String dateCurrent) {
        datePrevious = getValidDay(datePrevious);
        dateCurrent = getValidDay(dateCurrent);
        boolean isDateSame = false;
        Date datePrev = null, dateCurr = null;
        SimpleDateFormat initialformatter = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        try {
            datePrev = initialformatter.parse(datePrevious);
        } catch (Exception e) {
        }
        try {
            dateCurr = initialformatter.parse(dateCurrent);
        } catch (Exception e) {
        }

        if (datePrev != null && dateCurr != null) {
            isDateSame = isSameDay(datePrev, dateCurr);
        } else if (datePrev == null && dateCurr == null) {
            if (datePrevious.equalsIgnoreCase(dateCurrent))
                isDateSame = true;
        }

        return isDateSame;
    }

    public static String getValidDay(String notificationDate) {
        String finalDate = notificationDate;
        SimpleDateFormat initialformatter = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        Date date;
        try {
            date = initialformatter.parse(notificationDate);

            if (isToday(date)) {
                finalDate = Avon.getInstance().getActivityInstance().getResources().getString(R.string.today);
            } else if (isSameDay(date, getYesterdayDateString())) {
                finalDate = Avon.getInstance().getActivityInstance().getResources().getString(R.string.yesterday);
            } else {
                SimpleDateFormat finalfrmt = new SimpleDateFormat("MMM-dd-yy",Locale.US);
                finalDate = finalfrmt.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalDate;
    }

    public static String getTodayDate() {
        String formattedDate = null;
        try {
            SimpleDateFormat initialformatter = new SimpleDateFormat(
                    "yyyy-MM-dd",Locale.US);
            Date today = Calendar.getInstance().getTime();
            formattedDate = initialformatter.format(today);
            Log.e("", "formattedDate" + formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1
                .get(Calendar.DAY_OF_YEAR) == cal2
                .get(Calendar.DAY_OF_YEAR));
    }

    public static Date getYesterdayDateString() {
        Date yesterdayDate = null;
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            yesterdayDate = cal.getTime();
            Log.e("", "yesterdayDate " + yesterdayDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return yesterdayDate;
    }
}
