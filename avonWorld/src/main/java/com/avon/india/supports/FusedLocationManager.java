package com.avon.india.supports;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

public class FusedLocationManager implements ConnectionCallbacks,
		OnConnectionFailedListener, GoogleInterface {
	private GoogleApiClient mGoogleApiClient;
	private Activity _activity;
	private Location mLastLocation;
	private ILocationListner locationListner;

	public FusedLocationManager(Activity _activity) {
		this._activity = _activity;
		buildGoogleApiClient();
	}

	/**
	 * Builds a GoogleApiClient. Uses the addApi() method to request the
	 * LocationServices API.
	 */
	public synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(_activity)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	public void onStart() {
		if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
			mGoogleApiClient.connect();
	}

	public void onStop() {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();
	}

	/**
	 * Runs when a GoogleApiClient object successfully connects.
	 */
	@Override
	public void onConnected(Bundle connectionHint) {
		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);
		if (locationListner != null)
			locationListner.onLocationFetched(mLastLocation);
		if (mLastLocation != null) {
			Log.e("", "lat-" + String.valueOf(mLastLocation.getLatitude()));
			Log.e("", "long-" + String.valueOf(mLastLocation.getLongitude()));
		} else {
			Log.e("", "No location Detected");
		}
	}

	public Location getLocation() {
		return mLastLocation;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
	}

	@Override
	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onResume() {

	}

	public void onDestroy() {
		try {
			if (mGoogleApiClient != null)
				if (mGoogleApiClient.isConnected())
					mGoogleApiClient.disconnect();
		} catch (Exception e) {
		}
	}

	/**
	 * @author Android Lead
	 * @return the locationListner
	 * @daJun 12, 2015
	 * @description
	 */
	public ILocationListner getLocationListner() {
		return locationListner;
	}

	/**
	 * @author Android Lead
	 * @param locationListner
	 *            the locationListner to set
	 * @daJun 12, 2015
	 * @description
	 */
	public void setLocationListner(ILocationListner locationListner) {
		this.locationListner = locationListner;
	}

	public void loadLocation(ILocationListner locationListner) {
		setLocationListner(locationListner);
		if (mLastLocation != null) {
			if (locationListner != null)
				locationListner.onLocationFetched(mLastLocation);
			return;
		}

		if (mGoogleApiClient == null) {
			buildGoogleApiClient();
		} else {

		}
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
			onStop();
		onStart();
	}
}
