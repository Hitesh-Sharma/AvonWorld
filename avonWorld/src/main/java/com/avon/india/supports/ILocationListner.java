package com.avon.india.supports;

import android.location.Location;

public interface ILocationListner {
	public void onLocationFetched(Location lastLocation);
}
