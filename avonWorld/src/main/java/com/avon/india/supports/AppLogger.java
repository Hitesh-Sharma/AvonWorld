package com.avon.india.supports;

import com.avon.india.Avon;

import android.util.Log;

public class AppLogger {

    public static void e(String tag, String msg) {
        if (!Avon.ON_DEBUG_MODE)
            return;
        if (msg.length() > 4000) {
            Log.e(tag, msg.substring(0, 4000));
            e("", msg.substring(4000));
        } else
            Log.e(tag, msg);
    }

}
