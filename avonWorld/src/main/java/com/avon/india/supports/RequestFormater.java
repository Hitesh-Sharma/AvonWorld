package com.avon.india.supports;

import java.util.HashMap;

import com.avon.india.AppPrefrence;
import com.avon.india.Avon;
import com.avon.india.supports.ConstantsReal.IConstants;
import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.ConstantsReal.WebUrls;

public class RequestFormater implements JSONKeys {

    public static HashMap<String, String> getRegisterOnServer(String gcm_key,
                                                              String device_id, int country_id) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_GCM_ID, gcm_key);
            hashMap.put(JSONKeys.JSON_KEY_DEVICE_ID, device_id);
            hashMap.put(JSONKeys.JSON_KEY_TYPE, WebUrls.SERVER_TYPE);
            hashMap.put(JSONKeys.JSON_PLATFORM, "AND");
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getAllProductDetails(int offset,
                                                               int country_id, String timeStamp) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_OFFSET, offset + "");
            try {
//                if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA))
//                    hashMap.put(JSONKeys.JSON_KEY_LIMIT, "100");
//                else
                hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            } catch (Exception e) {
                hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            }
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_ORDER, "asc");
            hashMap.put(JSONKeys.JSON_KEY_TIMESTAMP, timeStamp);

            AppLogger.e("", "postDataParams  " + hashMap);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    //Custom test
    public static HashMap<String, String> getCategoryProductDetails(int offset,
                                                                    int country_id, int category_id) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_OFFSET, offset + "");
            hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_CATEGORY_ID, category_id + "");
            hashMap.put(JSONKeys.JSON_KEY_ORDER, "asc");

            AppLogger.e("", "postDataParams  " + hashMap);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getAllBuitiques(int offset,
                                                          int country_id, double lat, double lng) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_OFFSET, offset + "");
            try {
//                if (AppPrefrence.getInstance(Avon.getInstance().getActivityInstance()).getMyCountryName().equalsIgnoreCase(ConstantsReal.CountryNames.INDIA))
//                    hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_MAX_COUNT + "");
//                else
                hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            } catch (Exception e) {
                hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            }
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_LAT, lat + "");
            hashMap.put(JSONKeys.JSON_KEY_LONG, lng + "");
            AppLogger.e("", "postDataParams  " + hashMap);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getStatesBuitiques(int offset,
                                                             int country_id, int state_id) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_OFFSET, offset + "");
            hashMap.put(JSONKeys.JSON_KEY_LIMIT, IConstants.MAX_COUNT + "");
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_STATE_ID, state_id + "");
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getCurrentOffers(int country_id,
                                                           String currentDate) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_START_DATE, currentDate);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getVideos(int country_id) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<String, String> getSubmittedFeedback(
            String device_id, String gcm_id, String email, String phone,
            String message, int country_id) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        try {
            hashMap.put(JSONKeys.JSON_KEY_DEVICE_ID, device_id);
            hashMap.put(JSONKeys.JSON_KEY_GCM_ID, gcm_id);
            hashMap.put(JSONKeys.JSON_KEY_COUNTRY_ID, country_id + "");
            hashMap.put(JSONKeys.JSON_KEY_EMAIL, email);
            hashMap.put(JSONKeys.JSON_KEY_PHONE, phone);
            hashMap.put(JSONKeys.JSON_KEY_MESSAGE, message);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

}
