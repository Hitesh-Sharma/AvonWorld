package com.avon.india.callAsyncs;

import com.avon.india.Avon;
import com.avon.india.async.MyLoader;
import com.avon.india.async.MyLoader.RequestType;
import com.avon.india.async.ResponseParser;
import com.avon.india.interfaces.ILoadListner;
import com.avon.india.supports.CheckNetworkState;
import com.avon.india.supports.ConstantsReal.IResponseCode;
import com.avon.india.supports.ConstantsReal.JSONKeys;
import com.avon.india.supports.ConstantsReal.ToastMessages;
import com.avon.india.supports.ConstantsReal.WebUrls;

import java.util.HashMap;

public class CallAsyncToGetPercent implements ILoadListner {

	public CallAsyncToGetPercent() {
	}

	public void callAsync(int countryId) {
		if (countryId < 0) {
			Avon.getInstance().getActivityInstance()
					.showToast(ToastMessages.NO_COUNTRY_SELECTED);
			return;
		}
		if (CheckNetworkState.isNetworkAvailable(Avon.getInstance()
				.getActivityInstance())) {
			HashMap<String, String> prams = new HashMap<String, String>();
			prams.put(JSONKeys.JSON_KEY_COUNTRY_ID, countryId + "");
			MyLoader loader = new MyLoader(Avon.getInstance()
					.getActivityInstance());
			loader.setOnLoadListner(this);
			loader.setRequestType(RequestType.CALCULATE_PERCENTAGE);
			loader.startLoading(WebUrls.URL_GET_PERCENT, prams);
		} else {
			Avon.getInstance().getActivityInstance().showNoInternetToast(null);
		}
	}

	@Override
	public void afterLoadComplete(ResponseParser response) {

		if (response == null) {
			Avon.getInstance().getActivityInstance()
					.showToast(ToastMessages.NO_MESSAGE_FROM_SERVER);
			return;
		}

		if (response.getResponseCode() != IResponseCode.RESPONSE_SUCCESS) {
			if (response.getResponseMessage() != null
					&& !response.getResponseMessage().trim()
							.equalsIgnoreCase("")) {
				Avon.getInstance().getActivityInstance()
						.showToast(response.getResponseMessage());
			}
		}
	}

}
