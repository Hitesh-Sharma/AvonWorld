package com.avon.india.customviews;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

/**
 * @author ESEC-0054 Preeti
 * 
 *         Jul 24, 2015
 */
// animation applied to textview

public class TextSwitcherAnimation extends CustomTextViewHelviticaLight {

	private CharSequence mText;
	private int mIndex;
	private long mDelay = 800;

	public TextSwitcherAnimation(Context context) {
		super(context);
	}

	public TextSwitcherAnimation(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	private Handler mHandler = new Handler();
	private Runnable characterAdder = new Runnable() {
		@Override
		public void run() {
			setText(mText.subSequence(0, mIndex++));
			if (mIndex <= mText.length()) {
				mHandler.postDelayed(characterAdder, mDelay);
			}
			else{
				if(iOnComplete!=null)
					iOnComplete.onAnimationCompleted(true);
			}
		}
	};

	public void animateText(CharSequence text) {
		mText = text;
		mIndex = 0;

		setText(" ");
		mHandler.removeCallbacks(characterAdder);
		mHandler.postDelayed(characterAdder, mDelay);
	}

	public void setCharacterDelay(long millis) {
		mDelay = millis;
	}

	IonAnimationCompleted iOnComplete;
	
	public void setOnAnimationCompleted(IonAnimationCompleted iOnComplete){
		this.iOnComplete=iOnComplete;
	}
	
	public interface IonAnimationCompleted{
		public void onAnimationCompleted(boolean isCompleted);
	}
	
	
}