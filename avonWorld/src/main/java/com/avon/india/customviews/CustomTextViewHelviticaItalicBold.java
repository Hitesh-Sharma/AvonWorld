package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomTextViewHelviticaItalicBold extends TextView {
	public CustomTextViewHelviticaItalicBold(Context context) {
		super(context);
		inIt(context);
	}

	public CustomTextViewHelviticaItalicBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		inIt(context);
	}

	public CustomTextViewHelviticaItalicBold(Context context,
			AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		inIt(context);
	}

	public void inIt(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.HELVETICA_NEUE_BOLD_ITALIC);
		this.setTypeface(typeface);
	}
}
