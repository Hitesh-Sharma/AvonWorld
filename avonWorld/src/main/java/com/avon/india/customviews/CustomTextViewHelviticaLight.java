package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomTextViewHelviticaLight extends TextView {
	public CustomTextViewHelviticaLight(Context context) {
		super(context);
	}

	public CustomTextViewHelviticaLight(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomTextViewHelviticaLight(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void inIt(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.HELVETICA_NEUE_LIGHT);
		this.setTypeface(typeface);
	}
}
