package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomTextViewTimesItalic extends TextView {
	public CustomTextViewTimesItalic(Context context, AttributeSet attrs) {
		super(context, attrs);
		Init(context);
	}

	public CustomTextViewTimesItalic(Context context) {
		super(context);
		Init(context);
	}

	public CustomTextViewTimesItalic(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		Init(context);
	}

	public void Init(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.TIMES_ITALIC);
		this.setTypeface(typeface);

	}

}
