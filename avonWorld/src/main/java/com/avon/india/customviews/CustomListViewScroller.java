package com.avon.india.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class CustomListViewScroller extends ListView {
	private int positionYInScroll;

	public CustomListViewScroller(Context context) {
		super(context);
	}

	public CustomListViewScroller(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomListViewScroller(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
	}

	public int getPositionYInScroll() {
		return positionYInScroll;
	}

	public void setPositionYInScroll(int positionYInScroll) {
		this.positionYInScroll = positionYInScroll;
	}

	public interface ObservableListView {
		public void internalVerticalOffset(int offset);
	}

	@Override
	protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX,
			boolean clampedY) {
		super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
	}

}
