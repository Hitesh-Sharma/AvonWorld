package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomTextViewHelviticaBold extends TextView {
	public CustomTextViewHelviticaBold(Context context) {
		super(context);
		inIt(context);
	}

	public CustomTextViewHelviticaBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		inIt(context);
	}

	public CustomTextViewHelviticaBold(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		inIt(context);
	}

	public void inIt(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.HELVETICA_NEUE_BOLD);
		this.setTypeface(typeface);
	}
}
