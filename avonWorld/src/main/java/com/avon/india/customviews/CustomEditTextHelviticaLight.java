package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomEditTextHelviticaLight extends EditText {
	public CustomEditTextHelviticaLight(Context context, AttributeSet attrs) {
		super(context, attrs);
		Init(context);
	}

	public CustomEditTextHelviticaLight(Context context) {
		super(context);
		Init(context);
	}

	public CustomEditTextHelviticaLight(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		Init(context);
	}

	public void Init(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.HELVETICA_NEUE_BOLD);
		this.setTypeface(typeface);

	}

}
