package com.avon.india.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avon.india.supports.ConstantsReal.FontFiles;

public class CustomTextViewTimesBoldItalic extends TextView {

	public CustomTextViewTimesBoldItalic(Context context) {
		super(context);
		inIt(context);
	}

	public CustomTextViewTimesBoldItalic(Context context, AttributeSet attrs) {
		super(context, attrs);
		inIt(context);
	}

	public CustomTextViewTimesBoldItalic(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		inIt(context);
	}

	public void inIt(Context context) {
		if (isInEditMode())
			return;
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),
				FontFiles.TIMES_BOLD_ITALIC);
		this.setTypeface(typeface);
	}

}
