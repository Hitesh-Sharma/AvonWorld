package com.avon.india;

import android.content.Context;
import android.content.SharedPreferences;

import com.avon.india.async.ParserResponse;
import com.avon.india.supports.ConstantsReal.PreferenceKeys;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class AppPrefrence implements PreferenceKeys {

    static AppPrefrence _appPrefrence;
    SharedPreferences _sharedPrefrence;
    Context context;

    public static AppPrefrence getInstance(Context context) {
        if (_appPrefrence == null)
            _appPrefrence = new AppPrefrence(context);
        return _appPrefrence;
    }

    private AppPrefrence(Context context) {
        this.context = context;
        _sharedPrefrence = context.getSharedPreferences(PREF_MAIN,
                Context.MODE_PRIVATE);
    }

    public void saveNotificationIds(int notificationid) {
        try {
            JSONArray arrayIds = new JSONArray(_sharedPrefrence.getString(
                    PREF_NOTIFICATIONS_IDS, "[]"));
            arrayIds.put(notificationid);
            _sharedPrefrence.edit()
                    .putString(PREF_NOTIFICATIONS_IDS, arrayIds.toString())
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public List<Integer> getNotificationIds() {
        List<Integer> listIds = new ArrayList<Integer>();
        try {
            JSONArray arrayIds = new JSONArray(_sharedPrefrence.getString(
                    PREF_NOTIFICATIONS_IDS, "[]"));
            if (arrayIds != null && arrayIds.length() > 0) {
                for (int i = 0; i < arrayIds.length(); i++) {
                    listIds.add(arrayIds.getInt(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listIds;

    }

    public void saveCountryIds(String countriesJson) {
        try {
            _sharedPrefrence.edit()
                    .putString(PREF_COUNTRY_IDS, countriesJson)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Object getCountryIds() {
        try {
            String response = _sharedPrefrence.getString(
                    PREF_COUNTRY_IDS, null);
            return ParserResponse.getCountryList(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public void clearNotificationIds() {
        _sharedPrefrence.edit().remove(PREF_NOTIFICATIONS_IDS).commit();
    }

    public void setMyCountryId(int country_id) {
        _sharedPrefrence.edit().putInt(PREF_COUNTRY_ID, country_id).commit();
    }

    public int getMyCountryId() {
        int country_id = 0;
        country_id = _sharedPrefrence.getInt(PREF_COUNTRY_ID, country_id);
        return country_id;
    }

    public void setMyCountryName(String language) {
        _sharedPrefrence.edit().putString(PREF_COUNTRY_NAME, language).commit();
    }

    public String getMyCountryName() {
        return _sharedPrefrence
                .getString(PREF_COUNTRY_NAME, null);
    }

//    public void setTimeStamp(String language) {
//        _sharedPrefrence.edit().putString(PREF_TIMESTAMP, language).commit();
//    }

//    public String getTimeStamp() {
//        return _sharedPrefrence
//                .getString(PREF_TIMESTAMP, "");
//    }

    public void setMyLocalLanguage(String language) {
        _sharedPrefrence.edit().putString(PREF_LOCALE_LANGUAGE, language)
                .commit();
    }

    public String getMyLocalLanguage() {
        return _sharedPrefrence.getString(PREF_LOCALE_LANGUAGE, "en");
    }

    public void setUserRegistationStatus(boolean status) {
        _sharedPrefrence.edit().putBoolean(PREF_IS_USER_REGISTERD, status)
                .commit();
    }

    public boolean getUserRegistationStatus() {
        boolean status = false;
        status = _sharedPrefrence.getBoolean(PREF_IS_USER_REGISTERD, false);
        return status;
    }

    public void setBuitqueLoacatorStatus(boolean status) {
        _sharedPrefrence.edit()
                .putBoolean(PREF_IS_COUNRY_HAVE_BUITIQUES, status).commit();
    }

    public boolean getBuitqueLocatorStatus() {
        boolean status = false;
        status = _sharedPrefrence.getBoolean(PREF_IS_COUNRY_HAVE_BUITIQUES,
                false);
        return status;
    }

    public void setAboutUsStatusForThiwan(boolean status) {
        _sharedPrefrence.edit()
                .putBoolean(PREF_IS_ABOUT_US_THIWAN, status).commit();
    }

    public boolean getAboutUsStatusForThiwan() {
        boolean status = false;
        status = _sharedPrefrence.getBoolean(PREF_IS_ABOUT_US_THIWAN,
                false);
        return status;
    }

    public void setBueatyStoreStatus(boolean status) {
        _sharedPrefrence.edit()
                .putBoolean(PREF_IS_COUNRY_HAVE_BUEATY_STORE, status).commit();
    }

    public boolean getBueatyStoreStatus() {
        boolean status = false;
        status = _sharedPrefrence.getBoolean(PREF_IS_COUNRY_HAVE_BUEATY_STORE,
                false);
        return status;
    }

    public void setCalculationMaxPercent(float max) {
        _sharedPrefrence.edit().putFloat(PREF_MAX_PERCENTAGE, max).commit();
    }

    public float getCalculationMaxPercent() {
        return _sharedPrefrence.getFloat(PREF_MAX_PERCENTAGE, 0.0f);
    }

    public void setCalculationMinPercent(float min) {
        _sharedPrefrence.edit().putFloat(PREF_MIN_PERCENTAGE, min).commit();
    }

    public float getCalculationMinPercent() {
        return _sharedPrefrence.getFloat(PREF_MIN_PERCENTAGE, 0.0f);
    }


}
