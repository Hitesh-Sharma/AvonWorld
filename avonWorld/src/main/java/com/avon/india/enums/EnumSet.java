package com.avon.india.enums;

public class EnumSet {

    public enum CategoryType {
        START_SAVING, PRODUCTS, OFFERS, OUR_HISTORY, OUR_MISSION, ABOUT_ONE
    }

    public enum CountryReason {
        EAST, WEST
    }

    public enum LocationType {
        ALL, STATE_VISE
    }

    public enum AboutUsType {
        MISSION, HISTORY, STRENGTH
    }

    public enum UserStatus {
        REGISTERED, NOT_REGISTERED, NEW_USER, LANG_CHANGED
    }

    public enum ImageToShowOf {
        BEAUTY_STORE, CURRENT_OFFERS
    }

    public enum SplashType {
        CHOOSE_LOCATION, LOCATION_CHOOSEN
    }

}
